--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.4
-- Dumped by pg_dump version 9.6.6

-- Started on 2018-01-15 15:55:07 MSK

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE dbqdr7ncu514hf;
--
-- TOC entry 3132 (class 1262 OID 486468)
-- Name: dbqdr7ncu514hf; Type: DATABASE; Schema: -; Owner: uotmjnaeeeyngs
--

CREATE DATABASE dbqdr7ncu514hf WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE dbqdr7ncu514hf OWNER TO uotmjnaeeeyngs;

\connect dbqdr7ncu514hf

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 13277)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3135 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 493 (class 1247 OID 494137)
-- Name: friend_status; Type: TYPE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE TYPE friend_status AS ENUM (
    'CONFIRMED',
    'IN',
    'OUT'
);


ALTER TYPE friend_status OWNER TO uotmjnaeeeyngs;

--
-- TOC entry 496 (class 1247 OID 494144)
-- Name: home_type; Type: TYPE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE TYPE home_type AS ENUM (
    'WORK',
    'HOME'
);


ALTER TYPE home_type OWNER TO uotmjnaeeeyngs;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 185 (class 1259 OID 494150)
-- Name: accounts; Type: TABLE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE TABLE accounts (
    id integer NOT NULL,
    name character varying(15) DEFAULT ''::character varying,
    surname character varying(15) DEFAULT ''::character varying,
    patronymic character varying(15) DEFAULT ''::character varying,
    birthday date,
    email character varying(25) NOT NULL,
    icq character varying(15) DEFAULT ''::character varying,
    skype character varying(15) DEFAULT ''::character varying,
    other text,
    registrationdate date,
    password character(64) DEFAULT NULL::bpchar,
    image text
);


ALTER TABLE accounts OWNER TO uotmjnaeeeyngs;

--
-- TOC entry 186 (class 1259 OID 494162)
-- Name: accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE SEQUENCE accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE accounts_id_seq OWNER TO uotmjnaeeeyngs;

--
-- TOC entry 3137 (class 0 OID 0)
-- Dependencies: 186
-- Name: accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER SEQUENCE accounts_id_seq OWNED BY accounts.id;


--
-- TOC entry 187 (class 1259 OID 494164)
-- Name: address; Type: TABLE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE TABLE address (
    id integer NOT NULL,
    account_id integer NOT NULL,
    country character varying(15) DEFAULT NULL::character varying,
    city character varying(15) DEFAULT NULL::character varying,
    street character varying(45) DEFAULT NULL::character varying,
    house integer,
    structure integer,
    apartment integer
);


ALTER TABLE address OWNER TO uotmjnaeeeyngs;

--
-- TOC entry 188 (class 1259 OID 494170)
-- Name: address_id_seq; Type: SEQUENCE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE SEQUENCE address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE address_id_seq OWNER TO uotmjnaeeeyngs;

--
-- TOC entry 3138 (class 0 OID 0)
-- Dependencies: 188
-- Name: address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER SEQUENCE address_id_seq OWNED BY address.id;


--
-- TOC entry 189 (class 1259 OID 494172)
-- Name: friends; Type: TABLE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE TABLE friends (
    id integer NOT NULL,
    friend_id integer NOT NULL,
    status friend_status NOT NULL
);


ALTER TABLE friends OWNER TO uotmjnaeeeyngs;

--
-- TOC entry 190 (class 1259 OID 494175)
-- Name: group_admins; Type: TABLE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE TABLE group_admins (
    group_id integer NOT NULL,
    account_id integer NOT NULL
);


ALTER TABLE group_admins OWNER TO uotmjnaeeeyngs;

--
-- TOC entry 191 (class 1259 OID 494178)
-- Name: groups; Type: TABLE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE TABLE groups (
    id integer NOT NULL,
    name character varying(15) DEFAULT NULL::character varying,
    creator integer NOT NULL,
    image text,
    description text
);


ALTER TABLE groups OWNER TO uotmjnaeeeyngs;

--
-- TOC entry 192 (class 1259 OID 494185)
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE SEQUENCE groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE groups_id_seq OWNER TO uotmjnaeeeyngs;

--
-- TOC entry 3139 (class 0 OID 0)
-- Dependencies: 192
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER SEQUENCE groups_id_seq OWNED BY groups.id;


--
-- TOC entry 193 (class 1259 OID 494187)
-- Name: groups_link; Type: TABLE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE TABLE groups_link (
    group_id integer NOT NULL,
    account_id integer NOT NULL
);


ALTER TABLE groups_link OWNER TO uotmjnaeeeyngs;

--
-- TOC entry 194 (class 1259 OID 494190)
-- Name: message; Type: TABLE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE TABLE message (
    id integer NOT NULL,
    date timestamp without time zone NOT NULL,
    from_id integer NOT NULL,
    to_id integer NOT NULL,
    text text NOT NULL
);


ALTER TABLE message OWNER TO uotmjnaeeeyngs;

--
-- TOC entry 195 (class 1259 OID 494205)
-- Name: message_id_seq; Type: SEQUENCE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE SEQUENCE message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE message_id_seq OWNER TO uotmjnaeeeyngs;

--
-- TOC entry 3140 (class 0 OID 0)
-- Dependencies: 195
-- Name: message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER SEQUENCE message_id_seq OWNED BY message.id;


--
-- TOC entry 196 (class 1259 OID 494208)
-- Name: persistent_logins; Type: TABLE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE TABLE persistent_logins (
    username character varying(64) NOT NULL,
    series character varying(64) NOT NULL,
    token character varying(64) NOT NULL,
    last_used timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE persistent_logins OWNER TO uotmjnaeeeyngs;

--
-- TOC entry 197 (class 1259 OID 494212)
-- Name: phones; Type: TABLE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE TABLE phones (
    id integer NOT NULL,
    account_id integer NOT NULL,
    phone character varying(15) NOT NULL,
    type home_type NOT NULL
);


ALTER TABLE phones OWNER TO uotmjnaeeeyngs;

--
-- TOC entry 198 (class 1259 OID 494216)
-- Name: phones_id_seq; Type: SEQUENCE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE SEQUENCE phones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE phones_id_seq OWNER TO uotmjnaeeeyngs;

--
-- TOC entry 3141 (class 0 OID 0)
-- Dependencies: 198
-- Name: phones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER SEQUENCE phones_id_seq OWNED BY phones.id;


--
-- TOC entry 199 (class 1259 OID 494228)
-- Name: posts; Type: TABLE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE TABLE posts (
    id integer NOT NULL,
    account_id integer NOT NULL,
    date timestamp without time zone DEFAULT now() NOT NULL,
    text text NOT NULL
);


ALTER TABLE posts OWNER TO uotmjnaeeeyngs;

--
-- TOC entry 200 (class 1259 OID 494236)
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: uotmjnaeeeyngs
--

CREATE SEQUENCE posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE posts_id_seq OWNER TO uotmjnaeeeyngs;

--
-- TOC entry 3142 (class 0 OID 0)
-- Dependencies: 200
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER SEQUENCE posts_id_seq OWNED BY posts.id;


--
-- TOC entry 2951 (class 2604 OID 494238)
-- Name: accounts id; Type: DEFAULT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY accounts ALTER COLUMN id SET DEFAULT nextval('accounts_id_seq'::regclass);


--
-- TOC entry 2955 (class 2604 OID 494239)
-- Name: address id; Type: DEFAULT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY address ALTER COLUMN id SET DEFAULT nextval('address_id_seq'::regclass);


--
-- TOC entry 2957 (class 2604 OID 494240)
-- Name: groups id; Type: DEFAULT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY groups ALTER COLUMN id SET DEFAULT nextval('groups_id_seq'::regclass);


--
-- TOC entry 2958 (class 2604 OID 494241)
-- Name: message id; Type: DEFAULT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY message ALTER COLUMN id SET DEFAULT nextval('message_id_seq'::regclass);


--
-- TOC entry 2960 (class 2604 OID 494242)
-- Name: phones id; Type: DEFAULT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY phones ALTER COLUMN id SET DEFAULT nextval('phones_id_seq'::regclass);


--
-- TOC entry 2962 (class 2604 OID 494243)
-- Name: posts id; Type: DEFAULT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY posts ALTER COLUMN id SET DEFAULT nextval('posts_id_seq'::regclass);


--
-- TOC entry 3112 (class 0 OID 494150)
-- Dependencies: 185
-- Data for Name: accounts; Type: TABLE DATA; Schema: public; Owner: uotmjnaeeeyngs
--

INSERT INTO accounts (id, name, surname, patronymic, birthday, email, icq, skype, other, registrationdate, password, image) VALUES (10, 'тер', '', 'qw', NULL, 'qqq@gmail.com', NULL, NULL, NULL, '2017-06-11', '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b', NULL);
INSERT INTO accounts (id, name, surname, patronymic, birthday, email, icq, skype, other, registrationdate, password, image) VALUES (11, 'термин', '4', '2', '2017-07-14', '3@gmail.com', NULL, NULL, NULL, '2017-06-30', '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b', NULL);
INSERT INTO accounts (id, name, surname, patronymic, birthday, email, icq, skype, other, registrationdate, password, image) VALUES (14, 'Терм', '', '', NULL, '222@bk.ru', NULL, NULL, NULL, '2017-07-09', '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b', NULL);
INSERT INTO accounts (id, name, surname, patronymic, birthday, email, icq, skype, other, registrationdate, password, image) VALUES (15, 'Терми', '', '', NULL, '2222@bk.ru', '', '', NULL, '2017-07-09', '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b', NULL);
INSERT INTO accounts (id, name, surname, patronymic, birthday, email, icq, skype, other, registrationdate, password, image) VALUES (16, 'Терминн', '2', '', NULL, '22222@bk.ru', '', '', NULL, '2017-07-08', '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b', NULL);
INSERT INTO accounts (id, name, surname, patronymic, birthday, email, icq, skype, other, registrationdate, password, image) VALUES (17, 'Тер', '', '', NULL, '222222@bk.ru', '', '', NULL, '2017-07-08', '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b', NULL);
INSERT INTO accounts (id, name, surname, patronymic, birthday, email, icq, skype, other, registrationdate, password, image) VALUES (18, 'Терминн', '1', '', NULL, '2222222@bk.ru', '', '', NULL, '2017-07-09', '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b', NULL);
INSERT INTO accounts (id, name, surname, patronymic, birthday, email, icq, skype, other, registrationdate, password, image) VALUES (19, 'NIKOLAI ZHADOVS', 'ZHADOVSKII', '', NULL, 'nick911@bk.ru', NULL, NULL, NULL, '2017-10-20', '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCAB3AFoDAREAAhEBAxEB/8QAGwAAAgMBAQEAAAAAAAAAAAAABAUDBgcCAAH/xAAaAQACAwEBAAAAAAAAAAAAAAABBAACAwUG/9oADAMBAAIQAxAAAAHIqVcmP6yhmesPGesDgBbTRMLKbRHJBUuTLVU5oY2rraV261fHlpIWwseV7ZQ51epNo7ItGVqhCyp0LlRisYFA/wARNoGFDpuNszvBb1Ph0KkpgMtmLhh1FFFULXMrGlfthfcbXHK9FbXMBc46x65x32MU6CbZLvZWv4kEhuDo6+2fOLFgz9NYZ3AC9SFG1zSrClgsLhcV2aR/lcBnKRjJD3ucOC3BhM4I9ndTSzbg9CPG0ZLbSnu4gn0ogBb7UZQfDAMNV162LlOD87eWWLbwD7vPZJMKQVz6zrLTsQMgKE1bXviO8w6ZtWodVIuwmB5T6F44vcoj/MzrscdjjeRPWZPbgG2P4OtMZlWqhDkj61+wdVsp+2xk57DHmMetPpNI9t5KTXLVPNejWoO5A+vadsiZF+dwk2Qkmfg10MrUb2PleDNh43YrPOfyWxJg+ieh5pchVju4vpXpPq/Nr8WdtTZS85/JTOoYxJTHWGmrJbzwzWriXsvLLsWdoW2A5r+SEz1K60JA01LfW09hbSKw/8QAKhAAAQQBAwMEAQUBAAAAAAAAAgABAwQFBhESExQhByIjJDEQFTIzQTT/2gAIAQEAAQUC4vtim+xmw2rbrdcluib6jMtLzcg1JV6VoW9vHccOH3M4H0XZY7HtbO5p+1ENjHSxRGO9GrIcb0Lpw3NRVerQAfY1aJljoYhs3qneVj07wWOx71JamQsMOQObIwPSk7U6kkUs0BkFUu7oFTkiKHpMONdnyBfxyOWsVbmLysli1L1iPJ3ukWVbavMG4xk/DTdpyTxeRFQN8r/1nQOQ4oIWk7KWJSVomlsUuqM9bgo4w3qONW1umUP9st7otHfKV535EOWtRgBu5vPuLWeoz+Hd9xjznGNlz4NISq2eS5+HTeEZ7NWJyY/KZ1smdTl7SZzKHeB45XJAnU7vtSLYRfeP/UxcU7vOqlYIZMvVkmtCBwDDDNIElaYUUHJ4cYEKjfpr/Vkohv1m07kEOn8iv2DJoah0bMdhttTZWDg1x+IS9ZMDst1soZsTSM9Y064VMw2SWup5gjrsTFUzM8Iym9iZ4hdVWcJHdeP03+Q/xpv/AJPUKyMIwXgFULMVucbURMWYiZHlZDU00hDD5dn8GXAu5Ylpkt6vqR5siy2TD4fwt93ON9oidi5Kd0Be7S5fB6iN8zMifiwyeFhanfZIdPQsmxNaNdlApvyH8tMP8HqI/wAjKGDubAeEy0RU52iHdFEuiv/EACQRAAEDAwUAAwEBAAAAAAAAAAEAAgMQERIEICExQRMUUTIi/9oACAEDAQE/Adt1erUd0UeZ5Ri/FLDZuVBQ7o2YgEerEpzS5uKsrI0sjsieek3nlTy4iwRRoKjujjZad7TceoLVOa0j9XyAq9Aat7Uk3gQf+q6GslDcVkb3Kuo32NcqPdiKdbfU03bskdkUxuRspG2Oxqd2oXeVebNvSJ4YbqVzXm4oIpHchqdE9n9BDhHnlR8OrIBI2y+s5fXevrvWm0rSbyU1xbiGntYhCO6bHbmrtTCOyvuw+FNkz6Tr2WbmnhR65zR/oXUspldkaR90Ao4cJva0v8q1wjEfEYnr4noQH1Rx2KDbLymOXC+EjlaXpDaO6GjR0nDhab1Da3tWKxWITPE7paf1CgqzZ//EACYRAAIBAwQCAgMBAQAAAAAAAAABAgMREgQQITEgQRQyEyJRI2H/2gAIAQIBAT8B8LGJY97VP6QfFvKpNx4iKT9shW/bFnskNJkHsh9bXJO5KyOIyTL8id0R7Hwy5EYxDjwP68lOGbuR7Iliorc7sZCnKRUoyp8rokaalKd36HQmujld7S5RfZlDS8ZSHTS6Eh6KlKWRirWRiaiipxut8dqFPOe3e7GLlFWOE2vDT08IlSahG7KMrxvuyRD6msh1LejHOaRY1FGVVWTNNCdKOMtpVqce5IjVhU+juPkX68GoWVN70MqM8mj5sP4fMpHzKRX1ba/zGjRKSk5LoyZOvh2VNTkrLa3/AEhptRLpHwa/tEqTh2QspGCl2T0cZfVlKkqUcUM1NsfCmyRrPshcSuRrL2j89M/PTHqY+kVqua6Lp75Y8irKXBrfsvKXQnvLopv9kazteVR2RmjMyZLpkO0av1u3ZX3rv14f/8QANxAAAQMDAAYIAwcFAAAAAAAAAQACEQMSIRAiMUFRcQQTIDJhcoGxI2KhM0KCkbLC8TQ1UsHR/9oACAEBAAY/Ak7yFT4t7IPzaK1A7tYJtQDDx9Qho/CUT5dGy/MWTCuo0aNRv+EZV1Sh1BC2RrLVjPESqLn22zBgQnnfT1k3ks9IaPRNLa95ziyF1V1khuYlf1A9WQnFnxDbjmnitQttEjdP1T21aJo5Vlubp2qQzEoQw/kmF4yW2uBRZbNuJWu0ucqcNtGfZM/CqlNpbaOIXxbYbvaEbdYOGDxH5Lo9Aw6qTBG2F+MIFYMKrRcZ+8JWzQ1M5D3RcQGt4uXVka547wradV8INcLqgPeRtc2pmYKtcy30XdCZUAjcdLU1re8BtWu6VMq28H5jtVzjJWNq6txxu56WgjIGgu4KUefYnZyWsZM9mEGN2uMBODu80x2DBhGeJ9+xPBazi3kjUGtiBKvosc4EST4rWaW7shS2jUcOIYVrUyzz4USESartsw3CtBJHjpc2nZ1m5zsLFNp5E/8AF9l9VikPV4QZ0ttz39ymHQD4k8E2CM7I3plJhvrA7uC1sFYys6Nq/uFd0bg+4ey1az60fL/CbWpdawXRrVHcDumEy15DN8GFc15a47SCnCp8Wd8wnPdg7hoOyI0bdB0erf0lUqe1z93gstd6JlFste/AvwnkNebNuqcLUpuJ8VgCmPDagetcfVN56CVCPp+kroPld/rsgBDCbz0FBP8AKPYroHJ/7ezSpcz9FrEe67q7iKCqeUexXQOT/wBuilRmDUcGTzXHRX6QfuNtHM/x2P/EACYQAQACAQMDBQEBAQEAAAAAAAEAESExQVFhcZEQgaHB8LHR8eH/2gAIAQEAAT8hocdOtZK9P+DL/iVO8s7SzL+k/wAmCb+t35mNSmv42qeLI1JhnJ/Erm3EvLY1D3FeMkF8cMX+wDlrmmuZl6Y8HZisY6CB4CuXqcQTburXBr8N+08AlQdubFYUjNsd4Fz9aEf7V9ozF90aNm68Et8Fw2p7oOBoDrv+3hzRUUpwx6rUKiI/N4i/jHTcwy/65qMNbxv41Wk96+Lunxv6JiLOi3YjygLYOeY8mpBTk5VEeG7VC413l5WP8oVSWYhcijsywdb6+j9TIw8QDTEo7v1M/wAuEb2tNz21idmQdB0PmJKJaF4Ipi3XqHPmY2CBjdOuPmWUK4YFV8EOQbwcOst6X5YdLqDxekWCHTPM1Iu9eJTtDANn91ixjNqurMykJpzFOzvO35R67M9gBY4oTXR3i5UEUzl/a+pViixW+pgTY3hviUX7w6ulzDUc/RoHHL39NSA1+j9mdn0ivWyVJ/RCWPBzHuiZtSGKy5SddLxFsBsPTzG4VibHmVivXW968VCyBugSdzCh/JQjLoDKKnly9laFlfmAu+a2+Y7pceG2n/wLiMhu6oJoCTJCg9i94GwFl9zWwiwo5013nTr/ALGZmuOlHfx4lYJs4upmDKGWK2ivQq9UrRQf9LgEGFax/ggmAOgNt4xrG0olX7NO80svSFOGARntzXpjbp1YVZdg0DiB619TVluS2CC0HeY5hIsp8LT1yOzoa/Upc7sZaoFNAvF3DqAUmAc8QbxYHnMux1qpYqFV0vE2m8JQD0w2RzAD0hsoLv8ArFW0DxUDrvLNjTpLco0IJhWbTAyYakMfQ4LrMHwvpxLtoGgHvLcWFVy8y0NGz2TMvI0u/YJ+7HFeaUx2xYHcUClnC1QYg05N4sQC9hF1NfHyl/ARM3KdfM//2gAMAwEAAgADAAAAEKuSgVcGYqcuo6/UYYyMCjLXuiXNTFrG7MEqLC3/AApTWR/Ma4tXqd+Bu+LQwYgVR7J57rIIUMLscv0I+lYjkjGWuM9JT//EACURAQEBAAIBAwQCAwAAAAAAAAEAESExEEFhcVGBsdGRwSDh8P/aAAgBAwEBPxD/AAUPDTenhdkeds8ngdIJwS4QZl2SfSFM2PES8QmxYxINxCcM7XGwslDpFS7DbXiR42a6LHdfWx/Hhvu1ye8eN8RGu5dePyPa4+sIDvq+P3/X2sWdNyLbB2yfA+vXIxW6Nz4PunP6/kbRJqzo4kwep+sQvG6nccs8rZhjmUMWE+B48db0SGPW0SQJjw7S0Hw0taaPdpBsBpFdsxT8DYaxv1lMrhNDJIJwHmfqlz+ke3aA19A/L7f98gZKa7bac7F0bTq7uLrL7f6vQ1+z+ogsS4MbDxsZs/j+p/RuzdXdpZcV+I8Javtcon9oXWWkl5Q8TrM+SZsYHsj467z4vrm78nhY6pLiwslexctuo+PH4g2y7uQyE7Z33e3cvshz+Lv8C65JcnJ6gjzsNra3/8QAJhEBAAIBAwIGAwEAAAAAAAAAAQARIRAxUUFhcYGRsdHhocHwIP/aAAgBAgEBPxA03YQbDkwJDSaSLlCzrp1hDjg3iMpcbIsYYkCWwRCUsYBYEyi0rp3DEFywmZXKDQsYZQChKaEzcwCQgJCY7pnGxpU2lL2h0Gm20zJug2CVWamezHiBuV0934/cyuUpawZbzLUMtpkMAB5enH3EMIFUmJihO14+fRIkCoIUc7SgGdLjlpVDsZZsQo4lVCC4g3g1QKlSjvd3jjpSm5QVm2hzD+cyh8iBpxpFBiAakQ8mPCCGCJoR7pBkCnEG+GmUKx0hLjGiodVTmH0+49/0lCx5X9f3zLN4XGypVipWEQXPeXFL9PiZRvP7Z1KfM+WMh1w1UgagVHLoPX9whCjDQvOg1LlSkokFeZ7xV8IeA2G4nz6Shm9vmPW4E2gR3lwoe6KFN57j3nVh/h0oBvMPWVHb8YvmT8xnXRizaG6zgILoTvz8iOvGm/xMd9LhdIW5jKwgqCEuf//EACYQAQEAAgEDBAIDAQEAAAAAAAERACExQVFhcYGRocHwsdHx4RD/2gAIAQEAAT8Qsibe+NTgb8ZahifOWzdPXDdoRhwJ98FczNJWBffmh85cYzN6OAHxvzg01hcMB9NvXHD4va8YwDvcUREkfL8YYBwL+T85oJ1wAqLqgB5YENbV8b1ViqXQaV6I9S3rN6wWKKWmBWOmJp7bM0QyYH7cYyw1QxlnJrrxgcanVui8Td9sAFBi5oV4G9jOlf4YWgc/8/G1Mk5G22tYwRQBYRobFvHOCOk/Q2yLVkJqDhScnpcaeQ4B0SO2TZ5jrFpDnipOOHVZsa73KOpZxgO7OuM4GtDrmafbGbaIRQj2ymbQMwgj3Pm4UD6zVSPDLiHFQIAQnXm3IjtPUfXg2d85wVN6FZ3TquBlGE0oKq00YrixXtIbZezOuMyHChFwFC6U/jDnApAnWZccqmdL+/OSbnbnZ/vXEDPGRThL7vnBUFK7jeKaAO2Gudf8s3Pl+MFqOK46GgVfE85YNmgICJ5vJK6CdcQ30mUH7MttD4iBFTnlya7Uw9S9IpD6GDKqVKU7nc85fmPjgLxYJdQfHX2wAhsO2Pb6/wDhe/IKPN8Hl57eSiHZVa3z4MYiY0C1HCPTvl0kaeJ34fUObQDvKO1Xa4q1iB8jI0HkWxVr0fouBN2PnUz1OYjtPblAFyK4gEDby0f37ZtxqunGxLitki93LCGxeZjWcevTHr23DvGJPUCJm1U7WqIrNXX24kK7O/Xv93JbLg1rrjnSYFNogdf1rD/oATW+rOgVXsOSnhJYMOoO3fvi0Nh4Gqcvp0wN4LJb6/eUJORrP5wRUzTwHp+MMAX4cPBhJ5lPR/z7we+z2YbAhI2VOD5mJB1QhLeVD9Be2OH7UGwXibeL2XvgGWJKkDVKFbqyTWCT7stMse3N/XNwJgE8Jt1xbZeiw9Ylnt08YcH1V/A9uriga4kpVKbbWf7iZNERkW6XbNTsXm0A5u/XH6G2RpKLUJfrBgWXau0zR2HOh/MxIK5SEXpdr9ZXqRMAAGNFBUQBIcUtDA5RHjTbTRumPwe52oboFeBv2ZyxXtrwpdeWc9MNOhQaHb1wbENAW4JTziEx3xD+sFBoah7AHthxVCjAQNe8w2CLDBwZI7B660Yc3K49NIErurxjg6q4W1Dkeo85TrIU4kcInegFTq1s1Jw3oHt+cYqY4R3ggqOE3z+9MDS7yQT3UxtUKPCOQK6f6wZGcJclJ4ANpYqsmmffEGXsYfjHRp4gWLQKkC7UOuUJcZchCGnWyYxKqB79DgTVAYvR176zQRyhXT0uJbJ3LeuGFwAYKooP4cXgFQWawHNi/RjrQKvGv7cbgG8wx0UPvw5C08Mrterg0CVwMiIaAwfNTcc4GpSlJydcVD475vH17OWg6Mp+gP8AGWZ/avzhKwvnECk7n45sneg5KuTlG3B1I9ySBv1DN6KckR+MZN05RD9L954P69cf5PzkaduXf6S/Oan0k/H9cA6BfJhvkaLQ3PF74BHDgmeW95XYzYgNpynu0h/zihoedv4wRRfNB+TPL8+f/9k=');
INSERT INTO accounts (id, name, surname, patronymic, birthday, email, icq, skype, other, registrationdate, password, image) VALUES (9, 'Терминатор', '', '', '2014-01-31', '2@mail.com', '', '', NULL, '2017-05-30', '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQEhUREhIVFhUVFRcVFRYVFRcVFhgXFhUWFhUWFRYZHSggGR0lGxYXITEhJSkrLy4uGB8zODMvNygtLisBCgoKDg0OGxAQGzUlHyUtLS0rLS0tLS0tLS8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAAAQIEBQYHAwj/xABQEAACAQMCBAMEBQYJCgMJAAABAgMABBESIQUTMUEGIlEHMmFxFCNCgZEzcpKhscEVUnOCsrPR0vA0NUNUYmOTtMLhFlOiJDZ0dYOUw9Px/8QAGAEAAwEBAAAAAAAAAAAAAAAAAAECAwT/xAAoEQACAgICAQQBBAMAAAAAAAAAAQIRITESQQMTIjJRYQRx0fAUM0L/2gAMAwEAAhEDEQA/ALTFIVp9GK77OM8tNOAp+KXFMBoFPAoApwFAABSijFLQAVB41xNLWFpnyQMAKOrMfdUf46A1PxXO/GfiOK7VLeEbLKXaUkEMB5F0jsvmJ+O1TOVIcVbM5xni8903MkbzHPLQe6i9go/edzUrgygsrR20ssuSVZJJCSfURIgYd+rYqDaWwwHbcuPIo2On1PoPj+FW1nKFIVpHVBsVi8oA9Wxt36YY4rly2bvROk4FxII+mFl1/lEQo0mg+9lAWmORsRudvuqhS30Bo2AyAwOM9R2buDv0Nel4tuHIiLEKR74GzHfbAHT5D99JJzA51Etp0rk7nDxl9DN1bTud9wNqGCEtEzFGdORpAyR/j/BFeyjFSLS0Yqp5CZAwPrGBwP5te8VsxOnknPosoP7UFNITZAeMN1GfnUeSxVd1ODjIXO+fkeo3rTScOKpsJYiTg86BXU9iBIpOOvcA5we1O/gbXOOT9ePo8Zl0D3XMkuV33yAFPxBz6GhocRfCXiV1IguWGnHkkY4G32Sf2ZrcYrmfFeGFdQ3GnOoEY6AnJU9DW58Mzs9tGH99FCNnqQMhH+RAO/qprXxyemRNdosSKTFPxSEVsZDMUYp2KMUAMNIacaSgBtKKKUUDCiiikM9MUmK9CteTmpAdQK8g1PBosKPSlFNFOFNMVC0tGKKoRn/HXEDBZuR1ciMYODhvewe3lB/GuUxOO/TAAIGNjn/H3Vt/a1cYSCP1LufuCqP2msXYDVGMsfK40qfTzswX0GcbepNc/kdyo2hhWWPDA0rMVQs+hCFUZPL0j3QN8DHYbAVY8L4bcrzBHE5DHOWgHkODkiRx5Oueo6CrTwr4eeWReWY8jLDXzQY9iWKSQujqOu2T8vXZcG8PW3FI5HW6WcR4UkJPKdRGQA95I6n9DaoqguzmkCxRsXIEjDq2rVGGPUyOPyjf7CZz3IFe11YwlANRLMwOfe2YhpDLtjUxAyASAFAHcm/9oNnDbSiONFbTgan87jPViWBxn0UKox03rMkDuB+iP7KGqGnZd8KsreNQjTI3XA1FOpz7pb9te8dy6OTDCpVTjXzNJPyXQRj7/wAKpLCZC2nShHXJAGD22IzWr4XGH22x/sjH4CrWUSwm427ppfUu2kBlLKB+dHqA698VceFuMw2ivdSszoqQ28jIOboAluXDHT9kcwZPxFWaeDDJGXXsDjO/T4VhON3F7bIklrqGgyfSNIDKVcxqgljx5xkN2NKeEOCydL8UcNtOIWcl1CUcrGzLKhB1KqnKsR12z13FYvh8c0Vwyknlcq31D7KyPECpAPc4xt6D0qy9lU8d0l+VhSGTRy5Ugc/RnLI/1iRdEbbGx6dhVD7QeMAcVt7JVIVJ4HlOd3keJIkO3QKm3zZqzbaaZolijUU004im12nKGKTFLRQA0imkU+kIoA86KUikNAwopKKQyYRUeRakkUxhSaEiFT1anyJXiaho0JCmvUVGQ1IWhEtDxRSikrREHP8A2s2RKwTZ21NFj5jXnP3VmuDW/wBWg7mYeh6Icf0q2/tUz9Cj+Fyv64pR+6spwuQgW0ZI0s3OI3yW1LCPltEcfnmsZ/I0T9p3D2f2C6GlX7LGMEb7DGfuxir9rSK3ilCIiAapdMa6AWPmZyAdySOvwqs9mLA2hHpI2fngZIrQ8Vj1RuB3Qr+O1RJ++ggqhZ8+eILhnYljnvv/AI79fSs7LN36/HGOlbXj/CY4dUtyzJEhxoiAMjkDIVckADpuc7nFWtrwbhMtpz41Z41U53fmZAyV7ebfHpVOLbGmkjnto6MMkg9/e6H1Oe3y9a1Pha4DSADJ64wBvtsQPu71hr+z5chAVYxudLt7u/Qk9x02z0q2sTcWxEjIyrqVdY6AsMqGBGUyAcahvg4qFstrB9M8HlV4Y2XoV2rC8Qu7WG8lhlVlGvWJYydSlsMysPtLnfPUHOKsvBviEPagkHIGxOME/DH41heNz8y7lPfIB+8ZOfxpxhbdkynSwbfwh4dWxe+lRg0dwROjqMAgocrscZ1aj/OFcQ9r0pHGLgqd1aLBHUERRkdK76sqW/DvrHCDk4LEgAGXZeu3VwN6+fPH8pl4vc4G/wBKMYwOvLKx7evu1m0aJnVyc7+u/wCNNpxFNNdxzBSUZpKAHUhpaSgQxqYa9GFeRpFISlpKKBlhSEU6kxTIPJlryaOpBFJik0UmeKJXqopQKcBSoGxRSUtFNEmC9rJ+phGftse3oB++spako0baQSOu+M4bsG2HbYHff1rVe062LGFyyhVDeUnzMc5OF9AAMnYbj1rL8OtjcOI9saWb5dM5HUjb45zWMvkar4nevZMTypznyGQFNiNiCQcMMjII61s773T8q5j7MuFTOzakkjgSJVSTJR+YmBqUgb5GcruvlUHJrdXjXcQIK/SVA6rpjnH3HEch+RTtsaifzsI/66OQe2m6wIIf4zFyO2MMB+tTXl7MLhXtJ4mK45yqAWwxDR+Yj13qL4/uYb6VHeOa3eM8p0mTzqAZGWUxg5eM6yMjcEd8iqSxtrqzgk5cUd1GzIzS27iTl+UqRp06o9QbBLr2GO9NyqVoajcaG8RjEl3KyZYRB5nkb3m0KCqH5nSN+uomvTwwxknuInHluLWZnBHV44jPHJ+cHXIb4k96JfFSSQNZrBIgkUKZHlaRhuGKqCABlgPlvtUGztrizbmTKyfVTJHqBTXzozCGJYKCihycjqRjvtCduyndUdS9n7arFD6tJ/WMKqbYrNcMRsJJSN/ztIP4CrjgSCx4agfZgrO24xrkYkDPfdgMDNVHhNAJYhqAwwyxG3z3z+ut7wYVbN57RrdZbBkEyQqJrfLuSFAWdRjYHfOMbenzrid3xMHjbSKQyi/l0k7qVaVwGGO2GzX0PacLB/KDUdRY6vMNXQN5s42z0riHtYtjDxqN8E6xA+QPeKyGM/f5P2Vyt+46ksG3NNNOcUyu85RtKKXFKBSAWg0tFMQw15MK9iKQrSGjxor000UiiZRRRTIEIopaSmAUYpaKAEopTSUAYf2gonNheTOjQ4bHXGQCfu1A/dWS4fcG15U7wrKhGmRdRVtsDKSLuh8w3G2++a2ntMBEEbDqGcf+kH/prOGILwV2I96dQD3ABDbemdqxns1Wkdb8Me0OKSJFQltgqpcFYZCQPdSbHKmP6Ld8Gp9949jRlSWCeIPnJdcafjj7Q+K5rjXgHjj/AEhYWP1UsgzAwzCwcqsgZOgBABz6otdvseDQNZlww0shYoQvJUgHI5eMLgjG2Dt1zvWLlFZOiP6eUo7/AKzL+LbFeJov0WWF3jy2ovnSrbYKhTkHHfoVHrWMuPCMkKl7qWGJgM5jDtkAgZOwGdwMfKqrjvEEtLn6syQSJg6lYsMMoYqd9Y64IJPT1qLxXi11eFYGLNhhl5CEDHsAW2UDPr8TVylGWUZPxT8T4SwbrwdDZuRFFevrXcmOOONm6DPNaMuB8NQP75Evhe4mvo5mj0QxnzCaZp5pCpbBLkt5SdJAyNhjA3Fcre0nt5AGDDJAyhWQAnoCyEg7fHet14M8bXCsIHmWZdDBAVLMuBqHnOksAFIwT1IGacZR/wChLxeSbqGSy8W8H4tKTvFJCrBgsXkbboWDncjPQGqW2aaEgSxtGfRxg/cOrfdmupcd4TcrbNIZASF1MNTRqoxuQIipOOpBY1yPj3FJLPmRSBecTpyMLFGCuWZY1A1NpwAW1YJzvij1o9D/AMWfHk9fg6G3jS6SGGFI42ld44xzHHMdJGAYpCpLeVCfM+npnBqolDXiw39wqqkN7JDB5A+uHEccWR3POBOo/H4VlvY5YK11DJtlp30nuBHbynv8XH6Nda8V8OSGG3igARFnLlevl0Ss2M9CXdenrWDVystOlRQ4phFetMNd5yDRThSUtAxaSlpKBBRigU6gBtFLiloA9aKSigBaSiigAooooAKKKKAKLxtr+iSaQDnSrAgHyMwBIz0IOk5+FZbw3Zm+sGslI5kgkMIJABmt3hkEeexeN2xmuhy2iTqYXOlZfq2b0DnTn7s5rmnhKxZbx+GyycqQTDlSD/R3MRKo23UPkL1HVD2rKey46KMS3VmGt5rUhlZiC8bh1bAXquNQHUDpk571a2vjK+KctA5J3OFZsn+My9CfiRXXZ/CPFpAVn4rhTtlEYtt0IOpQDSyeArJUP0i6upVAJKGd1X4+VTqIz6k71i/GpdnR4v1c/DhYOMpYTXAEdy0MClzI88un6Q+diAATJIepCgbnJJrbyX1jPE8tyvKh+lcxNSlwA9tKkauADjUFiO2Rv3q94j4Ut7azHIto0llYIB70gyCTl2JKDy74OBWH8W30dxbyJG5dpb/MLqjaJgqxphQAcMnu4+0NxnNWkorBjLyy8svceV5a20f0O5j0xxNDFDLIFyFuEWQF3074IG5xnGCfjlUjmsnViowM4kU6onVsjKuNsEE98710fwTNbLb28EgU5mmjkZ18sc3JcFCrdT5lI6gjV0wa8oPDscN9cQRSSxKpU64JBGCGUbPGwMbbnpgdKxceWDo8XmfilzQyX2sSSQCKXLjADLpC6wANnkz0O4OFyR6VnrnxHw2UNPcW809yxyFZ9EQJzkNg+4OgHXGATtWrvPBczMJAbacAkGKa2jty/qWlgXJPofL0qI3DYLV+ZLwNQ49wx3BuIdWMDMbSAE5ycGheFxNvL+tXkVJV+1L+CX7GeFMk8EhGzR3E2ATpUOsKRjB7nmNv/smtx4huJHlIbAUE8sA5JTCgsw7HWr/cK8PZ9DcLFJxC9Kq7x6QoAGiKMu5LY21MzMxxt0HavPiMxeQuftYI3z5SoKfqI/E1r417jjm8EammlpK6TASlooFAwooooEKKWkFLQAUlLRQA+ikooAWikooAKKKKACiiigCv8RLm0uB/uZP6Brk9lBO7SXERy1uUlcsx1kFxhxndsNjJz3Fdf4mgaGVT3jcfiprn3HuEvELe4tiSL21CacamJIRXG3q2nttg1nNFJ1g6XJ7V7JmZJElQogLnRnDbagQNlwe+cGsXx72h3DTPLaRIIY0Uu0sas0m+FD+bZNTYAByNyfSszcFIblzKoj5qEDlqCi7khGCtgkHSCMggLvjNXXDrRYuH8S1rgFrZU1pg4MjTKBncHSVwfTfuKnqkKldvs0HjDxE03B4bqNtDXemB86isa+YTpEekeSmMknKDHXNc5seIjn29uGzBbu7jbq5XLsPgSFHyArr3g3gkc3BbWGVQQymVdShgG5zOhKnqNsYyMgkd655wHw3b207zT3du0QwqaJNJZ+amtGjYakAQPuwxsN6lp0mVBxVoqeE8XH0eeyuCQytz7d9wUuUz5TpHfU33j0q9h4zydF1rUfSY1MjMNQSSJysqqDtgsFxnYahvhc0619nF1JdPPlBBzXkQ7Ss6lyVwpIB2IOScdDvXlx7hyBY7TPLSB0RQw162ZtMpZ/dyrzAntvjtUJS2atrRuvCXH7e6GgXCNOSSynKj4LGTgPpGkHG5OTVD4oF5NMUe3cLFk/V6pVY9AwKjfbsQCMnNVPHPDzyy20zHBnj1XBJHleIRq+cbD3lXPfGepqBbXIknlUO4jVXRVwxR2Gogk6sL5mIUEYAC56VfNpZI9ON4OrW13HxG2js4WKJyo2udGGYAAfUDsvQliegAGDqFQ0vFmAlUYVh5VwRpUbKuD0woA+6qLhqSWiwCEaY41hkuSpwSuJJlT1YuQWx6OB6Vd2smtFfbzKDscqR2MZ7pj3T3XBp+PYT0elLiilrcyG0lOoxQAlGKWigAooooAKSlooAWjNJRQAuaKSjNAC0UlLQAUtJS0AeF9+Tfp7jdTpHunq3YfHtT+FWI+jtDFpaTh06XMWNiIJvrSmnqpUmQaT/EX1pt6cRudtkY7jI909R3Hwqs4TxR7bics3M1xQ2gaZNJX6vVGH2+1IurWSOo0jvUS0J/kp/aHZssjwRRwi3kl+lB1IM/1qgsWRjkp5yo0jqoBO1R+C2k1zE8rXMpaXUhDsCh0KUDlcYzpUjfvgbVd8StZJJr+DWGQfRXtmOB9TMzpEqn+KplB9MqT1Ne/AYF+iR3CIF0jfC5DATMZA4Ukr59Rz0IwcHGxFJkyk0jXeGeKKLSGGWIx8tFhVo8yxkRKFySq5Q7ZwwxvsTVR484TaGHni0jlkTAUAONeSAdfKIJAH4YrytvEFiZDBIeTKsA1q+Y01HHMVWYBWIGPmDkd8WF9ZpoSPdQeqqzAEEZI2O43++hKlgSl7rZV+Db+Se30mEWyxSFFWMFUZB5gFDZI677nv67U3tBtec0enKmFieh82oodLEe6uUG/XPbvWia0VBlpDgKRux0jG4O53/7dqrDfwzRoUOoYyzj8mmogbnpqJ6D7zgdc5SajRvGKcrM3e8TkEEr3FmskMjllxNpMLszMdJXzKuASR8PTFV1tyJxDBEriGSNuYCV0tKrSapFIy2gaHfSxGAo23zV3aI6RRQu552mWRmDEEFkaPWpXHdid9vKc7VcLwIWy/RFAWWaMiWRwv1UIA5zkIAEVirYA3xjfJNZpXk1brBmpOLSvCxICxzzosanqMdASOnkVY89tXwrZWWrQC2c9SSMEnudP2PzewxWZ8UyxSWlq8SKkZuXVOZ7vLTXGDNjqpKuzemo+laqBCFVSCCAAQx1MD6O32m+PyrSGGRPQ+iiitzIKBRRSAKKKKYCUUtJQAUlLiigBM0UlGaAFopKWgBaWk+Pp1qluvE8CkrFqndeoiGVH50nuj8aTaQ6su6h3/FoINpJFDHog8zn5IN6yPEPEEjflJeWD/ooPfx/tS/3aom4pp1cpNA6s/vOQfV/+4qH5ClA0vEfFkzA8qMQoQ31kpDPtndYlP7TUiHW9/exOGz/AAY4CyDBUExYC+o3zn4lfs1h5ebjDoupjleYSGzuQQMjAGMbj0rc3Mzfwk51u3M4aigsxJ80SS+XqQMqdvUmiLbJmkjMWnFZEt1MuSDamIjYB4HchVduoCujAEbjNXfsul50UcEkhjAkeKFlIyz6hLyZV9PMSrDuWGdwKztxIq/RGRdRKvG6SD6ssjhtG5xgl2zv1b41KueJCG3cQKIddyGCLrVSBFiRk1t5dMoXbORgelZp5Kkk1Ru/E/AYbl0huU3KSsxQ6WEiglXHZsKSN/l8sw3hiQaUXiVzy9Adc5OFGNP2h0+G3StFxLxjcjhkNy+idZJDFqkRWYDQuFbSw1Nq15Yf/wByFx4iR9Gq05eldIMTPHlO6nVqyNquVPLIgpLC0Xdv4fgeSNp57mYoA6iaQYXO2QoHf1+FXCsELPM+I8KMHJVNm6jGOg2A3b5b1WcO4tEthK7IYmUEI0h1l1BVXEfQrpLLvuNR+4Y1uK3F5PGpdwqo4RB5QocMpZjnDM2dz8MdqzlSNo2anwvxSa8lVEUAySiVdDpzFhVhmMjUCGMZfAxuXB2xU32kXpgAgWQM9wztO4Oc8pl0Qg/xF/WRTfDHDGsra4mt5I5ryQtBDKoEYSFcK7psRqLAgE7Er6DfG8biuWwZ00ugZcEklgVJRzlm74GxpJ4G1k0PFIwvDeHYkCkaJNJjJILtPJrXqJBvvHjJ6d61kMyIiHomMBhkxemlZfdOD8c74rJ+NbvFjEidLV7bOkrlTo2GVbUhy2dxv2NafgDSxxaeaYiskqaNDNAVWVgA6MWx0PU/j1qo/ImXxJwpaZIE95oWi/3towaIn1a3OQP5v4013KqGWSOZSypmPKSAscDVE+/UjoTW1/ZlV6PWkoBB3FFUIKKKKAA0lFFABRRRQBUnjCKxRypKsVLxHmxll94DHmyO4AOPWp1tdxSe46t6gEZHzXqKxVrx8CBHSedVkXDQypHcQBgcZw+nUdsk5J9dxXnxC2YosrQJnfzRubd8DuiSbMcncKT2rLmzTib+qTiviy1tyUyZX/iRDVv8W90fjWNt/Etwh5aXGRjdblSTgjcHJz69Diq+91zahDCq6GYHlDyYzuq7n7RYgk4xt2pereh8K2WHGbma5KPK+qOQNIkIkARArEGNyuMsFCtg70wi6mXSo0xruQmAqjcZPQKOm7aevWqNYJRqXT5lOCCejhsdOmfskd6uJuKNGhiZdxjVkthTnfSnuqc7asbjp1qVT2N2tEvmwoqRyyrMQdoIF8pJ21STbayPTLVZ2aXEuUt05MYPl0jW2RjSeY3lTHqu+e1U9txOGJAVMav3eWJpfzcb/wBteE3je7Gy3CbekIA/DG341VpE02bHh/gm2B1TAyOTl2ZmZmJ65Y4/UBS3sapeQZG8nC1TPXBQaGz67DH4Vk7Xxtd4y836MSfvFXsF4ZZOHS5yZLadGz303Mm2B8htVxkrVETi6yZHjVy6W4iUkaZ5ASO+Aun+jmvf6XzYo5MAsjuxiyAro5XWQO+mQNlfRlPY14+JY8c5emmZX7fbD5/WRUnw1f2f0Sa3eAyXT6fo7ZUaGVnbKMdxsRt3OR3rK6kaV7UzokHDvpnh6OOJAHDuyKMbamJXqdvLIp/CucFjKwj0uJiyx8vQ2S/u4Bxjrtv6/Crzh/tCuohKoWNZJnSRuYMxhuWI3KLkadQWM4zgb7ekSLiSTvqNuokyv5C6lg82Qo0xhXxvjpTddBG1s0HHOHjlSWKnWbe3jhbAJUzQRvc3ITG5OXB271lvC1qkPMupdRhjiGMDBZpDoQDIwd8n+ZnoK2zcQjhW2gW2mgu1nLxKSTqldTl5JZV+sDBsttk4+FVviCGaaGKW5lWWOM8wQxFUVY1ZEcNCEAZ9bHOwyAcEdKiZUMIj+Anld94zgpI6uMj6tcKQVI6Bm1dft79K9ePgvdOM7A2uR6jmrqFenD+LSCNxFiSCwtNa6vKXQqsbgnsrI+wxvoFU9ohW1jkxgyOoGCxPlDsNySeoB60J44jeck/2gcDt+RLcDaYKmrD7NsqnKeoA7Y6V7eJLC6SSO6iPmZWfXFMqy6ZJHkCldg+A2/x1Csx4nDSNcyfRgBzHBlxIcAMRkHWV6Y7d60viLwxPNDDMkOYo7cuTISCI2xKDgfnHtmpk/csFJYE4J42IkxcYLdNZHJcfBgPK33ip/jjjghNsV5basu2SjMoI0qG0nY7kjIHu1gkvrWJlJQuNAJ0nSVY9UYNkHHr6EVL4RyuJ3RiePQmk6dHVVUbFic5bJ+W5q1KVUyOKuzT2XG7Seb6iRoGchMMQEBOlVZj0O+c5Faa14jBKzLHIrFSQcH0OMj1HxFYmL2cKkhZ7g8kA74CvnHcnygCq6zupuHzlLG5juQ/vJo5gwMbMOh+41aco7E0paOpYorA8UfiU+pyTbqdxEJMEnHREG437H1qw4Bc3QdZHhiht3TCiWaTTk5UOZXYsTrG/lwBnYYzT9Qnga2ok/Eokblgl5D0iiUySfor0+ZwKyPE+Ks7iIzSzvkh4LJTFGOmNUpy7L1Bxjp1q7sUvynLj5djEScrCAZyCfdeQdSPUsTT53pC41ss+fP8A6nP/AOj+9RVX/wCFk/1m5/TX+7RTuQYMxFw+1n0aZ8ssZEjIwdUO/JUNMVVtQGCir1PrimWNvPbo8EzssTLHIgdXEcbOdKSHKrpGCd12O2OpxhgQCDq3HQ9CD2ORWg4Z4ouYRoZudEza3Rz5nI3Uc0guoDYbAPX5msVJGjizdcK8JWrKskji5wSQQQIQehwqHDfziak8R8MW4DSwRaJguqMRuY1aRfMmpM6Pex1GKpOD8QguX1wTciYtmQMVjyXQjRArs7SAMABqGwKehpnF+OcUUBAgjYpkrynE2APM240/gOzdMVpca0Rlso7e6uLSVBdxFWXLjIBLHc522bztq69RUrg8dvKp13DpN3I8+o5HUbE526elZK8vZGJ1ltRPmLe9kep615wSHB3HpWHI14nSYOCzpnSUlUHYoArEfFX05+Qbbb4VE4hY2jbS2pjYZy5EkJY9TgEBWON/KD233rK8N49PB7rn458wPfoen3YrSW/jdZFCXCZA05wA6EKcjVC5wcdcghuwParUkQ4sg3fh2LrDIxHXB2IHoPe3+Jx91PhvOTHYhs5hecNt5QruHQ6vmzZHbHxq8D2spaSGeEgqFWMYjZTtpzqAYEdgC3yPSoj2hk1KSSwYgZAdDgjK5wGXqoPlPxwKpOtCqzL+LrxWlbQchlXcdDpxg/qqRwpmjSK5jTGNSjHnHQq+rI6Nlsqc7HIxtTeJGKFIiY0aViZT5MosWAieXJDByC/YbjAwRRwmKUNoCs6uGk5I1Hy6clig7hfNnp07UruVg8Ro8ePxpG6GNsiQCSNepCsWDKzHqVkV137AHvge13JM0gldTLGAiCRkJUoVA3YYYsuQc51DAqfxS3N2sirFpZWeeADCthlVp4yDgFAVLKBvkt1LYqJwDjfEOGgSRZMRwxwNcZydOTnKgg7bjIOxqVh5Htfkt7CciSOVbnMUPnHM+vaAlSMqAyGRMjAz01KSO9VHEImlVpokl0Krc24L7lmyoDAYWNWY4CDJOepqx4j43tLmRZZrCIMI2R+WNAkYhwGkVSM41A4DdQeuwFVxTi95fqiEYgjChIkXQpOyAhF95ug1b79+1TNrouCdZL/w/dJJacSZfKsiwR7+UaVLMyA4IyVjGPUtirfw/al3ggUKyx6yEVwznKYRAM7tuTnpt1qJ4Z8PXS2nJWRVF1zhyZAD9aARHupBVswNGdQwCx2rBLxQpIH0YKE7Zxg9D0pJ1Q2rN/cCbh9ncCVUIvxcRbShyJCda5X7BXzKR3yKnePPEDw8JsYUDYnt4hnWRp0wxalZftZycZ6VjYfDUuzz6oWKCVFZGAbJzkMdm238udhVhx+6ku4raHMMhgACLAWaQjAQGQNsCdI8o33FN4yJNPBnLDhqPGXknEZG2kpq/NAw2ST+qtJ4e4Nd2zNIhWFmXTrnABCnB8kW5z8cEVnkmNrcs0y5McmmaBsqXU5Ei6cYG2fkSK2d/wARWVY0toifqwC3vMwXISXYbMyaS3bOcU4UKV0Rrnh8cjL9InluH1DZ25cfXsqnI+e1Ok4skIKxaUzkMI8qPTSWzk9+pqrnZEBeViwBOeWVY56HMhOjP3k/CqPiPEUJ8nTHbLHffzOwAz+av41bkkJRb2X8niZ03RgvXBIG2evzrP3/ABd5ABrLYZm36b7nb4ktVRJIW3P696aGrFybNFFI0vhjxW9kZjo5nNwcatADAnLdD2PSpN/7QL6TZCkQ/wBhQzfpNn9lZCnrTU3VIHFXZb/+Jr7/AFqX9L/tRVTRRyl9hS+j05hFPUHbPcZpZUGDXpNHhUI+A/VmqoRDWVkbUrFWByGUkEH1BHStXwHxgfLFdbqA68/zvOgkGGKktvvg4+frWRk2O+3zplQpNaG4pnRbnwylzHEUzgwsy3PKZFYqxAVlDNvsQMLvgdM1ipLSSIMSuVDFdY3UkfxT37fLIqR4f4i0JYmNZYsYdZC2AreVtGDgORsDg469q7baSWF/b6VWNYJMRodKqYJMeWC4AyApz5XGeuMkGtKUskW1hnAmuPhTDL8K0vi/wrJYztG4CLny6tXT5gf4yPWqE2w/jIfzX/trNpl2jwE59K0PBb68jia4hchEdYmGvvIrMo0sCunymqb6DkEq2cEbZHcH+yryE8rhbL3lvQD8RFBkfrk/VTimiZNFdJx+fzqj6UkiihcBQNUcQUIvqN1B2IzXQuHcbtnsYowYnfL85JcRyo7Mp5sLAAsrDVkAnGRldjXMorZSoYFteW2yNPlGR8a3HA/DUcyMrTuCVD+QKBs5VfeQtnJ7YzkfCrhZM6LSbh0NrHDMxKufrAPpE3L0Agq6vq8pJxkZ2qntuPabuZbaTlQsAMoA2RyWjKqMHUTgDVtnSrHPaJxjhv0UgOHYEZUtIcbDOdOnT3Hr1P3OtLH6WVkgaOO4GzxSlWWYHYGNSoVjpyGTBJxkZNDedBSaNWba1S5to5YreaSGf6FKzgtzUEWoSSqdtYY41b7DGaxt/wATWFJYo8I8j6mcYIwsgMSxLj6vSA2465HpVnN4ah+nLG7wxow1NidwieUswLtGCu4ICkZGQCKgT+HjJ7pZV1jRGsMrs5J3JfG+lc/gcCl5M4ofj0io4teXDSiVmJlYayR18wDZ26E5ztVZb28kuQgz6kkAfeTWtm8PbtrdgMDyrGSw6jDZI0EY6EdDUO34aEVncosUR6ZAnk1A6QoAPUjcnAAB+9UVeMGz4x4kBtY4UlVG5QL29zE0oV1JJaKRlZCrA42K/wBlZwniQNrIz8lZdarFGuIpGUajlCM5BYgae2Cc9zlLm9GjGmHVsCdOp8fDUT5fh8qk218zBdCxoyjdkQKfeJB9OhxgYGO29PnklQSRScWvmnmeZ/eZsn7hgdfQCl/hObl8oOwjzkqDgE4AyQOuwA39Kn8WQNKxxliRqbbTnSM6QB1zkGogtT6McHBwOnzqDSyC7FupJ+e9NNTXhUdvxb+ynyaZWIVAMgaVBLHUFA2J3OSCcfGkMr6ZWz8A+D3vZGMsErQhGwwyqllOMavhvVj429nzwBJLaAhAp5mZVLZPu4BbPQGq44snlmjndOQ07kNkAAkk4AG5J9AB1ro9n4YtbSxVrlFlurkjlJqBKblVxj4kZ9TgDpSim2NujnemiujfwKf9Uj/4r/8A7qK09ORn6iMFMdjVrw6zWeSKJnCDTqZjjAxtkgsue2wOTVS+SKsOGNHzSZgdHLIBKhvMcYwCD8fxoG9livD49vrAd8Ya3YHGok50yNjYn8K85baLYmFcFsKQcnJBYDfrsprzPLEgKtpVTqLEgKuNwMjbqB8yfur2s7xZd0RCysCxCRBnzq2WKQYODjp8O/RWgop+JrDty9SgE5z7ue+BgHPT1Hxq98LcUe0EckaE5WU3Ck5jlt0zlWT+OPMc/L1NReM8VtJFXEMaSBvMFgEOwUjOtZGDebfGBVlwqYPaQxhVyI7wu7YwVflI2cnIPkJzkbt23pXnANYydKZbXiFtHDK2pJdrSc+/G/a3kbuN9KufVQd2GrlXEOCGNzHJc26yBiNEqtHkZwGEgQx7jBwWBG4NT45+RaOq3SBWn80aLzF5YfySQsG2Oc5BOSM77kHyWPLYkZQiYDSaWU6MMxOiQbkFhkddjuTVbWBL8lP/AAFdEZWNHHYxTQSdi3RHz0UnBFeV5Owt7VSpChpZBnbUWkC59dhHjerLxPZwRg8uRJT5TrEKxnfVqBHU4wvz1VH4hg2dsdOW90HtvLcFvnkgfhUvA+1/eiojwXHYGTG2+zCrzw94lW0jVGWRg6yI5DhSAT5RGRuADgnoTk74qktYi7r2IkUY37An59sVHA1hiMAKc4+DN+4kfjQm0NpPDNVM0ZiSTSrRltUskbMpjbc/R1jc5yVGdRBG4GfKcz7jwZf/AEZ71YVW3AaQjmhnVVyCPNuxG/xyKw8eUIfy5U5AODkjfcelfQthwye4tZQtwREq6ynQM20oO2MZ67YqllEvDOOcG4q0WqT6UV8rKEDhy2oqCoV8qudssR0WvS94zcytqmnZmB2xcgAjIxo0LjIPp0O9dl4P4GWWISrIFDDKhUA26dsfjWU9pfhr6P8ARn1MysZFLEgHbSwGTsNkJ+6isVYLL0c8vdWDIY2KudnDtKyncblkBbJHX1pvCoRJKiOQAxAy6sFUEganzgYHx233zW58MTcy1i1ajoklhO+fcEciZHrh2+eK3HB/DNrOQZAPMuk+pB2Aye/w7U+GLsXPqjnF94Tijtfpcd1E+lgjqsaHkGUHBkGw97CnPTOe2KziXEFvdxMY8wOySHVnTymOklVwD2LZI+QFdSPh+3ksrgSflzDNGNKk5kjic5cgY95Due4G/SuK3l3JLDGXbUVZ1UnqFIV9Py1Mf2VEopMqLsvOL8DeCaaN7iFWR3VULMXIBJU+7pXK4Iyd8ioc/DEXpI0gzuwCxqPnqYmtDx+7W5tLe6wmqSPTIeWjPzrPC4MvvoHhCHbY4O1J7PLm1S/UXarLE8bBOYocAuFZD5unTGfjQ6GVdpHbDYWquQpYuTLPjBHvJGcKN8Z6H4UXNqw8sQ3ZCSYokCDSFkYB1JJwV94eg9ane0wwLxKZbUKiFInAj8oDcsaxgdDjqPhVtZ8dgvOHQWcsQkktx9VqkMa5BYMC43B5bAgbbr1p30FFZcWXEbBYmRjFHcKray6BS246vtkqynA9R6bVPFbOeSZUnmDvIAy5YL5dJIZiQFC7e8PSpXHvElxcwwws3ltZAEJGW0leXtsegUdvXrSPYteSJGszu+kFWeZySCSgxrUaUGDnbuMDfFKrC6yT+AW0VoC0sSOVLB3Vt4juulSw3Zge+ANWexxL4re8pGurgYZjpjVfIV8u0MYGceUqWb/Rg499sBvEOOrrgF7zCkBdWZEIl1jyhnMhInJAQhjgjA6jIrK8Unm4nLqjUKqJhI2kUJHGpGcO2Ad2ySdyWz3FXdYWyKvYz/xRJ/5Ft/wj/eoqL/AUv8e3/wDuYf71FRykXxiS04NO65VEc90WSNpB/Mzk/dmk4NZ8y4EJUZOoEMSh1KrZQ7jSc+pGCBU2TxRdRNpSREVB5Y0hjEQ9Rgrk/Mkn41e+MbRXsYOIomh3ChyO6Sq+AR3w6kA+hq2kQm+yg4nZQrCW0NGdQ8zkspxzAQoBbJDLg77VU8OvI4HZhhwRjqU7g5GR8BVnfcSjeNETmAjYqzMukAbaWGSRudiKgwSL1LEf/VH3nDJ8BWTNFo9bzjIZAkasik6mXmatTbHU2Rvuo79sVbX3iQkIwjWNjAmyhQpAPmOFAALNv0+FePh/w9LeHUdIQK2tpAcDUNmBjXO2AQO/yNR7DhLSRiXUqwk3EYbDHzJE0iDcdDgDqSNzjbNPJLoj8UuidEq50uNRyiIuoMchEUkaQ2ew+VWPDbrZodRLqTyV5hUZbZwqtlTkbYyD02NNS0gW2iDMWlkDnQrKCn1jgZQjLZAUjScV53dmoxLHMpd9TBQpbp1DeXA6kb7daabQNWMvbVm1NlmUxqRqG4AcgLnvjp9xo4if/YoFOfeIB/MeTV/XIfvpyzgDC6FBw6Y1qy5GnBVsgjYkDPfY1J4zw54YuU5RtSpcxsjahpl1RNg/OOLI36U9oO7KGB8nXsCGbrv7seB8zRwJ8y6SFw6MhyBp6ahnO2xUH7qYjgLq75k2/mqAf11EtHCupPZgfwPoalOmimrTJDgAkeU/LBHy2/dX0Z7K5+bZ6Dgl4YyduxiEZ/WhFfPfFIyr6t/NuMj17+nrXbPYnIRHCCdngmz/ADLqXH6mq6q0R9M3/g64VrZEBGVUZAxsG3H7x9xqq9p8Ki0jlZdXKmQ4Azu4aIHGRnBkB6jcA1L8GQxxxMR7ySNEfUIjaFGB+b1Px7VI9oEOvh1yOuI9Y+cbK4/o1F+40WjkXCBiSSPs88M4IwDiWGRGbA295f2V0XhdvKGB1ghWGAUG5z1z1Gf8GufcVBWXUGwXtMjH+7umQZ+P1g3710PwbfhlRWOWA94474x+6uiL9pzyXvJfCLUC7uo2PV22PcSoHI+IGvH3V8x39vyWlgI/JXEifo6k/wCmvqd5OXxDG+JY0Jx01DWoLfhivnT2oWfI4peIBgNMJf8AiAOT+kzVg2bJEOwWXkXVorZGlZl7E6VDNp+cZOah8GfYNtlM5zg+Rtgcd8bj5VL8KQlpDJt5HiVlO+oSPoIHwxmpngf6JHf4vFBgUujgE9SxCd8lQV/XS/IzPzBxJr2JBzjYZA+A9R+2pVupyY1dgNQdcEgHI07/ABw37a7De8V8OxA8iBXbGyqjk57bs+BWYuOMwmUabWFEKnYqGkO4Vm1/J12OcFariLkYi84bKrCHDF2IIGCcjYrgDJ+0f1+la6ysPocMtvpYudQuE1p5m5YeLToJcYIIwGJyxOmrDhot8G7wWXUBGgjLuOW4idwqkrqCyN9r0Ok9s34g4yEZIDHHOBpSRGURltOQqaY2DJgEYIxjsKquKsi+Troz/iSeQzNrDrgk6XGCGPvbHpn4AVK8PPHBcMs0YlTSS0f2WwUcHIz2JPTtVbxy3WOUhF0owDxg4JCOMqrHG7D3T8QaiQzlCGHvDp+HWs7yaNHUf4S8Pf6o/wDwTRXOv4fu/wDzm/Af2UU+SFTGS/a+f9ldW8Tf+7sP8lZ/tooq/sj6OU3n5Q/Jf2Ckj6/49aKKyNDf8Y/zbL/KH+phqLwb/Ndt/LXf9UtJRV9mPT/cor78pH+bH/WNXhb+6fl+9qWik9mi0TE/IxfKf+hU3xB1tv8A5ZH/AMy9FFAdGQn7/Nv21HHWloqGWXHF/dT8xf8AqrtXsh6Wf8jdf8xSUVtLsxXRp/Bv5e+/P/8AyTVd+Kv8ju/5B/6s0UVl2bdHIL/rF/8ABXH/ADUNa3wh/ovu/wCmiiumHxMJ/Iv/ABD/AJZB/JH/AKq4n7cP86y/yMP7KKK53pGy2zP+Eerfnw/1lQU/yiX5yf0zRRT6QnsvrX3V/wAdxVhce8v5j/0o6KKpaJ7Nbffk7L8w/vrCcR/JP+cP6wUUVrIz8fxMrxf8ofm/9a9QKWiuV7OhaCiiikM//9k=');
INSERT INTO accounts (id, name, surname, patronymic, birthday, email, icq, skype, other, registrationdate, password, image) VALUES (7, 'Барт', 'Симпсон', 'Гомерович1', '1988-04-18', 'nick9@bk.ru', '432122', '124321', '', '2017-05-18', '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b', 'data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABGAAD/4QMZaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjAtYzA2MCA2MS4xMzQ3NzcsIDIwMTAvMDIvMTItMTc6MzI6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjBCRDdEQzAwQkExQTExRTFBQjEyQzQ1NTY5ODk0NzZGIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjBCRDdEQkZGQkExQTExRTFBQjEyQzQ1NTY5ODk0NzZGIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzUgV2luZG93cyI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJFMTJBMTBCREU4QkIxRDgzMTZDREM0QTA4OEI2NENFMyIgc3RSZWY6ZG9jdW1lbnRJRD0iRTEyQTEwQkRFOEJCMUQ4MzE2Q0RDNEEwODhCNjRDRTMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAAEAwMDAwMEAwMEBgQDBAYHBQQEBQcIBgYHBgYICggJCQkJCAoKDAwMDAwKDAwNDQwMEREREREUFBQUFBQUFBQUAQQFBQgHCA8KCg8UDg4OFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCAE5AMgDAREAAhEBAxEB/8QAwQABAAICAwEAAAAAAAAAAAAAAAEGBwgCAwUEAQEAAQUBAQAAAAAAAAAAAAAAAQIEBQYHAwgQAAEDBAEDAQUDBgkJBgcAAAECAwQAEQUGByExEkFRYSITCHEyFIFCUoIjFZGhsWJyMyQ0FsHhokNTc7QlF/CSskQ2doOTZIQ1GDgRAAIBAgQCBwUECAYBBQAAAAABAhEDITEEBUESUWFxkSITBoGhscEy0UJSFPDh8WJygiMzksJDUzQVstJzJDUH/9oADAMBAAIRAxEAPwDf6gFAKAg1FRQg27VDqsQjibeopRPB5MM1k/8A2J2SPzHKTLYjDgpGXOlIywSfxDeeQhJMha+34cvkxr/dvY+2rOOutef+Wcl5lOb2faeztS5PM4M2cuOgq8o6HijAv1Dcs7hq34LSeKG48nkebGfzUlUlIdYhYeAlSluOp/SfWn5LI/ON/ZVlrNda0kFO46JtRXa8D1tWndrTgZU4926Jv2ja9ucH+ozcCPM+X0uhxxAK0G17FKrpI91XjrwPLrMHcn/Unnte215nRcSzlND0uZHY5Izr/lZsyXEtuR4ZSQFLjJWHX1dQnt9tle3CzavQsyfjuV5V2fae6sSceemCNkmXmpDKH46w4y6lLjTiTdKkLF0kH3g1ftV7DxbKzjuStHzG65PjnG5lmRueGZEnJ4pHl5tNK8OvlbxJHzE+QSolPralKZcCMS1psKhSqqsUJuKYoZk1OYJqQKAUAoBQCgFAKAUAoBQCgPL2PMMa9gcpn5PWPiokie8O10RWlOqH8CailWgaX8ObHs3C+C1bfM5lZOU493otSt1jzFl04qdlVFcbJNLNihkpW21IR6WCq1TRb/C9rrulnhKLfL1pZ/aZCekatq5wZs3zdvznHvGeY2HGWez0lCMbrjKSLvZTIqDEUJ7g2WoL+wVtM7kYQcngkq16jHxi5Pl6TEeM4twjXFjXF2V/tMJyCY2RkW+NU14l12Um56L+epTose9fOt/eLj3B6yLo+bw9iwp3YG8rSRVjynkWHjXm9jCcU7C7ydJCNs4vP7r2Y/6yb4J/sMloWHkZqPHw6dV39K+gdJqo6u1G5HBSVez9hpd227c+R5ld4zwuc8MlyHuiAN+3R1GQyjZ+IQoiQBEx7ZN7IYbsFe1ZPfoa4f6n3d7hq3CDpbs4Lrf4vcbZt+lVuPizZVta5OkcSaLyFxfg7Pbjj9j/AHbx7jibuFG1I/GRTbuQwVPurV2sAK63tW5qW3W9RedFGFZPrWbNa1Nnlv8AlxRdtd48w2G49Tx3I/tkB+I7Fy8hY8lSX5oUZL6r91LcUpdcQ1m6Xr2s/MydJc1Y54JZI2+3pYwtcnSjo0XmSVpX03ZOZlz+M3TQpMjS24yyVLlZWM4mPj0G5JUXEOMKX+WvoSxqY37Eb6dIyipe40iUGpuHGpQMjq03h/XNb5UjqMreNUnHNbnNJKncmxlylGYStR6kJBCm738Q2APbXNtn9Sz1O7yi/wC3d8Merly7/mZ6/ouTTRdMVmboN5CE7j05QPJTjlMiUJCiEoDCkfM8yT2Hj1rqVK1RrkXhXga54P6ls/nOR8E4nCsROENmyLmuYHPvlQnycilDimZQReyY0hxPymri/Tyv1tVjb3CzPUS08ZJzgk2ug9/JmoKTWD4mzAWm1r+6r+qaqjxeBIcQr7qgfs61LdFVgnyA9e3elQsR5DtejdATUgUAoBQCgFAKAUAoDEv1OZV7D8Cb5KjEiS7jFw2QDYlc1aIwA+35lQ5KKqwlV06SqN6ziZWoJ0/IsCRhXccjGSI6wCFMBkNHoQRcWuPYa+ZZa2cNTzxfjhKtenE392E7St8KGG9Am7fuuya1x3uMR8Y3hIyvxGQe/q8pPUtUfFOo6dm4p8zcnyPWup+ot+g9sg4Z30v5VSrr8DXdHo357rlFmwB6G46X71xfFrs+ZtaxRRNi4k1TaN0xu75IPpn48NpfhNOeMOcYq/mxjLbtZz5KyVN3/wAgrZtHv1/Taeenj9EvvcVXOhjr+jjO5GbL31ULqPW/5a1xt0q8jIuiZR18Wa25ym3yw6VrzrWPGOajkAspcBKRIBvfzDZLX8fesy94ux2+Wlrg5c/sw8Pfj0ljLRxlc8ziXgADp6VhpJ1T6S+briYZXxVnZXNj2ySHk/8ATQusbMMaFCzmzMRvwSXFI6nokqdJ7E9PZW7y35/9R+Uj/c/t1/cpn8vmYV6H/wCRzmVNhwsfZsBlddloDjGWiSITiFdEn8Q2pAv7rkVqdjUu1qY3I4OLj7jKXoKUHF5GGcRyPk+VuLtL4IxMt1GWRDVD5SySL+cPF4h5UJUcK6WdnFsJQO/y7n16d+3neIaLSu8nWU14F0yeK9hpmm0rvXeXgix84wWcRxO65iGExGdYk4nIQm2hYR2cbNZWfC3azaVC9cn9M6uc92tzk8+evXzRZse42l+WajwMt82cozNbwmL1vRPCVyTu94+tNqPwRY6khT+Rf/RajoPkP0l2A9bdu1ustaSy71x0jH39XtNSs25XWqGMuGccrhjlaBx7HyUnJaxyBjpE1Dk5wuvf4lxCA5LeClEkCSyfmKT7QLe/W/Te+S3HzOdUnF+zllXl7i+1+kdhqnE2F37kHVeNdcf2fb5wh45ohpltILkiTIX9xiO0n4nHVnolKftNgCa2zNNyfKqfozHdh4fFHLmJ5SjZIMY2br+fwryGctr2XQlqdHS+j5jDikpKgUOp6pIPcEdxVNm7buLmtyUlwp1ZhwazRkavcpJqQKAUAoBQCgFAKAwP9Vr4f0fW9cBBc2Ha8JCLJ/PZbk/iXRb2eDRvVhuN1WdLduPKMWe1iPNciuln3qACj4j4T937K+X80m8zoSTw6gB36C5t5EdCbCwv7be+vSc5zjyydUQopYoAH1o5Y1KnngSAbfy15vFU4AWNTV8Q0LUoq1ZGItUOtSWiAmwqpOkuZZh4skXHY9R2Pvoqcev9QaqeNhNS1vW5OUm4PGsQJebkGZlXWU2U++RbyVcm1vRIsLknuTe7va29fjbjclWNtUX6dJ427MLbbiqVPj5D1+RteibJrUYAysrjZMNgEhI+a82Uo6noOtu9XW06jydVC5+CVfZ+w89Ra57coriVnivT9ohtL3Xkl1uTyNkokfHuIa/qMdjYSEtsxGLlVvLx+a6QfjWet7Vn/VO+rX3FbhVWoe99PZ1GO27R+V9WZ0cu57F6VmuMt7y8j8FjsJtTLc6b8RDUObEkNvk+NzayQCAOtZL0Hca1V2LaxiviU7tFuKZ9OvM5rlTaG+Yt4jLixkIW3x9rMhPTHY5z/wA48k9PxckfET+YiwHuuPVfqGc5fltO/CsJSTzf4V+mJ5bZoly88z1YziMF9Rug5GMoNPbZis1hMiE3H4hOOaRPYKwD8SkfEElVyE3FXfoHUN2rtpuvK08f3q1+Bb7vbUaSXE2Z8wLXrqaxMCkSFA0dUQcqkCgFAKAUAoCD2oDXvn105HlDh/Xm+pYnZXPvp6W+XAhFkGx9in61j1Rd8vbbr6aLvZf6CPNfie4OtjXzys0jenxJFQCaEigFAKAUAoBQCgINI4OpFSLe2o5aurDZ5Owaxr+1w2IOx49rJQo0lqayxIBUgSI5u2uwIvbrcHoR0PSr7Sau7YlK5ZfLJrlfYeN22p4M9a5V1PrVinXw8K1Z6xioqiMecgTouvb3xNts99mJBxuxuQpUyQtLTTTWUx8hlSlLWQEpsnqSfSuk+hJL8zdh0xT7ma/vKcoRp0lln/UTsW1uvNcK6h+/8O2tbP8Ai/NSDi8Q4tHQmMkoU9ISD6pSlJ9DXRNz37RaGVLsqy6I4v20yMHa0V279KPo1PlzkrHb3gNS5Qx+FMDbRIjYfJ4BySAxkYjRkFiQiT1IcbB8FJ/OTao2vfNPrpTVrm8NM10k39HcsfUZ9TWxYllWpyqQKAUAoBUPIHE9utGwa3b48cr9TkCOTdGs6c7IA/n5id8o/l8WK0D1tfcdFCH45Y+xVMztME7zb4LAtQFhb2VxGtJVRuGZIoySaAUAoBQCgFAKAUBFut6AEXo8UB4i1qPOqwIpjUWFrU6esUxqePsup63uOPTidpxkfLY1DyJKI0tAcQHmr+KwD6gEj7CR2NXmm1l3TS5rUnB0p4cDxu2I3VSR6rbLTDLbDDaWmGkpbaZQkJQhCegCUjoAKtpzlOVZNtvi8z0UUssCp6jCO7/UGguXOE4xxgkpSL+C83nkqQCTa12oyVfD7Vg12b0XolDTSvP6rjp7I/rZqe63pSucvA2VT2rophDlQCgFAKAg9qEMgm1UtkvBGsOPX+9ee+WswslQxpweBjL6ePg1DMtwA/zVvda5T6+vKtm2uFWbLs8MW+ovFcpNlFK1IJoSKAUAoBQCgFAKAUAoBQCgFAQSlPxLNkDqo+wetVRjV04lMnRNnnfSzFXN1PZt/fHk9u2xZHIsOWslcGK5+CilPuKGb/lr6Z2zT+Ro7Vv8MV3tYnPr8+ebZnhNrdKyR4E1IFAKAUAoDhY+tQlgQapcOvKy0bc9uVZSNm23Nz4rtuqobUj8KwL+oSlkiuF+tLvPrkvwwj82bhtEaWm+syTWjGbFAKAUAoBQCgFAKAUAoBQCgFARRKuIKfyrsStU442bPNL+XKiwHkw1f/Vvj5EcfldWgVmNm0/5jXWoLLmTfYniWmqn5dpszNxXqaNG421XUQ38pzEYuLGkI72kBoF7+FwqNfSlMDn9cy4pqpEk0AoBQCgB7UBX9zzp1rUdg2Id8PjZmQF+vWKwt0dP1ahPELJmvXCOL/c/EmnQyPFa8YzMcSQQQ5PvLWCD1uFOm9fN2/X3e192T4Scf8Lob5oYctiPYX6sGXwoBQCgFAKAUAoBQCgFAKAUAqGQce9VyfLEnIx5yXEGy7Bxxx4Pjb2XZI8jIMnqlzG4JByElCv6Xy266F6H0nPqnd4Ri+9/qqYHdrv9KnWbUE967Q34TUmsDkn1qthZHKoJFAKAUANAYV+qnIuxOFM7i4iy3kdldha7CUk2PzcpKbZPs/MKqt71yNu1K5LKEZPuVT0gqzSOuHFagRWILCQhmM0hhtA7JS2kJAHuAFfL1267k5S/FJy7zocFSKR315FYoBQCgFAKAUAoBQCgFAKAUBFAR7DUTl4WiM6lV0OKnZPqUnzFDyjaLrLTKLm6ROz75cKgLdFBljwPXsa7j6K0nlaNzf8AqS+CNP3aaldS6EbJCt+MMTQCgFAKAUBB6C9Aa8/ULLRm964q0JN3G15STs2Sa7D8NhY5Daj9jzyLfZWteo9TGxoblX9S5PbL9GX2ituV+KPeSq48vb1/hr52a5XQ3t50JuKEi9U1AuKkE1IFAKAUAoBQCgFAKAUBB6g1KzCIPUWJt2ufZSCTar0nmsGeX9L0f95w9+39aQFbRs8xMY9/7JiQmC1ZXqkltah9tfS2z6d2NHbtvNRVe14mgaifPdk1lUz7WXPAUAoBQCgFAdUh9mOw7IfcS0wylTjrrhCUIQkXKlE9AABcmhKNWNczKeT+TNg5cZSpWqMRUazpLzqSj8REYcLsyagK6hDzvwIV+chN/dXI/W+5wlTTwdXHGXbwqbFs+mdPMeZkME9gOtcuUaY8WsTZeXGpNlWv4m3ttVNV0v3DDpIuex6Gq+b95JhxJ71TST+rIlV4gUw4Ek0AoBQCgFAKAUAoBQEVAK7vufTq2k7FsdvJeNx0mS0j9JxDSihI96lWArJ7XpvzGot2196VC11Fzkg5dBkbgnVzpnEGl664CJEXFR3JVxZX4iSn57t/f5uGvptKioc/MiVIFAKAUAoBQGDPqrmTRxvj9cjvqiwNtz+K13MSG1eCxjpzx+chKh1BcCPl39ijVprLsrVic45xi2etqKlNJulSnbFvOK06Xj9D1PDvbDuTrKG8NqGHCUrbjtAISp9w/s4zKUj+sc/IDXCNq2HV7tdlO54Y1xnwfYqo2+9rIWIJRVT0sZw9zhuA/G7vvLWlRHOqMBqDDbshr3OZCUlRUr9IJb8b9jXUND6V0NijpzySo+atO7I1y5r7k8nQ9NX0rY6xfa5H3NrK+XmmcMmgkL9T8oslvr7PGs7/ANVpaU8uH+FHh+au/iPJn8S8+6eVP6jucHe4DYUr9z7TGEKaoDqAidEFir0HmhKfaa17WektvvybpySpwrTuLq1uFyGeJ5+s8nxMnm/8GbZipWm8gIR5q13LeI+eASCuHIT+zko6d0dfdbrXMN49NanQNz+u1+JfNVqbFptfG7g8C+39vStYiuevLmZKov7Rb/N3rz5k1UlnmZzY9f1mEcjseUi4mAP/ADE55EdHXtYuEX/JV5ptJd1EuS3Fyk+FDyndjBVk6FEb521LJuFrUsVsG3LB8b4HDy5LZVe1g44hpB7XulRFbTp/SGvufXFQXW0/gzHT3SzHjXv+wl3mmLBSt3OaPueFjNdX5E/AyEttpHUqUWi50A69BVxe9GayGTjLuXxaKI7tZf6fqLFqnJmhbwS3q2fiT5aR5LhBz5UtKb2uuO6Eup6n85Fa5rNl1ulp5ltpPjh8i+t6y1cdEy13N7EWPsrDyUouklQu2L0FRRYkk0BBqqLo0DGvNUdeb1/A6U2VJXuGx4fCLUgkLSyuUmS8oWt0DbCgr3Gtz9GaV3NbzZqPM/kviYbdJ8llr8RtchISfhsE2sAOgA9K7wjTkdlSBQCgFAKAUBgH6oMmnPa5j+HMDE/ePIe7yGv3E2lfy/3ciC6h5eTdWk+SEMePQ/nK6D2VDgpKjyYLzxRxFrnFWJcZgKVktnydnti2aX8c7ISu6lrWbkIBv4Ng+KR/DXnCMYLlWBU25ZmQ0i1VxfBlNKHKqgcFJBPX+GqJpNYihS+TeLdT5X1xWv7OwrzaV8/GZSOflToEpPVD8d0dUKBHbsrsapklKNHiiqLcTB2iZzZsdm8nxTyItK+QNebS+xkEJ8GsziHFeDE9r08ung+kfdWPttw71Tsb0VzzrUf6Us+lPo/h6Dbdv1quR5ZcDoe2zcuQs5M0vheIw+7BWY+e3nIJK8NjXBbyaZCf71JTe/gn4Un73r45DYfScrsFd1S8EqNL70v4sMF0U4HjrNw5HSDMi6b9NWgYOY3sO2h7fN1td7O7GRLCVm/l+HjEfJZT17JR7Ot66zpdLDTwUIJRS4LI1q5clLFupmNllmO2hhhKWWWx4oabAShKR2AAsAKu0m10FFDst7DVWRT7DHu+cI8Y8itH/EevsfvMKDjGagpELJsupN0uNyWfFYUD2uTXjOCfArUmYU2DHcmcDoM3PSH9/wCJGQA9mw2DsOIZuB5ykNgJksJH3nEjySBcjp10HePSNm/F3LHgl+H7svsMzpNzkpcs8i/Y7IwMvj42VxclubjZjaX4ktlXm060sXCkqHcf9u9ccvWLlqbVyLi1weBtcLsZYo+kXrwbrkejOVAKApP4c5/6heOMJbyY17H5jaZaFdUqKkJxse49qVuqUmusegNN4bt58aRXsrX5Gr71cyibOCuqpUNdJqQKAUAoBQHVJksQ4z0uU4lqLHQp191Z8UobQCpSifQAC5oDA/09Y9/d8jsX1CZ5pf7y3J9yFq7TqQDD1iC4puMhCT1Sp9SS67+kbHtQGfPECoogTapAoCLUAsKigNYfqiwI3PdONtJ1GQ/i+UcnKmFnPQ1/Kcha6GinIl0gHySoKSG0n88dPWvK7Yt3Y8s4qS6HiVxm45Gf9M0vXdB1nHalq8RMLC4xoNMNJ+8o91OOK7qWs3UtR6k16KKKD3/EVUCPAXv61TygmwqaAWBoDg402tCm3EhbawUrQseSSkixBB7jrUtVwYNU3teTwhywNRgpLPGfIKnpmrsC/wAjGZtkecqEj0S3ISfmtJ/SHikd65t6y2pXLf5mC8UaKXWuD9nw7DO7XqWpcknhwMj+l/4a40oNV6UbZ1HKhJB7G3elaYhFe4WYTm+aeTdnIKmcJHxOrxVK/MdbaVMlBPuKnW/4K736Q0vk7fCuc3KXezSNxuO5da4I2IA61uZjCaAUAoBQCgMU/Utn3ta4G37KMf1xxL0JHcEHIFMO4I9R87pQF30nX2tT0zXtYYA+VhcbDx6SOxEVhDV/y+N6A+fZORNG06VFg7VsUDDzJn92YmyEMuOXNrgE3tf17UBY2XUPNoeaWHGnEhaFpIUkpULggjobigOygFAKAwBxk2na/qN5b3V4fMZ1xrGafiHFdfBLbX4qYlPoP2yvT20Bn+gFAKAUAoBYHvQGGPqk145jhzNZeHdGc09TO0Yd9F/mNycSr5yvEjrdbXzG/wBare/bjdtyhLJqneVwk4yTXA+DEZVjOYjHZmMU/IycVmY2EG6QJDaXLA+69q+YtTYlZuys/gdH2o3+1Pngpn31bHuR5IQC450bQPJZ/mjqf4qlKroRJ0TZ8H0pxVyOOsluD4Jkbjn8rmg8ru5HVIMeOr7PlMptX09obPk6e3HLljH4HPb0+a431mdk1fZM8SakCgFAKAUBiX6nMHJ2HgPfcbECvxCcWuYgIF1f2BxEs2Hr0aNAX7UM6xtGp4LZYqgqPmMfFntWtbxkspdA6X7eVAajfUx9KvJnJvJD23adNhysflGY7DrOQfUwqF+HQGyEjxV5INvOw63JoDa3jnWJOlaJrmozJf46XhcfHgvy+tnFsNhJUL9bXHT3UBaKAUBB70Bgj6Zkq8uXnXrGWrkjPpdV3VZCIwSFH3dbUBnigOibKZgQ5E6SSmPGaW88oC5CG0lSiAO/QUBqTpP1wwtt5Qx+lu60YuvZqYMfi8kHyqUHHFeLa3Wynx8VW7JNxQG3g7970BNAKAq/I8iPE0DapUpKVR2cRPW6lf3SkR13B91eV1ViyUzAXCjDrHEGjsyCS8nDRPInuPJFwPyA185780twvSWXMze9D/YjXoL5+cKwSxReL6Sp8n51zWuO9pzrKwmTDxkpUW/YvqaKWk/aVlIrL7PZ83W2Yfvx7qlpq5ctiXYZg4k1lGn8Y6frKUFtWMxENl1B7h75KVOf6ZVX0qvpxNDLoKRRBNVAUAoBQCgOmVGjzYz0OW2l6LIbUy+yseSFtuApUlQ9QQbGgMFfTtOc0uTsX0/5t1QyujyVyNcU7fymaxPcU7DdSo281NFSmXLfdIAoDPXkPaOnegK/uO8ajoOGc2DcszFw2IaNjIlLt5K7+DaBdS1fzUJKvdUN0CxyMQL+rPTXFpVidO3XLwFGyMhDwL5jrTewWguKQVAjqDasbLdNJF0d62n/ABx+091p7ryjLuZ3t/Vtxi2rxzOM2fA3FwcpgZrIP2FCF1XDcdLP6bsH/NH7Sl2bizi+4+1n6uPp2eR5DeY7ZH3kPRZzSwfYQuODer5ST4lHK+gqX0v7prGyb5zVF1XJIyOHk7Czn4EpCVtpdRlGLOqSlxKVfA414m6ampDVMzZm49tSQQtKVoUhQCkqBBSRcEH0IoDD2v8A0w8NazvA5AxOBLedadVJiIW84uJHfUblxlkmyVdTb2elAZhHvoCaAi49tAYX+qTYjiOIcxg4SS9nNyU1q2HjIuVuSMsfkqKbfoNfMc/Vrxv3Fbg5PJKpVCDk6LM+PFY9OIxuPxDavNvHxGIgcHZX4dtLd/y+N6+XtRfd+5Ob+9JnQLUOW3FH2eteDwTp1HssjG/MUYZzGaxpB+5t+z4fEv8Aj94RkyPxjyrXvYIj9be21b16Ms8+4KfCMG/gvmYfd58ttLpNsPW9q7nmagTUkCgFAKAUAoCD2qGDWj6pYU//ABDxhO0mQjD8pyMw/FwueV5eDcJmK5IksSEI/rWXPFIUlXtrHblq46TTSvSVVFcD109p3JKK4lp435+xefyCNI5EYRpvKLCQ27i5awiFkD2D2OkLIS8hfcIv5jtY2NTpddY1cI3rU+eD6OHahO1O3KkzHUVlnlTlrbd42JtM3FaXlHtW0/GvDzZiyMf4/jpngr4fmrdNkL9EAeorn3rLd79qUbFhtVXNJ9KeS9xmtp0imnKWVcDJdz9g9lcmjFtuU3Vm0xioqiBuTe9UxuNPNr2lLiuKqcHGGXLfNbQ549R5JCv5auvzt1LCcu9lDtQf3V7jHG1ZRXFXIuv8xobX/hdTP+Gt5QwB+zx8hwLizCkfmx3j8fr4q91dK9F7w5Slp7rxfii65vjHuxMBuumSSkkbWMPsyWWpEdxLsd9KXGXUHyQtChdKgR0IINxXVVgqmtnfXoBQCgIP8dUt0B1lQCfI2AHUkmwsPWoaa6wzVaVnUc0cwHa4d3OOuOjIxmuvnqzkc89ZEuWj0U2wkfJbV1+LqPWudetN2/L2FYtvxzz/AITObXp2587MjDqK4zGNE6m20I99K8rdONCSl/hxnPqF40wwHkjAwc1s8tskEFKmkY5hVvalx0kf5q6x6Dsf3rj4SUV3VfyNY3mdXGPQbO11Y1wUAoBQCgFAKA4XBBI7Cqc6onqNcOSH05/6jtfxg+OPqGsS8k82eyZOYkJjtKH84IZX/DWh+tNV5OhVvjOVO7Ey+1wrdqvun3bRqOs7ljzidpxMbLwL3S1KRcoJFiptYstCrfnIUDXH9Dr7+klz2puKo+ivcbTc08Lq8aMbcYsYfizbdh4pdQMbByk1ef01by1FEtiS22iTHQ44olTkdaAPFR8yk361se7Ru7jZt6uLc5KKhcosmquuHB16DH6SlluEsFXAzJ7j6Vpbgk88TLprgO1VsVJtUc0kSj55sOJkIb+PnsIkwJTamZMZ0eTbjTgKVJUPeDVcL1yC54YSi6rq6zzlbVxNMx7qW65X6b5KNc2kystwU8u2EzwC5UvXVLP91mBIKlxB/q3Rco+77h3T096jt662ozdLvFZV7Ps7jUNdonblVLA2kxWYxWdx8fLYaYzPxctAdizIziXWXUHspK0Egit15kYk+zzFQnUmhPkKqIKzunIOlcfYxeX3TNxMLAQCoKlOBLjlrdGmxdbh9yEk1FSaGvez79vPPkZ7X9Rhy9M4lmJCMnss9Co+aykdR+NqDHPVlpYFlPOdSk9O9aXvPqjT6ODVqSuXOhUaXbQyek2+dx1eRcsFg8VreHh4HBxUwsPj2ksRIqOyG0C3UnuT3JPc9a4fqdTcvXZXZuspG4QtK3FRjwPRsatc40ftPY4m/VPrVcVyussgs6le4haGZ595AziruMa7hsPr0Nzr4oclFydKQPylo13b0bp/K29Ted1uXyXwNI3Kdb7RsXW7GMFAKAUAoBQCgONgBb21CBq5rjxz3L3LO2qBWy1k4WtY943t8nERQX0D7H3VVyP11qV5lu10Vk/bh8jZtlt4Sn0l9Htrl6jXA2Nni7Pqmubljf3Rs+NayUAKDjaHgQttxPZbS0kLbUP0kKBq90mt1Okl/SlSnbR9TXFHhd00LixRSm+J9gw5CNQ5Jz2LhjtCySY+caABNkpVLAcSkA2AC+wrO2t4sXk/P0sJPphFQZaS0s19Epd+BbtRxu2YuE+xtudZ2CUXvKNLYiCCUs2+6tKVKBVf1FYTXXNPO5WxBwjTJuuPci5sxuRj/Uab6iwjtWPLlEHrTmnHhgxkcXGkutrZdSlxlxJQ42sBSFJULEKB6EEdDXqrjttStycX1YFMlGSo0Y4Tw7EwUx3IcabNmeP5Lyy87Ewr6V4lbqhYrVjpAWzc+6wHoK3TQ+sNXpkudxuRXSnzf4qmIu7Val9OB9yR9SkNP4WNyZi5sYE2mT8CgTCn3hh1DV/1a2O367tNV8qVejmRj5bLLhI6XdP5WziSnbOYc6uO4P2sPAxoeFSnr1CH2UqdA+03qz1Prm5Jf0rfK+vH4NHvb2eK+p1OzX+G+P8AX8gM3+7l5nYxYnO5x9zKTypPZXzJBUEq/nISk1qeu9SbjqqRnJRj+7VV95kLGgtW+HfQvvU9611tLLjxMioJZEiqEksnUkVICPiUB2uQL/looqVVzfEhqiPM+lxr8fhN23NaQF7PtuTfbt/scepMBux9n7An8tfTO06fyNHbt/hivtNA1M+a7Jme6ypbCgFAKAUAoBQHzSpLMKK/MlLDcaO2t55w9kobSVKJ+wCoSHA1V4Gbdf46jbHKQUT9rnZHZJqTa/zcnMdcBJHtQEV8++rb61G4To8IpR7s/ebptcOTTrrMl2rVZutOFDLC1Q228yBYUfVgRj0i1CSaAipTfEIW99QqAWqa4AW99QkuJHKhUUXAlJC1TVkioSjxRAPSqo4ujyCzK/uWczGAwD07AYZ7PZta2o0DHMkIQXn1htC3ln7jKSbrV1sKyG36a1qLvJcn5cen9OPQW9+7KC8KqVB3F/UGqMtwbDqf4lxs+UP93TghKlJ+4l4P36dvIo/JWwPU7Janyu3c/iTby40/WWPJdbcq+wtP007jG1XB4jgzcMe5gN+xTDzrBeUhyHmklxbzsiFIR8Lih5eTjZstPe1q7Tt242NZbUrLrHo4r+JZxNTu2Z25UkjYzy62rJJ1qeJyqU6kCpAoBQCgFAYs+orZHdY4R3bJxR5THca5joiB95T+TKYaPEepBeuPsqibUfE8kmVRVadZVdYwzWu65h9fYN2sVBjQUH2iMylq/X2+N6+YNZdd2/cm/vTk+91Og2LfJBRPWqzPcUAoBQCgFAKAUAoBQCgFARRvCiBHxfm9/ZVXKqYPEpouJQ4nJLed5BlaNqsFOVYwQH+LMx+IQ01BW4FfLZaQbqed8k2XayUi/UkWrPf9TK1o43b7UJTr5caeKVGqt9XLiiw/NpzpHhmceX8C/lNKl5XEks7Zq/8Az7WpzYu+zkMf+2SEe50JLSx2IV27V6enN0lpdfCUX4bjUWup/e9hTuFlXbTfFKpsLo2zs7ppmv7bH8fl5vHxp/ijqlKpDSVqSP6KiU19E1qqmkNFhFFgCakCgFAKAUBgb6rB46JrTjtxim9w19eWV+aIold13v8ACHPl+VYvdOb8ndUc+Sf/AIs9rFOdV6T0TYKI7D0r5onVvPHj1HQYuqTQqjnisMyeYA0pxKiaAUAoBQCgFAKAUAoBQEVNOP6P7CBfx69j6H1qmK53hgyKcxizI8WMa5lNa2PjWC3CymLyC05dhT5T+PxORdLk1DrjyiVrSpXzmvNRsrt6VtNrd1etztauTlGUVR0S5XFeHBUzwT95jZaVW5c1tU6ePxMgbHPiYnXsxk5rgbhRIUl59xX3UtttKKifyCsPoJSeptQp4uePdUuNQ07Muxll+m3HysVwPx9CmIU3IThYy1IWLEB4FwfxKFfTVaR9pocjKoqp5lJNVAUAoBQCgK5u2n4PkDVMpp2xNKew2WYLEkIV4OJ6hSHG1WNloUErQfaKp61xJTo6mt8zL71w0s4blWHKzmosfBjORcVHclJWyPupykdsKWy6Ej4nQChXf3nlm8+jpOTu6RKrzi/l9jNh0m6KKSlkXXXtn1zbIIyWr5aLmIBFy/DdS6E3/SAPkn9YCuaazSX9K6XrfL7DYbV+N1VR6wqyzVT1JqCoUAoBQCgFAKAUAIINj0I7ihAoSQfbR1SqswVPd95i6I3jsjl4T69bkPLYy2YZHzGsckou0482m6y2pXwqWnojuay+h216yvI+W4lVQ4zpi/0bxLS/fVlVZYMdk8XloQyWJnx5+OUnzEyK8h5nx+9crQSB09tretY/Uae5awvQ5Z1+lruwPWF6M1VGO8yt7m/NucT6S+ZGsMut/wDUPaY6gqJGhpPkrHsOD4XJL4HgvxuEIJv3rp/pj0/cVxam/GlPpTz/AIvZw4mA3DXxacIm18SNGhxmIURtLMSM2llhlAslDbYCUpA9AALCurNVNaPpqagVIFAKAUAoBQHFaUqQpKkhSVCykkXBB7gioYMR7l9OHF+1zznocF/VNsJ807FrDxxc7z9qw2C06f8AetqryvWbd2NJpSXXiVRuODqilTeK+ftWuvWNrxO845HUQNjiqxeQ8R2QiXD8m1qP6brdabqvR+hvJuFYS9rXc38KGUtbpcjkePJ3nkHW/JO88VZ+Chvq9PwhZz0JCP0vKOpLp/8AlVqmo9C3YJytTUn0PD5tGTtbum/Ej5W+f+KPLwyGccw6x95GXhS4Ck27gh5oWNYCXpPc0/7af80S+juVhrM9WLzJxNOF4u74ZwEhP97bQevb73iasLuw6219VmXsq/gVrX2HxPcZ3PTX0+bGyYpxJ7FM+N/lcrGvQ36U5J5/hl9hcefbazRzVtupN3Lmw4tPj9686MO3/wASj0moUsIS/wAD+wfmIdKPNkcn8bRFeEjb8O2r2Gawf5FGrm3tWrn9MJP+VoS1FuPFd54MznziOKS3H2ZnKyASCxiWX8i7ceniw2usivTWvaXNBRT4uUV8Wi1e42V0vsTZ8jnLr+wR3o2m6FsefS+lTPz5Eb9zQ7OpI+N6WW1ISRf4gk1dW9jVuk71+FpW8aR5bjfsTPOWrdzCEW/d8T5OHkcmaw+vSd6xak4YsuTddyDUn95phx0O+Jx8qV4o81oSpJaWU/EOl69t7hodVFX7EqvKWHLXrSyphj2jSebFvnVOjGpl6tKTSwRlUyaEkLQhxCmnEhbSwUrQoApUCLEEHoQR3qU2silqpgvcOHOOMhyjxtgo+BZhRtkys05qLBW7EalRoUFyS4lbbC0JPkpKSo2ufb1Ndd9Ha3U6qc1dfOoRjStHTP5Gs7paUFHlw+o3G17XMDqmKj4LWsZGxGGii0eDCaSyyi/UkJQALn1NdN4Ymunr1WBQCgFAKAUAoBQCgINQCLGirxDBv6UTB0PwYcwf2uM0/wCn7VtK+n6wNRRPgCuZnjfRc5FfiztaxTpebW2HXIMdaklaSAoEov0vVSwJTNRuJOMeNZGuytK2vTsS7vWmynsZsTEmI05JKvmKWxJ8lgqU0+0pK219j1HpXHPVF7cdJreaN2cbU8Y+J07DZ9utWbkcYxr2YmQ2uGOImSFs6NhAr9IQWCf40mtPjv24czXnz/xMyf5K1XGEe49ONx1x/DV5xdVxTKh2UiCwD/4a8pbvrpZ3pv8AmZMdFajlCPce9GgwYVhCiMxfH7pYabasfd4gVZz1d+f1y5u1tnvG1y5JH0/EeqiT9pq1SiqvlVWeqr1Dr29KpgnGHKmS4oVVGKSJJqK1IQqSSjzWi/8AULw516RkbQ+pPtvjW2gf9Oupf/n8aSvuvCH+Y1jeUlyY9Js4O4/7da6zGprlTnVYFAKAUAoBQCgFAKAUAoBQCgFAYl5U4Sx29z29u13Iu6pybBZLOO2WF1DraT5JjzmfuyGL/mq+JP5p9KsdVpLWog4XFzRf6YdHaetu5O26xKBom3ZHPDK4LZ4CcRvusSUwNjxSVebYWpPm1JYV08mH0fG2ftHpc8E3/ZZbZf5Vjbnin8uOXvNz0OsWojTii5VrRkhQCgFAKAUAoCjznlxvqA4dXb9lI/xNFdX06FWMQ6gflLZrqXoFpef/ACf5jWt6VXD2/I2cTYmuuUNZWKOdCRQCgFAKAUAoBQCgFAKAUAoBQHUq3l2ub/wVQqKqDk0a58lMM4X6j9UntJCF7brOTx8wgdFqwz7UpokeqgHlC/e3TtWietbSloeZ0rGUffhgZfapclyizZab+6/2VxBLpw7TcxeoKWxQmovU0ZIuKjtwAvSq6URU4rdbaaW+8oNMNJK3HXCEISlIuSpRsABXpGDk+WPil+FZ9xDnFKreBrfyFtS+U9rwbXGeVfhRdOekTlbtDF2f3g4z8ppiKoizyR1LxHwlPS9dK2W3PZ7crl5Y3VGkPvJKuLyoWMNFPcJUgqKNfE/pfY8TZjgLlyZyThZuI2llqDyNrLiImxQmOjTyVgliYwD1+U+kE/zVAiuq6XUw1FtXIOqZp+r0s9Ncduao0Ziq7LUUAoBQCgFAKAUAoBQCgFAKAgqtQFa2je9K0hkStx2DH4KO4qyF5GU1G8ifQfMUCaoeVegJNPI1G5J5kTunL+F3TjzCr27S9FjZDGO5CJJaZ/FS8oloPqhpc/rgyhCR6eRV0PY1qfqaVm9Y/KSuKEptP6XLJ+4zu1aO+5O9GHNHtpkW7A878Z51YiPZcYHL38F4rPIONlJXe3iA9ZCz/u1qFcl1PpzV2k5KHmRX3otP3VbM8tTb+9VPsZfmcjjpKfJiZHdTa/k282sdfelRFYDyJ8IP3/AuldhTBheRxjX9ZPjI9nk+2n+VVSrNz8L7mPNieHleRuPsGSnL7ViYSwbFL81hJBP61XtjadVqH/ThJ+yh5z1FuCrJlRnfUTxUwsx8bk3s7NJKWo2GiSJqlqFhYLQj5Y79PJYrLw9M67/UioLrlD/1HitXGX01l/K/sPFl8z73lfJrUdCdgoWLN5LZ5LcVKCPVUSMXXSPsWKuo7LorE+W9f5umMYS90y8taXWaheCHKutorE3Vdi3BSXuTdlkZ5kHyGDhj934dJ6dCw0bu27XcV8XqKyENZb0seXTWlbVfrl45e9VRmNP6ftZ3pO4/bFd1cS0x40eJHbiQ2UR4rKQhlhpIQ2hI7AJFgBWGldlKTdXJyzZtVqEbUaRWCyPCkbC9xfu+B5dghX4LHlOI25hHX8RgpawCsi4uqO54up9w9nSt39K66UZOxLCPDt/WaN6o29zh+YSx4roRvYxIZkstSYy0vR3kJcZdbIUhaFi6VAjoQQbg10qpy47b0JAN6kE0AoBQCgFAKAUAoBQHBSiki1v8tUt0Haa1cu/UBmJWcnca8OraVnIKvkbNuDyA9CxSz3YjpJ8X5XtH3W/XrfxxW5bpa0UE5YyllH9OBmtq2m7rp0jhFZy+S6zA+Y17XdRxOZ3vP/M2TZY8Z2RJz2bV+PlvPkHxA+bdKQVkWQlIFc9/7PVa7VxtOXLFv7p0T/qtJodI7ijzOPirLP2nrcca+dZ0nD4pxPjJDPz5ien94lEvODoB2Uuw91YndtT5+plOOWS7FgZvadP5GmjB9b73X5nvZDE4rLMGNlYMecwR4/LktIdFvYPIG35KsbOpu2XWEnF9KLq7p7d1UuLm7Srq4j43JKm9ejsEm6hHLjAV9obUAayC3nXL/VkY2Wx6F4+VHuIHEPGIN1avDcV+k4HFn/SWaf8Ac63/AHZBbJol/pR7j1cdoulYkk43XsdHJ7lMVo/+JJqie66uapK5LvLi3tekt4xtxXsPeZaajoLcdtDLZ7oaSltP8CQBWOlNydZYmQjFRwRNqpr1FVESAB+Wqc+JVVs+WdkYWMhP5HIyG4uPjJK5Eh1QQhCU9yonpXvZjO5KlpOTf3Vn3HjduxtQcrslGnF4LvMfSs7s/IUGRj9Uw7UPVZzbkd/OZ1JSH2HU+KjGiJIUoEHopwgEeytgs6Wxo7kZ3p81yEq8sKOjWXM3l7DW7+o1Gug4W4UhJU5p1VU/w0rUsWHx3K2PwmNwL3K+dZgYiO3DgNYtEeGEssjxQFqKHVrIHS6lVlb/AKruRl4IRp11/UYu16QsteOck/Z9h7kDauftfucTyUcqykgoibBjY0ouW7JU8yG1gH2jrV1Y9WYf1Yr2P7S3v+j6f25N9v7DIOs/VbMw8hiBzTrQ16O8sNI2zDurn4XzV2+elQ+fGSfRSwoe0gC9bRot20+rwtyXN+F5mpa7ZtTpMZxrHpVafBGyOPyUDKwmMljJTM3HSkB2NLjrS6042rqFIWkkEH2issYRH0hRIqK0zJOQ61UQhQkUAoBQCgIJoDAf1Gcs5XXWonGehPob5D2Zla1zu4xGKB8HZqgPzz1QwPVXX82rDW623pLTuT9nWzIaHb56y4rcfa+hGF9c17HaviI+FxiSmKwCVLWfJ111Zu464rupa1dVE1xTU6q7qbsrtx1cv2YdFPedw02khpbUbVtZe8qnJvlkndX1Fsg/vvLMKkg9SIkC8lw29U3SlJ+2sts78nzb+fJbw/iboYrd63fKs/jnj/DRmQDa5sLD0HsHsrX41Tx41NgzWHCgoSKAUAFAPWgFGFmPWw9f8tRGNRKVDGoaHJuzPLfuvQtak/KZY/1WTyjR+Na/0mmD0SnspX2Vs9yS26wuT+9dSr+7H7ZI1mMHuN+s/wC1bfZzPop1PpMkJFgB6AAfkHatbm28OPx6zZI0+7+w5H7KpS6Svy65siwPenKmUpyizi60082tl9AdZcBS40seSFJV0IKSCCCPbVSag002utVInHnVGk+o8XVdh2XgXIvZrSWF5Tjl9Ze2DSPMn5APVcvGFX3FgXUtnsv7bW6Lsu/yuNWdQ8/pl0rrObb76fUU7unWX1R+w3P1XacJuuvY3atalpnYPKsJkwpKPzkq7pUPzVJPwqSfukWNb3VZcDnri3jxPdT90VVShNak0AoBQCgOKzYUboR1Fa37dsPx3p+Y3TOuFONw8ZUhxCfvur7NtI7/ABOLKUJ95o8BHF0NM9ZYzeTkZPe9wPzN12xwTMnckpix7f2aE37G2GyE2/Srj+/7i9XfcYv+nHJfM7N6f2xaWwpSX9R4t8Sx9PTtWuVdKGzrAoSlfvPmJpu92dewS3U2tYSMk/8ALKT7/lt3/LWwQfl7dT/cue5R+01+fj3Kv4La7+Zl7TfsTWu5pV4GwUpN9ZyqQKAigJoBQCoeKdCVn2lU5FzczCavJ/dR/wCeZJbeLxKR1JlzD8tBF+/iCV291ZnabMLt5c/0x8UuxZmH3O9K3p3yvxy8Mf4nkz1tbwMXWMDj8BEsWYDKWisfnud3Fk+1aiVVaavUSv3ZXJPFv3cF3F1o7EbNpQiqYY9vF956vSrFNrIvaLMUaqUuKZBpQqWGQNj0PapeIWA+3pRPGizeTJyTZPDu1ucQckRNXcdA403+WpqOwro3i9hcF0fLPZLUsjx8f9pa3rXVPTm6vU23ZuOs4e9HJPUm0/lrnnwVIXPczc9IsAK29Gmk1IFAKAUBxX2qMMmQ10Grn1U5n9+7PonFjK7xXH3NpzyAe8bFkJjNqFj0W8ryH9CsJvOt8jSSks2qL2me2PR/mNXBPJPmfsKp3uT1J71xWKO4OnAe2qiCh6Ofx2279mLfArJR8Ygk3/8Ax0YJUB+su599Z/cp8ljT21+FT7zA7cufU3pv8Th3F7rA08dOkzrfHoJqCRQCgHrQAUAqlvlxJWL7Cg7kBkt70XCFY+WxImZp5HqFQmPBgj9dxVbHoIq3pr8+LXIvbj8jXdfW5qrNrokp92BfhWvGxUBqCR7KkpFARQE06+ISr2Hg7lr3+J9dm4lt0sTVpS9j5SSQtiawoOMOpI6gpWB29L1kNv1b02pjdWX3jF7npFqtNK08+BtTwdyGrk/jHB7VLAbzSm1Q87HHQtZOEosyEkel1p8wPQKFdyjJSSa4nCpwcJOL4GRaqKBQCgFAcHACmxqJOiHGppVt81zNfUFyRkJPVWGbxOvwQb3THTFE1zp/OdeJFc89YX5RjaisnX5HRvR1mLdy481Rd9TvFaAo0odFniqDyQn43T4tIupxR9EjqSfcAKhQ86ketfEi9Py6y6EUTiNKntROYd/vOcyE7KPG97l+QpKT9hQlNZ7epKOqhFf6cYx7jCbRCunnJ/fk5d5fKwEeJn3wFSUigFAKAUAqmeRElWhQr/iuZuiroxeueBT06OS5nncfqItWwzVNuXXd/wArMHLxbmn0W/8AMX2sAZ0UAHrQCgFAKUqBUxdCZRqj3fpvzh1blXbOO3lEYvaY6dqwiDfwTKa8Y89Ceth5fs3D7etdd9OatX9Kot428PmjjXqfSOzq3Lhcx+Rthbratj6TVug51WSKAUBCu3a9AaifUtr44y3JvmppIOo7CiNh9zaRb5jMtq6Yc1CO67p/YuAXNgDWvb1tv5yxgqzjWhsmxbl+Uv0k6QlSvsyPAxuTx+Xgs5HFym5cCQnyZkMqC0KB949faO/trkk7Fy1PkmuVrM7Fp70L6c4OqPE5BzTGB0vNznHEoeXEdjxEE2U5IkILbaEDuVEq6Wq52mxK7qIxXTV9ixb7i13O9G3p5ObpVUXa8j7NQxS8JqmEw7gs7Bgx47n9NttIVf8ALVG4XVd1FyadU5OnZUbdbdvTW4tY8qr20PaqxZkEKAUAoBQCgINQyUUGAhX/AFlzirHxODhWPp/WqrY7rT26KX+7/lMBa/59X/t/5i/3BrXE6uiM+1RVFSU1FAKLEqoxU0FGKobo6ENpZiq2vDzcCasruVyydQ3vjrfCUtoxWfZx05R6EwsykxHb+pCLhdvdW5+kr7hflBZSWPajRvVtlTsRn96Lw7OJvf8AFft0rpxympzqoqFAKAhVQwYA+p22QkcW6u4bRMvt8Z2QLXuMdHelJBH9JCaw28XXZ0l2a/Cy500ea4u0xjyBxPoOGi5besdl5XH7sdtyXk52JKVQ3bEm7kF27SlKPwp8Qnqa49t286jUyWnuQjeUn4VJuq9qxSNwvRdhedCXJNLP9uBjDRtbz2wfgtx5Ed/Gz2U3wMBxoMojNK6pkutJ+H8Q4kgm9/D061ntx1NizblY0keTma52sVKnCrrgbXt2ivXnG/q3zTX0r8K92LMmi9upufbWsGz1qKEigFAL0AoBegI6U5uVrrIpzVXQY92xbupblE39xlb2vPQVYnPKZQXHIyUOfOZlFI6lAJKF2+6OtbDo4R1GlnpU/Ep80et0pQ1/WOWn1UdTTwuHLLqxrUvcOZEyMVqfAfblQn0hbEhlQW2tB7FKh0NYK9C5Dw3E1TgzN2Z25rmttOvE7uhFeaSZ7nK9UsUQNVcqaxIbVMR271FIoUXBEEixJ6D1Psr0hzN0inLD6en5lPmcmLou3Aoud5SwOPlM4jApOx5+U+xCjw8epKmESZbgZZTIkn9k0FLIT8Sqzu37Fe1EoqSVqMvxYP2J4s1vcvUGnsfS+eXViu82A4p+nSZGy8bf+YH4+Y2uMr5mE1+PdeHw59FoCx+3kD/arHw/mjsa6hodutaWCUc+LebOX7jut7WTcpYV4cEbEjpWTzZiMkcqkkUAoCFdqhqoZr99Q5I3nhckfD/iKam3fqcW+Qa171H/APXXez5ovdC6XYrpMM8jZQch783qDJ89P01xubnrH9nMzK0+ceMfHopMdJ81j9Pp0Irmm1WVotG9Rh5t1eF9EOlcVXI3vRaT85qUpKsLby6X9h7FYeiaqqnQ0vcKVFaipJFAKAUAoBQCioQyFJCklNgUkEEKFwb+32j3VMZODTi6NfEonBTTUsU/gYu3DR0arhMttWgz5ev5KDHdmnHxF+eNfLY8l3iueSApQHdNrVtOg171N6NrURjNSw5mvEv5szW9w2/8vbdyxKUXHHlr4X7MEXHHaxzb+6IGagu6/tePyERiYwlKnsVICX2kuBPX5qFKsbdLC9WNyW2K7O3LntODaq2pL3KpiLG9atJOSjJdEVR/FnU7l+RYSCvJcY5tHiPjXFfhS0Ej1T4OhVvZ8NeC0+kufTqYf4Z/YZGPqGmdqS9sTrG07NJUljG8e7LJmk2LDsZuIgfa666Emqp7dYiqz1FtL+Z/Iql6jhTC3Jv2HoRsFzZsFkw9cx2qRybKlZyX+MkI/wDt4l0n/v1T5m22I1dx3X0RVPfItLm+ai5hbjyduPwPcgcAt5Twd5J2edsyR1XiIn/KsUT+itDB+Y4n2ea+lWVz1GrbS0tlW3+KVJSr1PgYy5LU3v7069Sql7cT2eSdEwkfiDZtf1fHMYhuNCXkIDMBlDNpeOtKaUPHusqaCfM/F171bbTul57lbuXpykq0xbpjhx7S31Onj5TUUsug2S0LZW9y0jXNsbKbZrGxMgoI+6lchlK1p/VUSPyV9CUNFRYrGj6iF1k1JIoBQEGhJrp9XEibrOt6lyZHgOZKPo+dTk58ZkXV8h+I9G8lH0QFrR5H0rH6rTfmbErUvvKjPSxc8qfN+nQYR4/xUrG6vHfyZUvO5ZbmXzLq+ri509XznCo+1Nwj9WuO7ldV264wwhb8Ef4Y5Hbdns/l9NFvOfiftLT/AJKxv0qhmc32kg3qES1QXqQL0AoBegF6AXoBQCgOiXGTMivw1pC0yW1sqSexDiSnr/DXpZuOLT4p/M8r0OfDg0/gWD6e8g7O4h16O+rylYgSMNIVcElzHvrZP8QFW/qbTO3udxrKS5l7TmWjp5aXGOD9hk8X+ytbx407y+5kyLk9Cq49npVKik6xzJ9g6VL5uNCcQCL9Kj7SGmcXG0uoUysBSHAUKSeoIULEEVXbm4XFLrKZKqa6jq+k2QpPDMDAOK8nNZyWWwKj5FRtBnOpSCT7EkAe6vqTTz57cZdKT70c6kqNozhXuUigFAKAhXahKMJ/Vr//AD3uv+5i/wDGMVRH7/Y/gTDNdphdr+pb/oJ/krgcvrfad/t/8eHYvgdvtrw4l3LgRVRMsx7KED0oB60A9tAP89AKAmgFAG/6wfaP5ahZrtHBe0+v6c//AEJlP/cuZ/4lVXPqz/lx/wDaicts/Vd7WZdrRj3tHAfeNVF4TVcSAnvVcsl2kM5D76PtFQ812lPSeV9Jf/ozcv8A3zsf/EivqLRf8e3/AAx+Bzq79b7WZ9q8PM//2Q==');
INSERT INTO accounts (id, name, surname, patronymic, birthday, email, icq, skype, other, registrationdate, password, image) VALUES (8, 'Мики', '', 'Маус', '2017-06-01', '1@bk.ru', '', '', NULL, '2017-05-31', '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b', 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/4QCARXhpZgAATU0AKgAAAAgABQESAAMAAAABAAEAAAEaAAUAAAABAAAASgEbAAUAAAABAAAAUgEoAAMAAAABAAIAAIdpAAQAAAABAAAAWgAAAAAAAABIAAAAAQAAAEgAAAABAAKgAgAEAAAAAQAAATmgAwAEAAAAAQAAATkAAAAA/9sAQwACAgICAgECAgICAgICAwMGBAMDAwMHBQUEBggHCAgIBwgICQoNCwkJDAoICAsPCwwNDg4ODgkLEBEPDhENDg4O/9sAQwECAgIDAwMGBAQGDgkICQ4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4O/8AAEQgBOQE5AwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A/fyiiigAooooAKKKKACiiigAooooAKKKKACijOOtFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRSZHqKMj1FAC0UwuoJyRx15qE3druwbiEH0LigCzRTFdWxg59KfQAUUZHrRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUgIIyCCKa7qqEkgYGTz2FfBH7Sv/AAUA+FPwDvbnw1pcY+InxBRcPpOm3AEFnkHBnnwQP9xQW+nUAH3rNLHDbPLLIkcaDLM7AADuSTXzf8RP2uf2e/hlcS23ij4l6AL+Mc2djKbmbPpiPIB+pFfzrfGz9sz47fHO/uYfEXiebQvDxkJh0TRWa3tkU9FbBzIf9psn3HSvlVhudmYlmJySx5J9c85/GgpI/ot1f/gqv+z1YX0kem6P471hF6SLYJGrfTL5/OsW3/4Kz/A6S4C3Hgzx9boT99Yon/TdX89lFA+VH9PvgX/gor+zD43uktZfGNz4Ru5HCJHr1k0AYn/aUsuPckV9m6B4k8P+KdFTVPDms6XrmnvytzYXKTRt/wACUkV/FoVBB/wzn9RXqXwy+NPxP+D3iqLWPh74t1fQp42DGCKbMMo/uuh+Vh16g0C5T+xbI9RS1+T37MH/AAUv8NfEXWdK8FfGa0sPBniudvKh1uJhHp145IChtx/dOfc7D2PQV+rUMsU0SSxSJJGwyjIwYEexHbpzQJqxPRRRQIKKKKACiiigAooo7UAISACSQAOpNBOFJ9KCflNfnV+2z+23pX7PnhZ/BPgp7LWvizf2xaNMiSLRYyPlmmAzlz1SPuRuYAbQwB9H/HL9pz4S/s/+HFuvHniGNdTmTdaaPZkS3k49QmflXP8AEeODX5BfFX/gq38Stc1G4svhV4W0jwjpfIjvdSQ3N0wzx8uQqn86/L7xZ4s8SeOPHmoeJvFmsX2u65eSl7i7u5jI5JJO0EnoM8fpxXOUFxPoXxN+1d+0V4vv5Z9b+LXix/MJPlW115CKPRQgXA+przQ/FL4lm9+0H4g+NjOTkv8A23cDn/vuuFooGfQnhf8Aat/aL8HahFPonxc8XIE6RXN158b+xVw3Ffb/AMLP+CrnxN0O6tbL4p+FNF8X6cHHm3unbra6A7kLkqx/Kvyco/z0zQJo/rS+BH7V3wb/AGhNMH/CD+Io49dSPdcaHfkQ3kYyRkKT8446rnrX0rkHnINfxWaJreseG/FVjrnh/UrzSNZspRLaXdtMUeNhyCCCCOe3pX7pfsc/8FF7Xxhd6T8MvjbcQ2HiRgIdO8USsqR3x7RzDosnIAYcHvk8kJaP2CzzRUUUiSorowZW5Ug5BHqPqOaloEFFFFABRRRQAUUUUAFFFFABnHWo5GVYHY4KgZPI6fjTmwYz6V+WX/BRn9rRvhX8NV+EPge/H/Cc+IrRjqd1bz4fS7Q8duRJJyB32gkdaAPHP26f2+7rT9c1H4RfBHWFWeDMWt+JrRwQj5YNBbkd8DBft2z1r8ULi4ubu+mubu4mu7mZzJNPNIXeRjyWJPJJ7nr+FRO7yXEksrNJK5LM7MWJJxnk+/NNoLiFFFFAwooooAKKKKAEP3Tyw47E5/Sv2Y/4J3ftp6laeItL+AnxV1WbULC5kEfhfWrubc9u2Di0dicshx8hPI6c5G38aKs2V7d6dq9tqFhcSWt9bSrLBNGxDRupyGBHPBFBMj+1xSdw3cHHT+dPr5e/ZB+NkXx1/Ym8L+LJpEOu28X2HWYweVuIwAT/AMCHP1ye9fUNBIUUUUAFFFFABSHpS0h+7QB4N+0h8aNM+A37JPin4gag0Ul7awCLTbZjg3Fy/wAsagd+SD9BX8nXjbxjr/xB+K2u+M/E99LqOuardvcXMsjE4LHO1fRR0x2wPWv1H/4KufF+TW/j34S+D2m3D/2f4ftBqOqgMNr3MwxGhweqoGbB/wCentX5H0FxCiiigYUUUUAFFFFABTkd45kkikaKRWBV1Ygqc9QRzTaPp17UAf0Af8E5v2v3+InhyD4J/ELUS3jXTLXdod/dSgtqluoO6Njn5pUA6j7yg+hr9ZwQRwQa/jA8E+Mde+Hvxc8OeNvC94+na9ot9Hd2c0bEFWRgcH+8p5BHcEiv63/gT8WtH+OH7L/hP4kaOiW66naKby0Em82lyoxLCT/snOM9iD3oJkew0UUUEhRRRQAUUUUAFFFB6UAcJ8TPHmj/AAy+A/inx3rkoi0zRdPkupRnlyo+VR6knAA96/kS+KnxF1z4s/tBeKPiF4imeXUdYvnmCFiVhTPyxr6Kq4H/AOuv2u/4Kx/FmfQfgP4K+E2nXEkc/iW6kvdTEcnW1t9oVWA5+aRx9dh9DX4In7xxj6jvx/8AqoLiFFFFAwooooAKKKKACiiigAo79ie2fWijpz6UAfsz/wAEj/iFLB41+JPwzmkka1mgj1S0BbjcMpJx24Cn8a/c6v5kv+CauuXGk/8ABVDwtZxyGODVNNvLWZM8MPK3r+RQ1/TaDkmgiQtFFFAgooooAKZKwS3dz0VSTT6oaqceGNRb0tnP/jpoGj+SD9qLxbN43/4KFfF/xFLOblH8S3NrbyE5BigkaFAPbCD868FrR1i6lvvGGrXsztJNcXsssjN1Zmckk++TWdQWFFFFABRRRQAUUUUAFFFFAB144Oex71+z3/BJj4uT23ibxv8ABrULh3s7pf7X0pXbIjkVQkqj0DKFP1T3r8Yh94fWvpj9jrx0/wAPP+Cjnwu14u0dq+pizu0Vsbo5lKEH1+9QJ7H9ZwOfrS01e3pinUEBRRRQAUUUUAFNb7hp1Nf/AFZ/WgaP5g/+CjXjSTxd/wAFTvGdkJvNtPDlrbaRAAThSqea49M75mBI9B6V8K17l+01qw13/goZ8atX3bhceMb8qT/dE7hR+AGPwrw2gsKKKKACiiigAooooAKKKKACg9KKP50AfaH/AAT4En/D2v4V7M48273Y9PskuP6V/Uv3r+Z3/gmb4fk1n/gqDouoqD5ej6Pd3THHAJQRj/0I1/TDxknNBMh1FcP4y+JPw/8Ah7o4v/HPjLw34TtWOFfVdRit9/socgsfYA15Bpf7Yf7MmteIxpGn/GfwVJes21RNdNDGT6b5AqfrQSfS+RnGeaKp2V7Z6jpVtf6fd219YXEYlt7i3lEkcqMMhkZSQykHII4q3kZ6igBapaknmeHr6P8AvQOPzBq5kYByMHpTJQHt3XI5XHrQNH8WGvWL6Z441nTpAfNtL+aB8jnKSMp/UVlV9EftZ+DJvAf/AAUa+L2gyWptLdvElxe2iZyDFcO0yEegw5H4V870FhRRRQAUUUUAFFFFABRRRQAVseHb9tK+IegaohIaz1GCcEHGNkit/SselH3gckEGgD+0TwjqA1X4W+G9TDBvtml285IOc74lY/zro68y+DDNJ+yZ8NZXbc7+GrIk/wDbBK9NoMwooooAKKKKACmscIScAe9OprjMTD1FAH8d3x0jeL9tD4rxyI0bp4t1AMrDBB+0ycGvKq+mP2ydBk8Of8FRvjdp7weQkniee8jXGMpcYnUj2xJXzPQaBRRRQAUUUUAFFFFABRRRQAUE4GT070UYBOCSAeDQB+z3/BLTw7ofgz4Y/Ff46eML+20TRotmmR3t5II4o0jHmSsGPuwHHoK5r9p7/gpt4j8Qanf+E/2f5JfD+hbTFc+JbiIfapuoIhU5EY7h+SQeAvU/nT4x+MviHxB8FPDXwu0iafRPh1oqbo9JhbH2u4bBknnI++xPAHQCk+DfwF+KXx48froPw38MXerhHAvdQZfLs7JT/FLL0XjkLjJA4BoA8217xH4i8VeIptX8Ta9rXiLVZ+ZbvU7x55XPqWZifwrFxhjgfOOm0YOfzr91vhp/wSW8L2mkQz/FPxzqmqaiyfvbTRV8mAH2Yjcfrx9K1viV/wAEnPh9feEJZfhj4w13QdbjUmKDVWFzbSn0Yn5l+o6UBc+RP+Cff7Wet/DP9oLTPhl4216e7+HWuytFBJfTkrptzjKspb7sbFcEdsg1+yXxG/a++BngP4KeJfF8XjnQvE39kbUk0/SrtJ7iWR+EQJnPJ4z0GeSBX8wHxQ+F3jf4OfGHUvBPjvS59H1m1Y7WAJjuI8kCWNiOU469a88KgDjHHJGOPXPv7Z9aBNI+/wD4vf8ABR39ob4i+I72Pwnr7fDTww0ubW00qNDdbOwknKlifpt6d+M/NS/tI/tCJqovx8b/AIr+eG3EHxRdlCf9wyFMe23FUvhn8BfjB8ZbmRPhx4C8QeJ7eN9s17FBstoj/tTPhQfbdmvoa+/4Jz/tX2WgS3w8B2V5IiZFtb6rC0p9gCRz+NAI+V/iD8S/GfxT8YweIvHmq/2/4hitltm1F4VSWZF+75m0AMRyM4zXCV1fjLwJ4y+HnjCTQPHPhjWvCurpuP2bUrV4XbBAypIxIvX5lOK5TuR3FAwooooAKKKKACiiigAooooAKs2cD3Wr2lrGCZJpkjQepZgB/Oq1emfBnwzc+Mf2sPh54ZtInnnv9ftowijkgSAn9AaAP65PhpZ/2d+z54HsCu02+g2kZHoRCma7iq1pbpa6bb20a7Y4YljRfQKMAflVmgzCiiigAooooAKQ/dNLQelAH88f/BVv4bz+HP20/DXxFhhI0zxXovlSyBcAXNq21gfcxvFj/dNflnX9Rv7e3wRn+NP7BWuQaRZpdeKfDr/2rowK/MXT/WID6MmRj2Ffy6OjJKUkR43T5Sjj5lxxtP0INBURtFFFBQUUUUAFFFFABRRRQAd6MgHr2z1/WgkAEk4HevsX9j/9kvxH+0x8Y1NylzpXw20mcf27qvTeSCwt4v70jDOcfdHWgC9+yR+xp4w/aV8eRahffavDfwwsplGq6wE+e4GctbW4P3nYfxHKrnPOMH+lP4b/AAw8D/Cb4b2fhLwD4esvDuh2w4ht05kYjl3Ykl2OMliSSfbFa/g3wb4c8BeANN8MeFdItNG0WxiEcFvbxhQABjJ9SepJrq6CGFB6Um4E9R0z1oDA9CD+NAj8w/8Agp58GNM8X/saP8TbS0jTxL4TnSV50QbpLZ3COGI6gFgfxNfnn+wh+xa3x78Wj4hfEGCaL4V6VdGNbUMySavOvOxSOREpI3sD82cAgiv33+NHgL/haH7MXjHwCZI4RrdkLUysM+WC6lm+oAJ+tbfw78C6F8Nfg54e8E+G7OGy0fSLKO2gREAB2rgscdWY8kn1oA1/DnhjQPCPhGx8PeGNG03QNBs4xHaWFhbrDDCo6BVUYH+P1NbzDK4p2aKAPFPjT8Bfhx8dvhVc+GPHmgwXqMC1pfRoBcWUh6SRt2PqO/Ir+YL9pH9n/wAT/s6ftEX/AIN15Gn0yQtPo2pKD5d3b5IDA+o6MPWv6525U96+IP27/wBntPjr+xRrUuj6etz4/wDDcL6loDIuJJymDJbZ65kRSF9GwaCkz+XfvzRStlZGU5DA4x7c9fy/U0lBQUUUUAFFFFABRRR3oAOO5IHfFfpV/wAEwfhNceNf27P+E9u7ISaJ4OtHuBIVJQ3Ui+XGAemQCWx7V+c2j6RqPiDxTp2h6PZz6jqmoXC21pawoWeaRjgIAOTnPav6qv2PP2fbX9nn9kDR/DNxBH/wld+Fv/EFwACTcOgzEGHVU+6PfJ70Cex9Vr0AOc06iiggKKKKACiiigAooooAZIiyQMjqHRhgqehFfzi/8FBf2StQ+Enxnvfih4O0qab4ea/cNNeGGMsNMuGOWVyOisTkHpX9Hh6VzfinwroHjTwBqnhfxRpVrrOhahB5N5aXK7llX0I9e+RzmgEfxeevt1o9PfpX6WftY/8ABPTx78KPFOpeLvhXpt54z+HE0xkW0tg0l9pgJJKsgGWjHQOo+vrX5qujxzOkiPFIOGRlII9iD3oNBtFHbPbOKKACijvRQAUdf6/SilCs7KqDc7HCgdzQB6l8F/hJ4r+OP7RPh/4feE7WWe9v5lNzc8+XZ24P7yZyOiquT7nAHJFf1e/Bf4Q+Evgf8A9E+H3g2y+zaZYRfvZ3A867mP35pSPvMx79gAO1fH3/AATv/ZpX4NfsyJ4z8SWUY8eeK40uZjLH+8tLXGYoQT0yDuPrn2r9GMUEMWkJwKWuN+IXi2x8B/A/xZ4z1GRIrLRdKnvZWY8ARoW/pQI/PL9uP9uu6+BGow/Dn4Xx6fe/EiVPNvr26iWaHSo2HyjYThpWyDhuFHUGvz3+DX/BSr49eFvjtp938TdfTx74Ku7oLqlnJYwQywI3G+Fo1XaV67TkYBAGTmvg3x9411j4i/GjxL431+5lutV1m+kuZWkckqGPC/QDAxXIYBGDyO9BaP7T9B1nTvEXg7SNe0ieO70rUbOO6tJ0+68UihkI/BhVvUdQs9K0K71G/uIrWytojLPNI4VEQcliT0HvXxT/AME8fG0njT/gmD4IFzO091ojy6TIX5YeUQVyfo4A9hXm/wDwU7+MEvw//Ybt/BOk3r2mu+M7s2h8tirCyjANxyOcMHROOzGgg8s8d/8ABWTwZoPx7udD8LfDzUvFHgqznMU+uDUVhml2nDyRQlCGQdsuu7r0r9OPhZ8TvCfxf+DOjeO/Bl+uoaJqMO+Ns/PGwOGjcdVZTkEHuK/je52jJBxj6E+uP6elftb/AMEjviVeSX3xJ+FN7dySWkEMes6XDIR8m5hFMF9smM8f3vegD9u6a/MTA45HccfjTqQ9KAP5Tf22/g03wU/4KDeL9EtbVrXw5rL/ANs6KACE8mdiWUcY+STzFwOgxXyV2zX7+f8ABV34WHXv2avCnxRsbZpb/wAN332W8dEyfss+euP4VcKc9txr8AznB6Bh+h/p/ng0FxLE1pdwW1vNPbzRQzruhkZCFkHfB7/hVevt79jjxN8NvEnxEb4EfG7SbXWfAXiifZot3cALLot+/wAu+OX7yrJ8oYfdyAcCvqz4sf8ABJ7xVZ6ldah8H/GGn6rYk7odL1stFNHk/cEqjBAHr6UDPx1/wzSEgdSBX1l4i/Ye/an8M6hJb3fwj8QajEh/4+dMeO4jb6YbOPqKxdK/Y5/ad1i/S3svg34wiZ2xvuIFhUe5LOOKAPmitTRdE1jxJ4ltdF0DS73WtXunEdvZ2kRkklY8AAAE81+lnwy/4JY/G3xLqtvP8QdU0HwRpBcecqSm5uivcAAAA9uSRX7AfAH9kL4O/s96XHL4W0JdU8TFAJte1MCS5bH9zIxH9FAoJbPlb9hT9hRPhHHZ/Fj4q2lve/Ei4izpmmuPMj0eNh945HNwQSM/wDIHU1+pwBU9z2/+vQBjH+OafQS2FFFFABRRRQAUUUUAFFFFABR2oooAikjSWIq6hgfUZr5G+MH7D/7Pfxlu57/X/BkWja/IP+Qrobm0nzzy2whX6/xKelfX1FAH4veJP+CQWiyXTSeEPjNq1pD/AAw6voiTMPbzI5E/9BrhYf8AgkH4x+1ubn4yeG4YAeGh0iVmI9SCw/nX7t9Bk1xfxE8baR8Ofgn4n8b67MINL0bT5LuZsZJ2rkKPcnA/GgD+ZP8Aa1/Zq8E/sx+IvDfg+x+I99458c3sJutUtV05LaDT4ekefmdizncQMjCrz1r43r0D4p/EXXPix+0D4p+IPiG4ln1HWb+Sfa0jEQoW+SNc/wAKrtA+g968/oLiHf19vWvuD9gn9n8/HH9tfTLnVrdpvBnhdk1LVjtO24ZG/cwZ6YZsZ9ga+H/xx+Ga/pt/4J0/BpPhb+wDo2u31mtv4k8YgaresRlxCci3QnuNh3DH9+gHsfesSLFEkcaLHGq4VVGAB6Adh/KpqKKCAPSvgX/gpN4wvPCn/BLbxZBYTm3udcvbbTDg43RyPmQfiitX30TgZNfk/wD8Fb7uaH9ir4fWqMwjuPGSebg8EC1uCP1AoGj+ffAGcfSgjIx60UZwCT0AzQWf0O/8En53k/YI8UQMSUh8VS7cn+9Gh/pXwt/wVR8by+IP+Cg+m+FElJs/DPh2CLYDlfNnLTM2Ox2ugP8AuivvD/glHZtB/wAE/fEV0c4uPFU+OP7qRj+tfj7+2nrUuu/8FRvjNdSymYQeIJrSJvRIT5age2BQTE+Xhyeenev0P/4JganNYf8ABUewt0ZlTUPDV7BIoPBx5UoB+myvzvxkEGvvz/gmjbtcf8FWvDMgyTDo99KxHYeWF5/Ogb2P6ax1oPSgdaWgg8m+N/w+svif+yl468D3yCSLVNIlhjyudr7SVI984r+Pu/sbjTNdvdNu1KXdpO8EykY2MjFWH5j9K/tbcZjIr+UX9tj4ft8Ov+ClPxI0hUZbO8vP7StNy4/dz/MAMcYzuoKifLlneXOnarbahZzPb3dtIs0Mqkgo6nIII9CK/q6/Y++NkHxz/Ye8KeJ5ZQ+u2cC6frSk4IuIlALf8Cxmv5POe3Wv1Y/4JW/F3/hF/wBp3xD8LdQuGXTPFNqJ7JWbIFzCDwvuyk/XbQEj+g3apPQH6jJpdqgYCqB7CkB6Z5OeafQSFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAHpX5F/wDBVv4wt4f+Avhb4RaXdBL7xHdNd6mEfDLaxYwpx/eY9PQV+uZICknoBX8tn/BQD4hP4/8A+CmnjdUkL6f4fZNHtfnyp8oZcj3LMc/7tA1ufFXftyOSPb/9f6UtFHegs9J+Dnge6+JX7VXw/wDA1mpaTWNbggkIXO2LeGlb6BFY/hX9iOm2FrpWg2Ol2MMVtZWcCwW8Ma4VEQbVAHYAACv5v/8Agl94JTxP/wAFJDrlxbC4t/DOgz3iuw4SdykMf5q8v4g+lf0mgUEyFooooJEPQ56V+XX/AAVg0M6j/wAE/PDesAt/xKvFsErDHaSKaP8Amwr9Rj09K+af2u/hk3xa/wCCevxH8IwQifUzprXunLjJ+0QfvY8fVlx+NA0fyW0hJAyOoqSSKaC7lt50kjnicpLG3BRsnKkeox0rrPAHg/U/iB8a/C/gvRoHudS1jUorWFEUk/MwBPHYDJ/Cgs/pa/4J6+D28I/8Et/ARljeK51gzanKGGD+8faP/HUFfz5/tUo6f8FIPjWHDKf+EvveCP8Apq1f1deBfDFl4G+CfhbwfYhY7PRdKgsYgO4ijVC31JBP1NfzL/8ABQHwfP4P/wCCqXxHWVNsWrypq0DYwHW4G84+jbxn2oJifGHPUc45r9R/+CT/AIbfU/27/FniFkLQaR4TeMNjgPPNFjn1wjV+XB6HnHvX7/f8EnfhrLoX7M/jD4lXaSRzeJtSEForDrb2+5dw+rs/5Cgb2P1qooooIEPSvwQ/4K3eCjYftCfDzx5HEVh1TSXsZ5NuA0kTsVGe52sK/fA9K/K//grD4cbUf2F/CevxQGRtI8TIjsBnYk0Tgn6ZRR+NA1ufzz5wQR2ORXofwl8bXHw3/ab8C+OreRUfRNZguZG55jDjzFOOo27hXnnJHNIenr9aCz+1LQtVttc8H6XrNnMlxa31pHcRSIcqyuocEe2GHNa9fH/7CXxAk+In/BMP4ZajczGfUNMsW0i7Zj8261YxAn3Kqp/EV9gUGYUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAGZrN7Hp3hTU7+VlSK2tZJnYnAAVST/Kv40vG+uT+JvjR4u8SXLF59V1m6vXYnJPmTMw/LOPyr+u743XUtj+x98T72EkSweFr+RCPUW7kV/HQOQDkZ7/1/WguItFFKPvCgZ+0n/BILRUPiL4yeIipL+RZWasV6YMrkZ/EflX7h1+Nf/BIRYx8KPi4QR5n9sW4PPbycj+Zr9lKCZBRR2rmvF3ivRfBPw71fxR4hvUsNG062ae5nPOFA6AdyTwB3JoJNm8v7Kw0q4vb67t7OzgQvNPNIESNR1LMeAB6mvz1+M//AAUm+Afw1uptG8MS3fxW14bkaHQnUWSsONrXDcN/wAMK43xX8Jfj9+2lq0dz431vUPgt8CfOzaeHrfP9paonVZZweBnjCscDH3c1wX7Qvgr4BfsHfstWN94E8A6P4h+J+tyvZ6PqPiBReTIQmXnKsAu1ML0AILKM80DR+NXxh8UW3jr496945sPAyfD+z1yc3aaTFO8kaM3LMjMFyCecYxnoKwfA3j3xX8NPiVZ+L/BWry6L4js9/wBlvY0Vni3KVJXIIBIJGfesvxH4k13xf4xvPEHiXU7nV9YuXJluJmzkdcAdFUcgKoAA9Ohw6Cz65g/bs/auguhIPjBr7bTnbKkTjvgYK4PSvHvi18a/iH8b/FGk658SdWttd1rTrP7Jb3i2UUMnk7iwVigXccknJB615PRQBd0yCzufEdhbajeSafYSzqlxcpEZGiQnBYKOSQO1f1e/sueN/gTrH7M/hrwp8EvFula9oug6ekBto3K3EGByZY3+ZWJz169a/k14/iAYHgg9DX31+zl8ENU+L3g668bfs5eM7zwZ8cfCLJcajotzdGOO5TcNktvJg/KSuGjk+XPBJzQTI/pt3Ake9LXwr+yb+1HrnxP1XUPhV8XtBm8G/Gzw9CBqNrNF5Saig4M0ascgnAJA4PJHHFfdVBIGviL/AIKG6MdY/wCCUnxHYIHewFteKCM4KzopP5Oa+3a+Xf2040k/4JcfGYOoYDQwcfSaI0DR/JzxkgHIz/n+dA6j60i5K5PXANB+6evSgs/fT/gkV4ne9/Ze+J3hSa5Mn9leI4rqCNjyiXEAB4/3oWP1NfrrX4J/8EidYMX7RvxZ0ASAJd+HobwR+phuFTP5TfrX72UES3CiiigQUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAeV/HG3lvP2OfilZwgmWfwpfxpgdzbuBX8dQIZFI7jPFf2q63YpqfhDVNOkjWVLq0khZGHDblK4/Wv41fHPh658KfGvxf4Yu4Wgn0nWbmzZGUqR5cjKOD2wAaConK0dOeRg0+OOWa4jhgjeaeRgsaIMlmJwAB3Oa/ab9jj/gnJZ3+i6d8SPj9pk8qzjzdN8KzjGU4IkuOh5P/LM/jxQUbH/BIaHWbfQ/i4bnStUt9Gnezktb6W2dbeaQB1ZUcjaxAAyAeMiv2nrK0jSNM0LQrTSdF06z0nSrWMR21paQrFFEgHCqqgAD6Vq0EMKxtV0TTdct7eHVbSO9ghnWdYZV3JvU5UkHg4IBwe4FbNFAiFV2YAG1VHoP8/8A66/Dv/gr3per/wDCWfBbVykraELe/t85+VJy0LYP1UHH0r9yT0NfPn7SvwD8PftFfsv6v4B1wra3ZP2nSNREeXsrpA2x/dOSrDuD7UAj+RvrRXrfxj+CfxB+BvxWvvC3jrQ76xMMpW2vzEfs14vaSN8YII7A8dK8k7+/Wg0Cig8deKKADqcevFfor/wS9vtStv8AgqBb29i8n2S78NXqX6Lna0amJlz2yHC4Pua/OknA56+h71+8/wDwS+/Zr1/wXoOt/G7xlps+k32vWQsvD9pcR7ZBaFld5iDyu9lXHsvvQJ7H6L/ED4L6B4x+J/hH4gWIGh+P/Dd4JtO1e3XDPG2RLbygffjdSRz0OK9mjDBQDnipP8aWggQ/dPrivkX9uzV49I/4JU/F6SRgjTabFbxknGS9xEP8a+uj0r84P+CmS+Ntc/Ym0bwJ4G8M6/4jvda1yN71NLtHm8qCFGb5goPBYr1/u0DR/NqPuj1x1+lOre8QeFfE/hTVBaeKPDuteHbo8LFqNm8Bb6bgM9KwfX260Fn6gf8ABJyZ0/4KJ+JolYhJPBNzvHr/AKRbGv6KK/ng/wCCTVjLN+3/AOLb4A+Vb+Cpw3HQtc2wH9fyNf0P0ESCiiigQUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAIxwhI64r+Zf/go98JZ/h1/wUF1fxHbwFNE8XxDUYJAMBZ8BZk/MK341/TQxwhJ7V/PH+23431X9pX/AIKweFfgZ4elJ0fRtSj0eLYoINxK2biXI5JCgY+hoHE6v/gmx+yba+NddPx38faYt54f0+5MfhmzuAGjupkJDzsp6heVA7Nur970XaoAGAOnGK5HwB4O0j4ffBrwx4K0K1js9K0bTorO3jVQOI0C5OOrHGSe5JrsaBMKKKKACiiigApG+7S5HrTHdUjLMwAHJOelAHFeOPh14K+JHhGbQvHPhnSPEmluCPKvoBJtz3UkZU/SvzU+J3/BKD4VeIb2e/8Ahv4t1vwDM77hYzwi9tF/3QzBl/76Nfot4g+Mnwr8LXxt/EPxA8KaTcA4Mc+pIpB9CM8V0/hzxj4U8X6aLvwx4h0fX7bGS9ldpLgepweKAPwZ1f8A4JLfGK1uXXRPH/gnVIM/I1xHNbsfqBuqzoX/AASU+LF5exjxD8RPB+kQkjebO3lujjvjOzmv6AvlPpRxntmgdz84Pgl/wTR+C3wt1q01zxRcXfxL16DDI2pwrHaxuDkFYV449ya/RmCCOCCOKGOOKJECIiLgKo4Cj0AHap6KBNhRRRQAVGc9efapKKAPOPiL8Kfh/wDFTwRd6D488LaV4jsJoyhNzbqZUz/cfqp981/NT+2R+yjqX7M3xwhXT5pdT+H2syPJod66fNCMjNvIf7y54PcV/U4elfHP7dnw00z4j/8ABN7x+t1aiTUtDsW1fTpVTLI8I3MPXBXd+QoA+Cf+CQXhR2vfjF44ZQIkS10mI4ySxLSvz9Fj/Ov29r84f+CXVn4eg/4Jl2tzo+46rceILw64Wxn7QrBUAI7eUIvzr9HqACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA57xZrEfh/wCGfiDXJpPKh0/Tprp5P7oSNmz+GK/n2/4Jv+Hpvid/wVR8T/ETVUS5k0yK61aRn+YCe4lIDDPOQScegr90Pj0SP2KPizgsD/wiOoY29f8Aj2k6V+SH/BIC2iPi/wCMd4Qnnizs4s98bmNAH7jjqOvTvTqKKACiiigAppPynHJ9B3pWOFJ5/Cvjn9r39rHw3+zX8F5ZYXtNW+IGoxOmhaQWzubgebIB/wAs1zk564x1IoA9E+Pn7SPw0/Z6+HT6z431df7QkjJ0/R7Vg13eHtsU9Fz/ABnjg9TxX4A/tA/t8fGn40are6dpGr3HgLwYzER6XpMpjkkUcDzJRy2QcnnGRwB0r5N+IPxG8a/FT4oX/jLx7r154h1+8bMk87Hai9kRc4RB0Cjtk1xVBS0JJ5prm7knuJpLm5f78srF2P1YnJPvmuq8FePvGnw68Xwa74I8S6t4b1SJgwmsrlkDkf3l6MPYiuRooKP2Z+An/BVbUNOs7XQ/jxoEurIoCjxDoyKJT7ywnAJ9Sp/PpX6j+BP2rf2e/iLpEV34b+KvhIM4ybbUL5bKdDjoUlKnP0zX8j/1oBIOQSreo4oE0f2S33xa+F2m2ZuL/wCI/gWyhxnfLr1soI/F+a85uf2sv2cLTUltJ/jH4HE7NtAS/Drn/eX5f1r+SJndhhpHb2Lkj9abgYwcY7r0H8qBcp/Z74X8beD/ABto39p+EvE+geJbEDLT6Zfx3Cp/vFSdv411WRnGRn0r+Nr4b/FTx58JfiRYeKfAfiPUND1S1lVx5cp8qUDgpIvR0IJBB5wa/qT/AGVvjzY/tDfsl6J44jWO21lc2ms2iE4guUA3AZ7HIYexoE0fSdFFFAg7V5v8Y/K/4ZM+J3nCMx/8InqO4OOMfZZOtejnpXyh+2z42g8Df8EzfilfyXYtbu/0ltNsznl5bj93j/vkt+VAHyd/wSQmnf8AY4+JkLs5to/GrGEMfuk2sG4D8ga/WGvzp/4Jg+D5PDX/AATL0/V5UKSeJdbutTGRglQwgX/x2EH8a/RagAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAOW8b6Qmv/B7xTokgLR3+k3FswHcPEy/1r8T/APglJex+Gf2rvjP4CvpPL1BbNURW6s1vMyN+OK/dhhlCOueK/CXXLOX9lz/g4x07WWRrTwh46uy8Uo+WMJdkI6+nyygfTdQB+7dFQxSLLEsiEFGAKEHqCMipqACiimsflPOKAOD+J3xC8P8Aws+BXiTx74muPI0nR7J7iQA4aVgPkjX1Z2woHqa/ks+N3xe8R/G/9o/xB8Q/E05e5v5yLO2ViY7S3B+SKPPRB6DqTn1r9VP+CsHxsvE1Hwf8D9GuzBaeWdU15UYZkc4WCIgDsNz9edw9K/LzwV8Kjq+kJqmvvPbWsuDBaxna7DHBY9q5sTjKWHjzTZ7/AA9w3jc4xHscLC779DxXeueuPrS5HqPzr69g+FngmJMHSGk95Lhzn9cfpU0nwy8FSL/yBo090lcH9DXlf6wUL7H6SvBPOEv4kL9tfzPj2jOenNfT2ofBrwxOhNi99p8vYl/MXPvkZ/WvI/FHw28Q+HrdrxNmp2AJ3TQKSyD1ZeoHvXZQzbDVXa9mfMZ14bZ3lsHUnT5l3Wp57RUkFvc3c3lWkEtxL/djXca2E8K+KJIw6aFqzD1Fsf8ACu6VaCerR8hQy3GVVeFNtejMOitC60TWrNd13pd/Ao7yQEf0rO5A+bIPvTjVhLZmVfB4ij/Fg4+qaF7j61+2/wDwSD8QXL2nxi8LPLIbS3azvYoy2VVn8xGI+uwV+I+4A8H5u2K/Zb/gkDalviN8arsA7U0/T4857l5zVnKz906KKD09KCBG+4a/Fj/gpv8AEK/8d/GH4Y/s1eEJTd6rfaglzqEMbHIklIjhU+wDMxz0GT2r9WfjB8UfDvwe/Z78TePvEtxFFYaXZPKsTyKpuJADsiGe7thQOvNfkl+wP8M/EXx3/bO8b/tZ/E21+0RC+lOgxTx5RriQt8yZH3Yo8IMd2oA/X34WeC7L4dfs7+C/A+nqFtdE0a3slIXG4xxqpY+5IyfUk139NAwcYxinUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAh6V8Dft+fs83Hxl/ZY/4Sfwzbt/wsDwazajpckeRJNGvzSRj3woI96++qa4DRMpGQRgigD5B/Yr+Ptl8df2PNKuLqcJ418Oxppvia0kf97HOi4EhHYOFJGe4b0r7Ar8lfjR8L/F/wCyD+2BcftR/BnSL3U/hrqUpPxG8L2fO2J3DSTIp4ChiGBA+Q+xNfpF8L/ij4L+L3wj0zxt4E1m21nQ72JXDxt+8iYjmOReqODkbTzx70AekVE5CoxPAHWqljqmn6ksjWN3BdCNyj+WwO0jqDimatKYfC2pTLnclrIwx7KaFuJq5/Lp8ctWb4wf8FX/AIk+IZ3N3pVrq7pHlsgRw4jRfzX9DXZxrthUDAAAAAGAB2GK8r8DKbn4s/EvUpcm4l16cMW+9/rZD/WvVl6V8RndaU67j2P688I8qp4bJY1ox96pdt+W36C0UUV4yR+oJ3VwprKGQqVVg3BBHGD1p1FNStsE0nFpq5wFj4UTRPihLqemKItPvISLqEHIEg5DDI4rvgflHPP1pCoJzSgYrSdac2uY4cDleGwfNGjGybvbtcR0Dg5wR2yB/hXKax4K8O67Ay6hpdu0hHEsahXHuDXW0HpgdT0ohWnB3jKzHi8swmJi41qakn0a/qx8heNvh1eeFpjfWzyXmkk4DH78X+97V+z3/BIrwzJa/Az4oeLZEZV1HV4bSJiuNywxkn9XNfm38SNQgsfhdfQugnubzEFvDjJdiQOK/f39jD4XP8JP+CeHw+8O3cCxazdWK6hqeF/5bT/vCv8AwHdj8K+2ynETrUrzP5J8TeH8BlGZqnhnZSV2u2rPqzI68AVDc3MFtp011cTQw28UZkkllcKiKBksWPAAHJPasyDXNLufCo1qO8g/ssxNKLl2AjKDq2emPevy6+Ofxz8f/tU/FC9/Zy/ZXEraEsoj8aePmYpZ28XIaJGxll9WX5mIwOMmvVPze1jxX47eI/Ff7fH7eWmfBf4WTXp+EHhe6EniDWdhW3lcHa8rEZyMArGp5JJOMc1+ynw98CaB8NPg/oPgnwvYx6domlWywQRJ3wMFie5J5z715z+zx+z94O/Z4+BNn4P8LQedeOBLq+qyr+/1Cfks7n+6CTtXsK98oAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAKt7Z2t/pVzZX1tBeWc8bRTwTRh0kRhhlZSCGBBIIIwa/MLxv+zn8VP2afjPffFz9k2I6l4bu8zeJfhxcTf6Pc85ZoF7EDoB83PAxwf1HppUHtzQB+MPgz9ufwrof/AAUQ0e6lg1Pwx4Z8bJHZeMvD2qQPFJoWsAqguhuIGyTIDn1yfUn9k5o0utKniyHjkjKkjuDXxp+1p+yF4F+O/wAC/EV7pXhzTNN+K0FoZtD1u2jFvNNMvzLFKygb1fBT5843Z4615d+wl+1MPG3w7i+CXxSu59I+MfhRXsHttSXyZr6KElAPmOWmQKA/Q9Djk0LcPO5+P8Olz+DP2xfi54H1GOW3v7fX7hxG4wSPNYjg89GU/iK79eVr6w/4KMfs2+KdF+MMf7Snw70+TULLylXxVbwqSYmUBVlKqMlXX5WPYgetfDfhfxxofiTTkeC6it73aDLaysA6H09x1wfwr4/OsDNVPapaM/p/wj4swlTL1gK0uWcNvQ7aio96lM5GPXtWbqGuaTpVm02o6hZ2sQBO6WUD8uefwrwVCbsktT9kq4vD04c1SaS7tmtkYznvijI9a4PSfEuueNfEC6P8MfBHizx9qrsEC6bp8jovPViFJx7nAr33Rf2Lv22vGempeP4a8K+AYJTlY9V1WOOUfVU81h+IFepQyfE1N42Pgc38U8hwLcVU52v5f8zz7vSZHPI4617Ncf8ABOz9seyhE9n4w+HF/JjLQ/2tKPw+a3wa878Q/s0ftq+CSp1L4RDxTajhZtDnjvGf3CwuWX/gSitp5BiFtqeRhPGbJKrtNSj8rnO5Hcjpmqt5d29lYT3N1IkVtEhMrscBRjv/AId6wLvSP2hbXUPsM37PnxEivS20Rtol0cn0wI/616n8O/2IP2oPjn4msW8c6FL8KfBHnK882qr5U+zuI7fJkZu2X2jnrU0MhxE5L2miLzbxgyehh28K3OfRW0+Yn7IXwg1P9pj9uKy8Q6hZSf8ACrfBtwLu7d1Oy5mHMUYGMFmI3H0AHrX7K/tcfG6z+BP7Fuu6tY3CR+LdTt/7N8M2sXzSS3EgCBkA5JQNuHHJAHUitvQtG+Cv7Hn7IAthdWPhTwZo8JkvL27ZTc383ALt3llYkAKo7gAACvif4GR+Lv20P29ZP2gPGOlXOm/BHwZNLb+BNLvbfaL+V12+eVOdxUfMzdNxUDlCK+xo0Y0oKEeh/L2a5riMxxU8TXd5Sdx/hXwP+0T+1R8M/DPhrxBFq/wJ/Z+sdOtoDbRy7da1yONFUq5HKo2CTn05z0r9Gvhd8J/Avwf+Gdl4U8BeHrPQdKhGX8mMB53xgySEfeY9f84r0aNFQKqIiKBgKowBj/PSpe9annhRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFACHlTX5lftnfsa6z478YW/xy+Bs7+HfjDpjLPcLaP5P9pGPGxhjjzRjvw2ea/TamsNyEHoetAH5Kfs+f8FBdPvLp/hH+1Jpn/CG+NLcmwudQ1K12Wt3/Cy3CMMRsc4JPynPpW18XP8Agmx8Gvi1cv45+EHiVfA93qGblBp8gutMnLfNvjCn92Mn+Alfavqj9ob9kH4R/tGaLv8AFely6V4njTbaa/pp2XUP+9/DIOOjCvzCm/Z7/bu/ZD1S4uPgz4ivfiP4Ijw5t9MTzcqOoezlBx16xlsDJ4pNX0ZcJOLunZldf+CWn7Qcd55CfF7wqLQnBkWa73Y9cbea+hvhd/wSr8A6RrcGq/Fnxjq/j24jbcbG1LwW7HPdz85Htx9RXm/hX/gqt4p8MXr6L8aPgrdW2owuVnm0ydrWRMdcwyqQWz23KK+nPDn/AAU+/Zc1qzik1LVPF/haQ4Dpqeiltp78wtJ/n0qFSpx1UDprZhiqsOSdRterPt7wT8O/BPw48HW/h/wR4Y0fwzpUS4EFjbBNxH8TMcsx92JNdsPz9/WviRP+CiX7ITIzH4sKpU/dbQr7cfp+55rmtY/4KY/so6ZbvJa+KvEWulc4Wx0KYFvp5oStDjSfU/QHA9BQcYr8gPEv/BXb4d2vnR+D/hV4t1pukD6rfw2QY56lYxLx+Oa8QuP+Chv7W3xfvrjS/g18J4bGZ8oG0bSZtTniJ/vM6lemecAfSgD94L+/sdN02a81G7trK0hXdNPcSiONB6lmIA/Ovhf45f8ABQb4GfCa0vNM0TVk+Ini0ZSKw0N1liV8cB5c469h9K+C7D9lP9vj9o6+juvjP48vvBOhyDc0Or3wMiqT/DaW5wCB/CxFfe3wK/4J6/A/4M3MGrX1jN8QfFcbbv7V1pAyxt1zHFnavPI6n3oA+OvBfwV/aH/bn+LOk/ED9oB7vwV8HLO4Nxpnh0B4XuEb7qouAW44MjjpnGNxr9frO18KfCz4T6Ro2l2MelaBp8cVlp9laRZPZURF43MTk+p59a7aONYoVSONY0HAVRgAD0HaqWqaTp+s6K+n6nax3lo+CY5B0I6EehHY0Acne/EXwxYG0a6nukguFUibyCUiLSeWFcno28EY9QfQ1FJ8Vvh3CqNN4r0yFGmaJXdmCl1GSucYyBzjPTmtq68G+G702Yu9HtZ1tgPJRl+VdrFwfqGJP1Oapp8PPBqouPD1hlLprlCYwcSsu1nHuRkH2JoA6LSdX0zW9Hh1HSbyHULGUZinhbcjDAPB79f51qVi6DoWneHPDltpGk2y2thBuMcScKpZizEDtlix+pNbVABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABSEZHrS0UAcL4v+GXw98fWD2/jTwV4Y8URsMH+0dNimbHsxXcPwNfMniT/AIJ9fso+I7lpm+GNvosxP39KvZof/HdxX9K+1aKAPzpf/gl7+y80rsLPxxGD/CNbBA+mYzW3pH/BNT9lPS7mOSXwnrmr7eqX2rOVb67Apr77ooA+dPDf7JX7NvhK5hm0P4M+Bop4jlZbrTlunHvmbfzXvWnaTpmk6etppen2Wm2i/dgtoFjjX6KoAFaNFACd+4paKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD//Z');


--
-- TOC entry 3143 (class 0 OID 0)
-- Dependencies: 186
-- Name: accounts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: uotmjnaeeeyngs
--

SELECT pg_catalog.setval('accounts_id_seq', 1, false);


--
-- TOC entry 3114 (class 0 OID 494164)
-- Dependencies: 187
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: uotmjnaeeeyngs
--

INSERT INTO address (id, account_id, country, city, street, house, structure, apartment) VALUES (1, 7, 'Россия', 'Москва', 'Ленина', 1, 0, 0);
INSERT INTO address (id, account_id, country, city, street, house, structure, apartment) VALUES (2, 8, '', '', '', 0, 0, 0);
INSERT INTO address (id, account_id, country, city, street, house, structure, apartment) VALUES (3, 9, '', '', '', 0, 0, 0);
INSERT INTO address (id, account_id, country, city, street, house, structure, apartment) VALUES (4, 10, NULL, NULL, NULL, 0, 0, 0);
INSERT INTO address (id, account_id, country, city, street, house, structure, apartment) VALUES (5, 11, NULL, NULL, NULL, 0, 0, 0);


--
-- TOC entry 3144 (class 0 OID 0)
-- Dependencies: 188
-- Name: address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: uotmjnaeeeyngs
--

SELECT pg_catalog.setval('address_id_seq', 1, false);


--
-- TOC entry 3116 (class 0 OID 494172)
-- Dependencies: 189
-- Data for Name: friends; Type: TABLE DATA; Schema: public; Owner: uotmjnaeeeyngs
--

INSERT INTO friends (id, friend_id, status) VALUES (9, 7, 'CONFIRMED');
INSERT INTO friends (id, friend_id, status) VALUES (11, 7, 'IN');
INSERT INTO friends (id, friend_id, status) VALUES (7, 9, 'CONFIRMED');
INSERT INTO friends (id, friend_id, status) VALUES (7, 11, 'OUT');


--
-- TOC entry 3117 (class 0 OID 494175)
-- Dependencies: 190
-- Data for Name: group_admins; Type: TABLE DATA; Schema: public; Owner: uotmjnaeeeyngs
--

INSERT INTO group_admins (group_id, account_id) VALUES (13, 10);
INSERT INTO group_admins (group_id, account_id) VALUES (13, 7);


--
-- TOC entry 3118 (class 0 OID 494178)
-- Dependencies: 191
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: uotmjnaeeeyngs
--

INSERT INTO groups (id, name, creator, image, description) VALUES (13, 'Sport', 7, 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQEhUREhIVFhUVFRcVFRYVFRcVFhgXFhUWFhUWFRYZHSggGR0lGxYXITEhJSkrLy4uGB8zODMvNygtLisBCgoKDg0OGxAQGzUlHyUtLS0rLS0tLS0tLS8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAAAQIEBQYHAwj/xABQEAACAQMCBAMEBQYJCgMJAAABAgMABBESIQUTMUEGIlEHMmFxFCNCgZEzcpKhscEVUnOCsrPR0vA0NUNUYmOTtMLhFlOiJDZ0dYOUw9Px/8QAGAEAAwEBAAAAAAAAAAAAAAAAAAECAwT/xAAoEQACAgICAQQBBAMAAAAAAAAAAQIRITESQQMTIjJRYQRx0fAUM0L/2gAMAwEAAhEDEQA/ALTFIVp9GK77OM8tNOAp+KXFMBoFPAoApwFAABSijFLQAVB41xNLWFpnyQMAKOrMfdUf46A1PxXO/GfiOK7VLeEbLKXaUkEMB5F0jsvmJ+O1TOVIcVbM5xni8903MkbzHPLQe6i9go/edzUrgygsrR20ssuSVZJJCSfURIgYd+rYqDaWwwHbcuPIo2On1PoPj+FW1nKFIVpHVBsVi8oA9Wxt36YY4rly2bvROk4FxII+mFl1/lEQo0mg+9lAWmORsRudvuqhS30Bo2AyAwOM9R2buDv0Nel4tuHIiLEKR74GzHfbAHT5D99JJzA51Etp0rk7nDxl9DN1bTud9wNqGCEtEzFGdORpAyR/j/BFeyjFSLS0Yqp5CZAwPrGBwP5te8VsxOnknPosoP7UFNITZAeMN1GfnUeSxVd1ODjIXO+fkeo3rTScOKpsJYiTg86BXU9iBIpOOvcA5we1O/gbXOOT9ePo8Zl0D3XMkuV33yAFPxBz6GhocRfCXiV1IguWGnHkkY4G32Sf2ZrcYrmfFeGFdQ3GnOoEY6AnJU9DW58Mzs9tGH99FCNnqQMhH+RAO/qprXxyemRNdosSKTFPxSEVsZDMUYp2KMUAMNIacaSgBtKKKUUDCiiikM9MUmK9CteTmpAdQK8g1PBosKPSlFNFOFNMVC0tGKKoRn/HXEDBZuR1ciMYODhvewe3lB/GuUxOO/TAAIGNjn/H3Vt/a1cYSCP1LufuCqP2msXYDVGMsfK40qfTzswX0GcbepNc/kdyo2hhWWPDA0rMVQs+hCFUZPL0j3QN8DHYbAVY8L4bcrzBHE5DHOWgHkODkiRx5Oueo6CrTwr4eeWReWY8jLDXzQY9iWKSQujqOu2T8vXZcG8PW3FI5HW6WcR4UkJPKdRGQA95I6n9DaoqguzmkCxRsXIEjDq2rVGGPUyOPyjf7CZz3IFe11YwlANRLMwOfe2YhpDLtjUxAyASAFAHcm/9oNnDbSiONFbTgan87jPViWBxn0UKox03rMkDuB+iP7KGqGnZd8KsreNQjTI3XA1FOpz7pb9te8dy6OTDCpVTjXzNJPyXQRj7/wAKpLCZC2nShHXJAGD22IzWr4XGH22x/sjH4CrWUSwm427ppfUu2kBlLKB+dHqA698VceFuMw2ivdSszoqQ28jIOboAluXDHT9kcwZPxFWaeDDJGXXsDjO/T4VhON3F7bIklrqGgyfSNIDKVcxqgljx5xkN2NKeEOCydL8UcNtOIWcl1CUcrGzLKhB1KqnKsR12z13FYvh8c0Vwyknlcq31D7KyPECpAPc4xt6D0qy9lU8d0l+VhSGTRy5Ugc/RnLI/1iRdEbbGx6dhVD7QeMAcVt7JVIVJ4HlOd3keJIkO3QKm3zZqzbaaZolijUU004im12nKGKTFLRQA0imkU+kIoA86KUikNAwopKKQyYRUeRakkUxhSaEiFT1anyJXiaho0JCmvUVGQ1IWhEtDxRSikrREHP8A2s2RKwTZ21NFj5jXnP3VmuDW/wBWg7mYeh6Icf0q2/tUz9Cj+Fyv64pR+6spwuQgW0ZI0s3OI3yW1LCPltEcfnmsZ/I0T9p3D2f2C6GlX7LGMEb7DGfuxir9rSK3ilCIiAapdMa6AWPmZyAdySOvwqs9mLA2hHpI2fngZIrQ8Vj1RuB3Qr+O1RJ++ggqhZ8+eILhnYljnvv/AI79fSs7LN36/HGOlbXj/CY4dUtyzJEhxoiAMjkDIVckADpuc7nFWtrwbhMtpz41Z41U53fmZAyV7ebfHpVOLbGmkjnto6MMkg9/e6H1Oe3y9a1Pha4DSADJ64wBvtsQPu71hr+z5chAVYxudLt7u/Qk9x02z0q2sTcWxEjIyrqVdY6AsMqGBGUyAcahvg4qFstrB9M8HlV4Y2XoV2rC8Qu7WG8lhlVlGvWJYydSlsMysPtLnfPUHOKsvBviEPagkHIGxOME/DH41heNz8y7lPfIB+8ZOfxpxhbdkynSwbfwh4dWxe+lRg0dwROjqMAgocrscZ1aj/OFcQ9r0pHGLgqd1aLBHUERRkdK76sqW/DvrHCDk4LEgAGXZeu3VwN6+fPH8pl4vc4G/wBKMYwOvLKx7evu1m0aJnVyc7+u/wCNNpxFNNdxzBSUZpKAHUhpaSgQxqYa9GFeRpFISlpKKBlhSEU6kxTIPJlryaOpBFJik0UmeKJXqopQKcBSoGxRSUtFNEmC9rJ+phGftse3oB++spako0baQSOu+M4bsG2HbYHff1rVe062LGFyyhVDeUnzMc5OF9AAMnYbj1rL8OtjcOI9saWb5dM5HUjb45zWMvkar4nevZMTypznyGQFNiNiCQcMMjII61s773T8q5j7MuFTOzakkjgSJVSTJR+YmBqUgb5GcruvlUHJrdXjXcQIK/SVA6rpjnH3HEch+RTtsaifzsI/66OQe2m6wIIf4zFyO2MMB+tTXl7MLhXtJ4mK45yqAWwxDR+Yj13qL4/uYb6VHeOa3eM8p0mTzqAZGWUxg5eM6yMjcEd8iqSxtrqzgk5cUd1GzIzS27iTl+UqRp06o9QbBLr2GO9NyqVoajcaG8RjEl3KyZYRB5nkb3m0KCqH5nSN+uomvTwwxknuInHluLWZnBHV44jPHJ+cHXIb4k96JfFSSQNZrBIgkUKZHlaRhuGKqCABlgPlvtUGztrizbmTKyfVTJHqBTXzozCGJYKCihycjqRjvtCduyndUdS9n7arFD6tJ/WMKqbYrNcMRsJJSN/ztIP4CrjgSCx4agfZgrO24xrkYkDPfdgMDNVHhNAJYhqAwwyxG3z3z+ut7wYVbN57RrdZbBkEyQqJrfLuSFAWdRjYHfOMbenzrid3xMHjbSKQyi/l0k7qVaVwGGO2GzX0PacLB/KDUdRY6vMNXQN5s42z0riHtYtjDxqN8E6xA+QPeKyGM/f5P2Vyt+46ksG3NNNOcUyu85RtKKXFKBSAWg0tFMQw15MK9iKQrSGjxor000UiiZRRRTIEIopaSmAUYpaKAEopTSUAYf2gonNheTOjQ4bHXGQCfu1A/dWS4fcG15U7wrKhGmRdRVtsDKSLuh8w3G2++a2ntMBEEbDqGcf+kH/prOGILwV2I96dQD3ABDbemdqxns1Wkdb8Me0OKSJFQltgqpcFYZCQPdSbHKmP6Ld8Gp9949jRlSWCeIPnJdcafjj7Q+K5rjXgHjj/AEhYWP1UsgzAwzCwcqsgZOgBABz6otdvseDQNZlww0shYoQvJUgHI5eMLgjG2Dt1zvWLlFZOiP6eUo7/AKzL+LbFeJov0WWF3jy2ovnSrbYKhTkHHfoVHrWMuPCMkKl7qWGJgM5jDtkAgZOwGdwMfKqrjvEEtLn6syQSJg6lYsMMoYqd9Y64IJPT1qLxXi11eFYGLNhhl5CEDHsAW2UDPr8TVylGWUZPxT8T4SwbrwdDZuRFFevrXcmOOONm6DPNaMuB8NQP75Evhe4mvo5mj0QxnzCaZp5pCpbBLkt5SdJAyNhjA3Fcre0nt5AGDDJAyhWQAnoCyEg7fHet14M8bXCsIHmWZdDBAVLMuBqHnOksAFIwT1IGacZR/wChLxeSbqGSy8W8H4tKTvFJCrBgsXkbboWDncjPQGqW2aaEgSxtGfRxg/cOrfdmupcd4TcrbNIZASF1MNTRqoxuQIipOOpBY1yPj3FJLPmRSBecTpyMLFGCuWZY1A1NpwAW1YJzvij1o9D/AMWfHk9fg6G3jS6SGGFI42ld44xzHHMdJGAYpCpLeVCfM+npnBqolDXiw39wqqkN7JDB5A+uHEccWR3POBOo/H4VlvY5YK11DJtlp30nuBHbynv8XH6Nda8V8OSGG3igARFnLlevl0Ss2M9CXdenrWDVystOlRQ4phFetMNd5yDRThSUtAxaSlpKBBRigU6gBtFLiloA9aKSigBaSiigAooooAKKKKAKLxtr+iSaQDnSrAgHyMwBIz0IOk5+FZbw3Zm+sGslI5kgkMIJABmt3hkEeexeN2xmuhy2iTqYXOlZfq2b0DnTn7s5rmnhKxZbx+GyycqQTDlSD/R3MRKo23UPkL1HVD2rKey46KMS3VmGt5rUhlZiC8bh1bAXquNQHUDpk571a2vjK+KctA5J3OFZsn+My9CfiRXXZ/CPFpAVn4rhTtlEYtt0IOpQDSyeArJUP0i6upVAJKGd1X4+VTqIz6k71i/GpdnR4v1c/DhYOMpYTXAEdy0MClzI88un6Q+diAATJIepCgbnJJrbyX1jPE8tyvKh+lcxNSlwA9tKkauADjUFiO2Rv3q94j4Ut7azHIto0llYIB70gyCTl2JKDy74OBWH8W30dxbyJG5dpb/MLqjaJgqxphQAcMnu4+0NxnNWkorBjLyy8svceV5a20f0O5j0xxNDFDLIFyFuEWQF3074IG5xnGCfjlUjmsnViowM4kU6onVsjKuNsEE98710fwTNbLb28EgU5mmjkZ18sc3JcFCrdT5lI6gjV0wa8oPDscN9cQRSSxKpU64JBGCGUbPGwMbbnpgdKxceWDo8XmfilzQyX2sSSQCKXLjADLpC6wANnkz0O4OFyR6VnrnxHw2UNPcW809yxyFZ9EQJzkNg+4OgHXGATtWrvPBczMJAbacAkGKa2jty/qWlgXJPofL0qI3DYLV+ZLwNQ49wx3BuIdWMDMbSAE5ycGheFxNvL+tXkVJV+1L+CX7GeFMk8EhGzR3E2ATpUOsKRjB7nmNv/smtx4huJHlIbAUE8sA5JTCgsw7HWr/cK8PZ9DcLFJxC9Kq7x6QoAGiKMu5LY21MzMxxt0HavPiMxeQuftYI3z5SoKfqI/E1r417jjm8EammlpK6TASlooFAwooooEKKWkFLQAUlLRQA+ikooAWikooAKKKKACiiigCv8RLm0uB/uZP6Brk9lBO7SXERy1uUlcsx1kFxhxndsNjJz3Fdf4mgaGVT3jcfiprn3HuEvELe4tiSL21CacamJIRXG3q2nttg1nNFJ1g6XJ7V7JmZJElQogLnRnDbagQNlwe+cGsXx72h3DTPLaRIIY0Uu0sas0m+FD+bZNTYAByNyfSszcFIblzKoj5qEDlqCi7khGCtgkHSCMggLvjNXXDrRYuH8S1rgFrZU1pg4MjTKBncHSVwfTfuKnqkKldvs0HjDxE03B4bqNtDXemB86isa+YTpEekeSmMknKDHXNc5seIjn29uGzBbu7jbq5XLsPgSFHyArr3g3gkc3BbWGVQQymVdShgG5zOhKnqNsYyMgkd655wHw3b207zT3du0QwqaJNJZ+amtGjYakAQPuwxsN6lp0mVBxVoqeE8XH0eeyuCQytz7d9wUuUz5TpHfU33j0q9h4zydF1rUfSY1MjMNQSSJysqqDtgsFxnYahvhc0619nF1JdPPlBBzXkQ7Ss6lyVwpIB2IOScdDvXlx7hyBY7TPLSB0RQw162ZtMpZ/dyrzAntvjtUJS2atrRuvCXH7e6GgXCNOSSynKj4LGTgPpGkHG5OTVD4oF5NMUe3cLFk/V6pVY9AwKjfbsQCMnNVPHPDzyy20zHBnj1XBJHleIRq+cbD3lXPfGepqBbXIknlUO4jVXRVwxR2Gogk6sL5mIUEYAC56VfNpZI9ON4OrW13HxG2js4WKJyo2udGGYAAfUDsvQliegAGDqFQ0vFmAlUYVh5VwRpUbKuD0woA+6qLhqSWiwCEaY41hkuSpwSuJJlT1YuQWx6OB6Vd2smtFfbzKDscqR2MZ7pj3T3XBp+PYT0elLiilrcyG0lOoxQAlGKWigAooooAKSlooAWjNJRQAuaKSjNAC0UlLQAUtJS0AeF9+Tfp7jdTpHunq3YfHtT+FWI+jtDFpaTh06XMWNiIJvrSmnqpUmQaT/EX1pt6cRudtkY7jI909R3Hwqs4TxR7bics3M1xQ2gaZNJX6vVGH2+1IurWSOo0jvUS0J/kp/aHZssjwRRwi3kl+lB1IM/1qgsWRjkp5yo0jqoBO1R+C2k1zE8rXMpaXUhDsCh0KUDlcYzpUjfvgbVd8StZJJr+DWGQfRXtmOB9TMzpEqn+KplB9MqT1Ne/AYF+iR3CIF0jfC5DATMZA4Ukr59Rz0IwcHGxFJkyk0jXeGeKKLSGGWIx8tFhVo8yxkRKFySq5Q7ZwwxvsTVR484TaGHni0jlkTAUAONeSAdfKIJAH4YrytvEFiZDBIeTKsA1q+Y01HHMVWYBWIGPmDkd8WF9ZpoSPdQeqqzAEEZI2O43++hKlgSl7rZV+Db+Se30mEWyxSFFWMFUZB5gFDZI677nv67U3tBtec0enKmFieh82oodLEe6uUG/XPbvWia0VBlpDgKRux0jG4O53/7dqrDfwzRoUOoYyzj8mmogbnpqJ6D7zgdc5SajRvGKcrM3e8TkEEr3FmskMjllxNpMLszMdJXzKuASR8PTFV1tyJxDBEriGSNuYCV0tKrSapFIy2gaHfSxGAo23zV3aI6RRQu552mWRmDEEFkaPWpXHdid9vKc7VcLwIWy/RFAWWaMiWRwv1UIA5zkIAEVirYA3xjfJNZpXk1brBmpOLSvCxICxzzosanqMdASOnkVY89tXwrZWWrQC2c9SSMEnudP2PzewxWZ8UyxSWlq8SKkZuXVOZ7vLTXGDNjqpKuzemo+laqBCFVSCCAAQx1MD6O32m+PyrSGGRPQ+iiitzIKBRRSAKKKKYCUUtJQAUlLiigBM0UlGaAFopKWgBaWk+Pp1qluvE8CkrFqndeoiGVH50nuj8aTaQ6su6h3/FoINpJFDHog8zn5IN6yPEPEEjflJeWD/ooPfx/tS/3aom4pp1cpNA6s/vOQfV/+4qH5ClA0vEfFkzA8qMQoQ31kpDPtndYlP7TUiHW9/exOGz/AAY4CyDBUExYC+o3zn4lfs1h5ebjDoupjleYSGzuQQMjAGMbj0rc3Mzfwk51u3M4aigsxJ80SS+XqQMqdvUmiLbJmkjMWnFZEt1MuSDamIjYB4HchVduoCujAEbjNXfsul50UcEkhjAkeKFlIyz6hLyZV9PMSrDuWGdwKztxIq/RGRdRKvG6SD6ssjhtG5xgl2zv1b41KueJCG3cQKIddyGCLrVSBFiRk1t5dMoXbORgelZp5Kkk1Ru/E/AYbl0huU3KSsxQ6WEiglXHZsKSN/l8sw3hiQaUXiVzy9Adc5OFGNP2h0+G3StFxLxjcjhkNy+idZJDFqkRWYDQuFbSw1Nq15Yf/wByFx4iR9Gq05eldIMTPHlO6nVqyNquVPLIgpLC0Xdv4fgeSNp57mYoA6iaQYXO2QoHf1+FXCsELPM+I8KMHJVNm6jGOg2A3b5b1WcO4tEthK7IYmUEI0h1l1BVXEfQrpLLvuNR+4Y1uK3F5PGpdwqo4RB5QocMpZjnDM2dz8MdqzlSNo2anwvxSa8lVEUAySiVdDpzFhVhmMjUCGMZfAxuXB2xU32kXpgAgWQM9wztO4Oc8pl0Qg/xF/WRTfDHDGsra4mt5I5ryQtBDKoEYSFcK7psRqLAgE7Er6DfG8biuWwZ00ugZcEklgVJRzlm74GxpJ4G1k0PFIwvDeHYkCkaJNJjJILtPJrXqJBvvHjJ6d61kMyIiHomMBhkxemlZfdOD8c74rJ+NbvFjEidLV7bOkrlTo2GVbUhy2dxv2NafgDSxxaeaYiskqaNDNAVWVgA6MWx0PU/j1qo/ImXxJwpaZIE95oWi/3towaIn1a3OQP5v4013KqGWSOZSypmPKSAscDVE+/UjoTW1/ZlV6PWkoBB3FFUIKKKKAA0lFFABRRRQBUnjCKxRypKsVLxHmxll94DHmyO4AOPWp1tdxSe46t6gEZHzXqKxVrx8CBHSedVkXDQypHcQBgcZw+nUdsk5J9dxXnxC2YosrQJnfzRubd8DuiSbMcncKT2rLmzTib+qTiviy1tyUyZX/iRDVv8W90fjWNt/Etwh5aXGRjdblSTgjcHJz69Diq+91zahDCq6GYHlDyYzuq7n7RYgk4xt2pereh8K2WHGbma5KPK+qOQNIkIkARArEGNyuMsFCtg70wi6mXSo0xruQmAqjcZPQKOm7aevWqNYJRqXT5lOCCejhsdOmfskd6uJuKNGhiZdxjVkthTnfSnuqc7asbjp1qVT2N2tEvmwoqRyyrMQdoIF8pJ21STbayPTLVZ2aXEuUt05MYPl0jW2RjSeY3lTHqu+e1U9txOGJAVMav3eWJpfzcb/wBteE3je7Gy3CbekIA/DG341VpE02bHh/gm2B1TAyOTl2ZmZmJ65Y4/UBS3sapeQZG8nC1TPXBQaGz67DH4Vk7Xxtd4y836MSfvFXsF4ZZOHS5yZLadGz303Mm2B8htVxkrVETi6yZHjVy6W4iUkaZ5ASO+Aun+jmvf6XzYo5MAsjuxiyAro5XWQO+mQNlfRlPY14+JY8c5emmZX7fbD5/WRUnw1f2f0Sa3eAyXT6fo7ZUaGVnbKMdxsRt3OR3rK6kaV7UzokHDvpnh6OOJAHDuyKMbamJXqdvLIp/CucFjKwj0uJiyx8vQ2S/u4Bxjrtv6/Crzh/tCuohKoWNZJnSRuYMxhuWI3KLkadQWM4zgb7ekSLiSTvqNuokyv5C6lg82Qo0xhXxvjpTddBG1s0HHOHjlSWKnWbe3jhbAJUzQRvc3ITG5OXB271lvC1qkPMupdRhjiGMDBZpDoQDIwd8n+ZnoK2zcQjhW2gW2mgu1nLxKSTqldTl5JZV+sDBsttk4+FVviCGaaGKW5lWWOM8wQxFUVY1ZEcNCEAZ9bHOwyAcEdKiZUMIj+Anld94zgpI6uMj6tcKQVI6Bm1dft79K9ePgvdOM7A2uR6jmrqFenD+LSCNxFiSCwtNa6vKXQqsbgnsrI+wxvoFU9ohW1jkxgyOoGCxPlDsNySeoB60J44jeck/2gcDt+RLcDaYKmrD7NsqnKeoA7Y6V7eJLC6SSO6iPmZWfXFMqy6ZJHkCldg+A2/x1Csx4nDSNcyfRgBzHBlxIcAMRkHWV6Y7d60viLwxPNDDMkOYo7cuTISCI2xKDgfnHtmpk/csFJYE4J42IkxcYLdNZHJcfBgPK33ip/jjjghNsV5basu2SjMoI0qG0nY7kjIHu1gkvrWJlJQuNAJ0nSVY9UYNkHHr6EVL4RyuJ3RiePQmk6dHVVUbFic5bJ+W5q1KVUyOKuzT2XG7Seb6iRoGchMMQEBOlVZj0O+c5Faa14jBKzLHIrFSQcH0OMj1HxFYmL2cKkhZ7g8kA74CvnHcnygCq6zupuHzlLG5juQ/vJo5gwMbMOh+41aco7E0paOpYorA8UfiU+pyTbqdxEJMEnHREG437H1qw4Bc3QdZHhiht3TCiWaTTk5UOZXYsTrG/lwBnYYzT9Qnga2ok/Eokblgl5D0iiUySfor0+ZwKyPE+Ks7iIzSzvkh4LJTFGOmNUpy7L1Bxjp1q7sUvynLj5djEScrCAZyCfdeQdSPUsTT53pC41ss+fP8A6nP/AOj+9RVX/wCFk/1m5/TX+7RTuQYMxFw+1n0aZ8ssZEjIwdUO/JUNMVVtQGCir1PrimWNvPbo8EzssTLHIgdXEcbOdKSHKrpGCd12O2OpxhgQCDq3HQ9CD2ORWg4Z4ouYRoZudEza3Rz5nI3Uc0guoDYbAPX5msVJGjizdcK8JWrKskji5wSQQQIQehwqHDfziak8R8MW4DSwRaJguqMRuY1aRfMmpM6Pex1GKpOD8QguX1wTciYtmQMVjyXQjRArs7SAMABqGwKehpnF+OcUUBAgjYpkrynE2APM240/gOzdMVpca0Rlso7e6uLSVBdxFWXLjIBLHc522bztq69RUrg8dvKp13DpN3I8+o5HUbE526elZK8vZGJ1ltRPmLe9kep615wSHB3HpWHI14nSYOCzpnSUlUHYoArEfFX05+Qbbb4VE4hY2jbS2pjYZy5EkJY9TgEBWON/KD233rK8N49PB7rn458wPfoen3YrSW/jdZFCXCZA05wA6EKcjVC5wcdcghuwParUkQ4sg3fh2LrDIxHXB2IHoPe3+Jx91PhvOTHYhs5hecNt5QruHQ6vmzZHbHxq8D2spaSGeEgqFWMYjZTtpzqAYEdgC3yPSoj2hk1KSSwYgZAdDgjK5wGXqoPlPxwKpOtCqzL+LrxWlbQchlXcdDpxg/qqRwpmjSK5jTGNSjHnHQq+rI6Nlsqc7HIxtTeJGKFIiY0aViZT5MosWAieXJDByC/YbjAwRRwmKUNoCs6uGk5I1Hy6clig7hfNnp07UruVg8Ro8ePxpG6GNsiQCSNepCsWDKzHqVkV137AHvge13JM0gldTLGAiCRkJUoVA3YYYsuQc51DAqfxS3N2sirFpZWeeADCthlVp4yDgFAVLKBvkt1LYqJwDjfEOGgSRZMRwxwNcZydOTnKgg7bjIOxqVh5Htfkt7CciSOVbnMUPnHM+vaAlSMqAyGRMjAz01KSO9VHEImlVpokl0Krc24L7lmyoDAYWNWY4CDJOepqx4j43tLmRZZrCIMI2R+WNAkYhwGkVSM41A4DdQeuwFVxTi95fqiEYgjChIkXQpOyAhF95ug1b79+1TNrouCdZL/w/dJJacSZfKsiwR7+UaVLMyA4IyVjGPUtirfw/al3ggUKyx6yEVwznKYRAM7tuTnpt1qJ4Z8PXS2nJWRVF1zhyZAD9aARHupBVswNGdQwCx2rBLxQpIH0YKE7Zxg9D0pJ1Q2rN/cCbh9ncCVUIvxcRbShyJCda5X7BXzKR3yKnePPEDw8JsYUDYnt4hnWRp0wxalZftZycZ6VjYfDUuzz6oWKCVFZGAbJzkMdm238udhVhx+6ku4raHMMhgACLAWaQjAQGQNsCdI8o33FN4yJNPBnLDhqPGXknEZG2kpq/NAw2ST+qtJ4e4Nd2zNIhWFmXTrnABCnB8kW5z8cEVnkmNrcs0y5McmmaBsqXU5Ei6cYG2fkSK2d/wARWVY0toifqwC3vMwXISXYbMyaS3bOcU4UKV0Rrnh8cjL9InluH1DZ25cfXsqnI+e1Ok4skIKxaUzkMI8qPTSWzk9+pqrnZEBeViwBOeWVY56HMhOjP3k/CqPiPEUJ8nTHbLHffzOwAz+av41bkkJRb2X8niZ03RgvXBIG2evzrP3/ABd5ABrLYZm36b7nb4ktVRJIW3P696aGrFybNFFI0vhjxW9kZjo5nNwcatADAnLdD2PSpN/7QL6TZCkQ/wBhQzfpNn9lZCnrTU3VIHFXZb/+Jr7/AFqX9L/tRVTRRyl9hS+j05hFPUHbPcZpZUGDXpNHhUI+A/VmqoRDWVkbUrFWByGUkEH1BHStXwHxgfLFdbqA68/zvOgkGGKktvvg4+frWRk2O+3zplQpNaG4pnRbnwylzHEUzgwsy3PKZFYqxAVlDNvsQMLvgdM1ipLSSIMSuVDFdY3UkfxT37fLIqR4f4i0JYmNZYsYdZC2AreVtGDgORsDg469q7baSWF/b6VWNYJMRodKqYJMeWC4AyApz5XGeuMkGtKUskW1hnAmuPhTDL8K0vi/wrJYztG4CLny6tXT5gf4yPWqE2w/jIfzX/trNpl2jwE59K0PBb68jia4hchEdYmGvvIrMo0sCunymqb6DkEq2cEbZHcH+yryE8rhbL3lvQD8RFBkfrk/VTimiZNFdJx+fzqj6UkiihcBQNUcQUIvqN1B2IzXQuHcbtnsYowYnfL85JcRyo7Mp5sLAAsrDVkAnGRldjXMorZSoYFteW2yNPlGR8a3HA/DUcyMrTuCVD+QKBs5VfeQtnJ7YzkfCrhZM6LSbh0NrHDMxKufrAPpE3L0Agq6vq8pJxkZ2qntuPabuZbaTlQsAMoA2RyWjKqMHUTgDVtnSrHPaJxjhv0UgOHYEZUtIcbDOdOnT3Hr1P3OtLH6WVkgaOO4GzxSlWWYHYGNSoVjpyGTBJxkZNDedBSaNWba1S5to5YreaSGf6FKzgtzUEWoSSqdtYY41b7DGaxt/wATWFJYo8I8j6mcYIwsgMSxLj6vSA2465HpVnN4ah+nLG7wxow1NidwieUswLtGCu4ICkZGQCKgT+HjJ7pZV1jRGsMrs5J3JfG+lc/gcCl5M4ofj0io4teXDSiVmJlYayR18wDZ26E5ztVZb28kuQgz6kkAfeTWtm8PbtrdgMDyrGSw6jDZI0EY6EdDUO34aEVncosUR6ZAnk1A6QoAPUjcnAAB+9UVeMGz4x4kBtY4UlVG5QL29zE0oV1JJaKRlZCrA42K/wBlZwniQNrIz8lZdarFGuIpGUajlCM5BYgae2Cc9zlLm9GjGmHVsCdOp8fDUT5fh8qk218zBdCxoyjdkQKfeJB9OhxgYGO29PnklQSRScWvmnmeZ/eZsn7hgdfQCl/hObl8oOwjzkqDgE4AyQOuwA39Kn8WQNKxxliRqbbTnSM6QB1zkGogtT6McHBwOnzqDSyC7FupJ+e9NNTXhUdvxb+ynyaZWIVAMgaVBLHUFA2J3OSCcfGkMr6ZWz8A+D3vZGMsErQhGwwyqllOMavhvVj429nzwBJLaAhAp5mZVLZPu4BbPQGq44snlmjndOQ07kNkAAkk4AG5J9AB1ro9n4YtbSxVrlFlurkjlJqBKblVxj4kZ9TgDpSim2NujnemiujfwKf9Uj/4r/8A7qK09ORn6iMFMdjVrw6zWeSKJnCDTqZjjAxtkgsue2wOTVS+SKsOGNHzSZgdHLIBKhvMcYwCD8fxoG9livD49vrAd8Ya3YHGok50yNjYn8K85baLYmFcFsKQcnJBYDfrsprzPLEgKtpVTqLEgKuNwMjbqB8yfur2s7xZd0RCysCxCRBnzq2WKQYODjp8O/RWgop+JrDty9SgE5z7ue+BgHPT1Hxq98LcUe0EckaE5WU3Ck5jlt0zlWT+OPMc/L1NReM8VtJFXEMaSBvMFgEOwUjOtZGDebfGBVlwqYPaQxhVyI7wu7YwVflI2cnIPkJzkbt23pXnANYydKZbXiFtHDK2pJdrSc+/G/a3kbuN9KufVQd2GrlXEOCGNzHJc26yBiNEqtHkZwGEgQx7jBwWBG4NT45+RaOq3SBWn80aLzF5YfySQsG2Oc5BOSM77kHyWPLYkZQiYDSaWU6MMxOiQbkFhkddjuTVbWBL8lP/AAFdEZWNHHYxTQSdi3RHz0UnBFeV5Owt7VSpChpZBnbUWkC59dhHjerLxPZwRg8uRJT5TrEKxnfVqBHU4wvz1VH4hg2dsdOW90HtvLcFvnkgfhUvA+1/eiojwXHYGTG2+zCrzw94lW0jVGWRg6yI5DhSAT5RGRuADgnoTk74qktYi7r2IkUY37An59sVHA1hiMAKc4+DN+4kfjQm0NpPDNVM0ZiSTSrRltUskbMpjbc/R1jc5yVGdRBG4GfKcz7jwZf/AEZ71YVW3AaQjmhnVVyCPNuxG/xyKw8eUIfy5U5AODkjfcelfQthwye4tZQtwREq6ynQM20oO2MZ67YqllEvDOOcG4q0WqT6UV8rKEDhy2oqCoV8qudssR0WvS94zcytqmnZmB2xcgAjIxo0LjIPp0O9dl4P4GWWISrIFDDKhUA26dsfjWU9pfhr6P8ARn1MysZFLEgHbSwGTsNkJ+6isVYLL0c8vdWDIY2KudnDtKyncblkBbJHX1pvCoRJKiOQAxAy6sFUEganzgYHx233zW58MTcy1i1ajoklhO+fcEciZHrh2+eK3HB/DNrOQZAPMuk+pB2Aye/w7U+GLsXPqjnF94Tijtfpcd1E+lgjqsaHkGUHBkGw97CnPTOe2KziXEFvdxMY8wOySHVnTymOklVwD2LZI+QFdSPh+3ksrgSflzDNGNKk5kjic5cgY95Due4G/SuK3l3JLDGXbUVZ1UnqFIV9Py1Mf2VEopMqLsvOL8DeCaaN7iFWR3VULMXIBJU+7pXK4Iyd8ioc/DEXpI0gzuwCxqPnqYmtDx+7W5tLe6wmqSPTIeWjPzrPC4MvvoHhCHbY4O1J7PLm1S/UXarLE8bBOYocAuFZD5unTGfjQ6GVdpHbDYWquQpYuTLPjBHvJGcKN8Z6H4UXNqw8sQ3ZCSYokCDSFkYB1JJwV94eg9ane0wwLxKZbUKiFInAj8oDcsaxgdDjqPhVtZ8dgvOHQWcsQkktx9VqkMa5BYMC43B5bAgbbr1p30FFZcWXEbBYmRjFHcKray6BS246vtkqynA9R6bVPFbOeSZUnmDvIAy5YL5dJIZiQFC7e8PSpXHvElxcwwws3ltZAEJGW0leXtsegUdvXrSPYteSJGszu+kFWeZySCSgxrUaUGDnbuMDfFKrC6yT+AW0VoC0sSOVLB3Vt4juulSw3Zge+ANWexxL4re8pGurgYZjpjVfIV8u0MYGceUqWb/Rg499sBvEOOrrgF7zCkBdWZEIl1jyhnMhInJAQhjgjA6jIrK8Unm4nLqjUKqJhI2kUJHGpGcO2Ad2ySdyWz3FXdYWyKvYz/xRJ/5Ft/wj/eoqL/AUv8e3/wDuYf71FRykXxiS04NO65VEc90WSNpB/Mzk/dmk4NZ8y4EJUZOoEMSh1KrZQ7jSc+pGCBU2TxRdRNpSREVB5Y0hjEQ9Rgrk/Mkn41e+MbRXsYOIomh3ChyO6Sq+AR3w6kA+hq2kQm+yg4nZQrCW0NGdQ8zkspxzAQoBbJDLg77VU8OvI4HZhhwRjqU7g5GR8BVnfcSjeNETmAjYqzMukAbaWGSRudiKgwSL1LEf/VH3nDJ8BWTNFo9bzjIZAkasik6mXmatTbHU2Rvuo79sVbX3iQkIwjWNjAmyhQpAPmOFAALNv0+FePh/w9LeHUdIQK2tpAcDUNmBjXO2AQO/yNR7DhLSRiXUqwk3EYbDHzJE0iDcdDgDqSNzjbNPJLoj8UuidEq50uNRyiIuoMchEUkaQ2ew+VWPDbrZodRLqTyV5hUZbZwqtlTkbYyD02NNS0gW2iDMWlkDnQrKCn1jgZQjLZAUjScV53dmoxLHMpd9TBQpbp1DeXA6kb7daabQNWMvbVm1NlmUxqRqG4AcgLnvjp9xo4if/YoFOfeIB/MeTV/XIfvpyzgDC6FBw6Y1qy5GnBVsgjYkDPfY1J4zw54YuU5RtSpcxsjahpl1RNg/OOLI36U9oO7KGB8nXsCGbrv7seB8zRwJ8y6SFw6MhyBp6ahnO2xUH7qYjgLq75k2/mqAf11EtHCupPZgfwPoalOmimrTJDgAkeU/LBHy2/dX0Z7K5+bZ6Dgl4YyduxiEZ/WhFfPfFIyr6t/NuMj17+nrXbPYnIRHCCdngmz/ADLqXH6mq6q0R9M3/g64VrZEBGVUZAxsG3H7x9xqq9p8Ki0jlZdXKmQ4Azu4aIHGRnBkB6jcA1L8GQxxxMR7ySNEfUIjaFGB+b1Px7VI9oEOvh1yOuI9Y+cbK4/o1F+40WjkXCBiSSPs88M4IwDiWGRGbA295f2V0XhdvKGB1ghWGAUG5z1z1Gf8GufcVBWXUGwXtMjH+7umQZ+P1g3710PwbfhlRWOWA94474x+6uiL9pzyXvJfCLUC7uo2PV22PcSoHI+IGvH3V8x39vyWlgI/JXEifo6k/wCmvqd5OXxDG+JY0Jx01DWoLfhivnT2oWfI4peIBgNMJf8AiAOT+kzVg2bJEOwWXkXVorZGlZl7E6VDNp+cZOah8GfYNtlM5zg+Rtgcd8bj5VL8KQlpDJt5HiVlO+oSPoIHwxmpngf6JHf4vFBgUujgE9SxCd8lQV/XS/IzPzBxJr2JBzjYZA+A9R+2pVupyY1dgNQdcEgHI07/ABw37a7De8V8OxA8iBXbGyqjk57bs+BWYuOMwmUabWFEKnYqGkO4Vm1/J12OcFariLkYi84bKrCHDF2IIGCcjYrgDJ+0f1+la6ysPocMtvpYudQuE1p5m5YeLToJcYIIwGJyxOmrDhot8G7wWXUBGgjLuOW4idwqkrqCyN9r0Ok9s34g4yEZIDHHOBpSRGURltOQqaY2DJgEYIxjsKquKsi+Troz/iSeQzNrDrgk6XGCGPvbHpn4AVK8PPHBcMs0YlTSS0f2WwUcHIz2JPTtVbxy3WOUhF0owDxg4JCOMqrHG7D3T8QaiQzlCGHvDp+HWs7yaNHUf4S8Pf6o/wDwTRXOv4fu/wDzm/Af2UU+SFTGS/a+f9ldW8Tf+7sP8lZ/tooq/sj6OU3n5Q/Jf2Ckj6/49aKKyNDf8Y/zbL/KH+phqLwb/Ndt/LXf9UtJRV9mPT/cor78pH+bH/WNXhb+6fl+9qWik9mi0TE/IxfKf+hU3xB1tv8A5ZH/AMy9FFAdGQn7/Nv21HHWloqGWXHF/dT8xf8AqrtXsh6Wf8jdf8xSUVtLsxXRp/Bv5e+/P/8AyTVd+Kv8ju/5B/6s0UVl2bdHIL/rF/8ABXH/ADUNa3wh/ovu/wCmiiumHxMJ/Iv/ABD/AJZB/JH/AKq4n7cP86y/yMP7KKK53pGy2zP+Eerfnw/1lQU/yiX5yf0zRRT6QnsvrX3V/wAdxVhce8v5j/0o6KKpaJ7Nbffk7L8w/vrCcR/JP+cP6wUUVrIz8fxMrxf8ofm/9a9QKWiuV7OhaCiiikM//9k=', 'Волейбо́л (англ. volleyball от volley — «удар с лёта» и ball — «мяч») — вид спорта, командная спортивная игра, в процессе которой две команды соревнуются на специальной площадке, разделённой сеткой, стремясь направить мяч на сторону соперника таким образом, чтобы он приземлился на площадке противника (добить до пола), либо чтобы игрок защищающейся команды допустил ошибку. При этом для организации атаки игрокам одной команды разрешается не более трёх касаний мяча подряд (в дополнение к касанию на блоке)!!!!');
INSERT INTO groups (id, name, creator, image, description) VALUES (33, 'Spirt', 7, NULL, NULL);
INSERT INTO groups (id, name, creator, image, description) VALUES (34, 'Java', 7, NULL, 'Java — сильно типизированный объектно-ориентированный язык программирования, разработанный компанией Sun Microsystems (в последующем .                ');


--
-- TOC entry 3145 (class 0 OID 0)
-- Dependencies: 192
-- Name: groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: uotmjnaeeeyngs
--

SELECT pg_catalog.setval('groups_id_seq', 1, false);


--
-- TOC entry 3120 (class 0 OID 494187)
-- Dependencies: 193
-- Data for Name: groups_link; Type: TABLE DATA; Schema: public; Owner: uotmjnaeeeyngs
--

INSERT INTO groups_link (group_id, account_id) VALUES (34, 7);
INSERT INTO groups_link (group_id, account_id) VALUES (13, 8);
INSERT INTO groups_link (group_id, account_id) VALUES (13, 7);


--
-- TOC entry 3121 (class 0 OID 494190)
-- Dependencies: 194
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: uotmjnaeeeyngs
--

INSERT INTO message (id, date, from_id, to_id, text) VALUES (1, '2017-09-05 19:43:06', 7, 8, 'test test');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (2, '2017-09-05 21:55:59', 8, 7, 'test2');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (3, '2017-09-05 23:02:10', 8, 7, 'test3');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (4, '2017-09-05 23:02:26', 8, 7, 'test5');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (5, '2017-09-05 23:02:26', 8, 7, 'test6');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (6, '2017-09-05 23:02:26', 8, 7, 'test7');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (7, '2017-09-05 23:02:26', 8, 7, 'test8');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (8, '2017-09-06 21:50:54', 7, 9, 'weqe');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (9, '2017-09-06 21:51:10', 7, 9, 'weqe');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (10, '2017-09-06 21:59:30', 7, 9, 'qqqqqq');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (11, '2017-09-06 22:00:37', 7, 9, '11234');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (12, '2017-09-07 12:21:22', 7, 9, '123567');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (13, '2017-09-07 12:21:34', 7, 9, '23rghfdhj');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (14, '2017-09-07 12:21:58', 7, 18, 'цкевпвп');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (15, '2017-09-07 13:02:13', 9, 7, 'я терминатор');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (16, '2017-09-07 13:02:17', 9, 7, 'привет');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (17, '2017-09-07 17:39:48', 7, 9, '123');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (18, '2017-09-07 17:39:50', 7, 9, '123');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (19, '2017-09-07 17:39:57', 7, 9, '123');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (20, '2017-09-07 17:40:01', 7, 9, '123');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (21, '2017-09-07 17:41:14', 7, 9, '2');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (22, '2017-09-07 17:41:27', 7, 9, '22');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (23, '2017-09-07 17:42:33', 7, 9, '22');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (24, '2017-09-07 17:43:45', 7, 9, '2');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (25, '2017-09-07 17:44:33', 7, 9, '4');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (26, '2017-09-07 17:44:35', 7, 9, '1');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (27, '2017-09-07 17:44:37', 7, 9, '5');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (28, '2017-09-07 17:44:41', 7, 9, 'test');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (29, '2017-09-07 17:53:01', 9, 7, '123536654');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (30, '2017-09-07 20:46:27', 7, 9, '457');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (31, '2017-09-07 21:16:49', 7, 9, '123wqedsada');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (32, '2017-09-07 21:17:04', 7, 9, '111111111111111111111');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (33, '2017-09-07 21:18:16', 7, 9, '125');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (34, '2017-09-07 21:19:05', 9, 7, '1');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (35, '2017-09-07 21:19:19', 7, 9, '2');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (36, '2017-09-07 21:20:29', 7, 9, 'TESTTTT');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (37, '2017-09-07 21:28:14', 9, 7, 'MyTest');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (38, '2017-09-07 21:28:32', 7, 9, 'Hi');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (39, '2017-09-07 21:32:19', 9, 7, 'Hello');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (40, '2017-09-07 21:38:11', 7, 9, '1235321');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (41, '2017-09-08 08:13:56', 9, 7, 'Привет');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (42, '2017-09-08 08:14:02', 9, 7, 'Привет');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (43, '2017-09-08 08:14:42', 7, 9, 'Ghbdtn');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (44, '2017-09-08 08:16:29', 9, 7, 'Привет');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (45, '2017-09-08 08:19:19', 9, 7, 'ук');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (46, '2017-09-08 08:45:03', 7, 9, 'ytut');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (47, '2017-09-08 08:46:51', 7, 9, 'tetert');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (48, '2017-09-08 08:56:12', 7, 9, 'z');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (49, '2017-09-08 08:56:30', 9, 7, 'Привет, это я');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (50, '2017-09-08 08:57:52', 7, 9, 'Хай');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (51, '2017-09-08 08:58:05', 7, 9, 'Как дела ');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (52, '2017-09-08 08:58:17', 9, 7, 'как дела ');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (53, '2017-09-08 08:59:55', 7, 9, 'уцк');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (54, '2017-09-08 09:01:30', 7, 9, 'qwerfds');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (55, '2017-09-20 20:28:43', 7, 9, 'ййй');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (56, '2017-09-25 22:28:22', 7, 9, 'Ой');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (57, '2017-10-11 23:14:20', 7, 9, '11');
INSERT INTO message (id, date, from_id, to_id, text) VALUES (58, '2017-10-25 01:43:43', 9, 7, 'Ооо, привет');


--
-- TOC entry 3146 (class 0 OID 0)
-- Dependencies: 195
-- Name: message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: uotmjnaeeeyngs
--

SELECT pg_catalog.setval('message_id_seq', 1, false);


--
-- TOC entry 3123 (class 0 OID 494208)
-- Dependencies: 196
-- Data for Name: persistent_logins; Type: TABLE DATA; Schema: public; Owner: uotmjnaeeeyngs
--

INSERT INTO persistent_logins (username, series, token, last_used) VALUES ('nick9@bk.ru', 'S9wqVFDegcVQG+tO18EwYA==', 'uYVKWw9qLN+MSN4LKh9X/Q==', '2018-01-09 11:38:30.965');


--
-- TOC entry 3124 (class 0 OID 494212)
-- Dependencies: 197
-- Data for Name: phones; Type: TABLE DATA; Schema: public; Owner: uotmjnaeeeyngs
--

INSERT INTO phones (id, account_id, phone, type) VALUES (30, 8, '1242313', 'HOME');
INSERT INTO phones (id, account_id, phone, type) VALUES (32, 8, '44444444', 'HOME');
INSERT INTO phones (id, account_id, phone, type) VALUES (46, 7, '33333333333', 'HOME');
INSERT INTO phones (id, account_id, phone, type) VALUES (47, 7, '123456421', 'HOME');
INSERT INTO phones (id, account_id, phone, type) VALUES (48, 7, '2344433432', 'HOME');
INSERT INTO phones (id, account_id, phone, type) VALUES (52, 7, '1111111111', 'HOME');


--
-- TOC entry 3147 (class 0 OID 0)
-- Dependencies: 198
-- Name: phones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: uotmjnaeeeyngs
--

SELECT pg_catalog.setval('phones_id_seq', 1, false);


--
-- TOC entry 3126 (class 0 OID 494228)
-- Dependencies: 199
-- Data for Name: posts; Type: TABLE DATA; Schema: public; Owner: uotmjnaeeeyngs
--



--
-- TOC entry 3148 (class 0 OID 0)
-- Dependencies: 200
-- Name: posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: uotmjnaeeeyngs
--

SELECT pg_catalog.setval('posts_id_seq', 1, false);


--
-- TOC entry 2970 (class 2606 OID 494252)
-- Name: address account_id_unique; Type: CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY address
    ADD CONSTRAINT account_id_unique UNIQUE (account_id);


--
-- TOC entry 2964 (class 2606 OID 494254)
-- Name: accounts email_UNIQUE; Type: CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT "email_UNIQUE" UNIQUE (email);


--
-- TOC entry 2966 (class 2606 OID 494256)
-- Name: accounts id; Type: CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT id PRIMARY KEY (id);


--
-- TOC entry 2968 (class 2606 OID 494268)
-- Name: accounts id_UNIQUE; Type: CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT "id_UNIQUE" UNIQUE (id);


--
-- TOC entry 2972 (class 2606 OID 494270)
-- Name: address id_address; Type: CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY address
    ADD CONSTRAINT id_address PRIMARY KEY (id);


--
-- TOC entry 2974 (class 2606 OID 494281)
-- Name: groups id_group; Type: CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT id_group PRIMARY KEY (id);


--
-- TOC entry 2976 (class 2606 OID 494283)
-- Name: message id_message; Type: CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY message
    ADD CONSTRAINT id_message PRIMARY KEY (id);


--
-- TOC entry 2980 (class 2606 OID 494285)
-- Name: phones id_phone; Type: CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY phones
    ADD CONSTRAINT id_phone PRIMARY KEY (id);


--
-- TOC entry 2982 (class 2606 OID 494287)
-- Name: posts id_posts; Type: CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT id_posts PRIMARY KEY (id);


--
-- TOC entry 2978 (class 2606 OID 494289)
-- Name: persistent_logins series; Type: CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY persistent_logins
    ADD CONSTRAINT series PRIMARY KEY (series);


--
-- TOC entry 2984 (class 2606 OID 494290)
-- Name: friends fk_friends_1; Type: FK CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY friends
    ADD CONSTRAINT fk_friends_1 FOREIGN KEY (id) REFERENCES accounts(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2985 (class 2606 OID 494295)
-- Name: friends fk_friends_2; Type: FK CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY friends
    ADD CONSTRAINT fk_friends_2 FOREIGN KEY (friend_id) REFERENCES accounts(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2986 (class 2606 OID 494300)
-- Name: group_admins fk_group_admins_1; Type: FK CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY group_admins
    ADD CONSTRAINT fk_group_admins_1 FOREIGN KEY (account_id) REFERENCES accounts(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2987 (class 2606 OID 494305)
-- Name: group_admins fk_group_admins_2; Type: FK CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY group_admins
    ADD CONSTRAINT fk_group_admins_2 FOREIGN KEY (group_id) REFERENCES groups(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2988 (class 2606 OID 494310)
-- Name: groups fk_groups_1; Type: FK CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT fk_groups_1 FOREIGN KEY (creator) REFERENCES accounts(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2989 (class 2606 OID 494315)
-- Name: groups_link fk_groups_link_1; Type: FK CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY groups_link
    ADD CONSTRAINT fk_groups_link_1 FOREIGN KEY (account_id) REFERENCES accounts(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2990 (class 2606 OID 494320)
-- Name: groups_link fk_groups_link_2; Type: FK CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY groups_link
    ADD CONSTRAINT fk_groups_link_2 FOREIGN KEY (group_id) REFERENCES groups(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2983 (class 2606 OID 494325)
-- Name: address fk_homeAdress_1; Type: FK CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY address
    ADD CONSTRAINT "fk_homeAdress_1" FOREIGN KEY (account_id) REFERENCES accounts(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2991 (class 2606 OID 494330)
-- Name: message fk_message_from; Type: FK CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY message
    ADD CONSTRAINT fk_message_from FOREIGN KEY (from_id) REFERENCES accounts(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2992 (class 2606 OID 494335)
-- Name: message fk_message_to; Type: FK CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY message
    ADD CONSTRAINT fk_message_to FOREIGN KEY (to_id) REFERENCES accounts(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2993 (class 2606 OID 494340)
-- Name: phones fk_phones_1; Type: FK CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY phones
    ADD CONSTRAINT fk_phones_1 FOREIGN KEY (account_id) REFERENCES accounts(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2994 (class 2606 OID 494345)
-- Name: posts fk_posts_1; Type: FK CONSTRAINT; Schema: public; Owner: uotmjnaeeeyngs
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT fk_posts_1 FOREIGN KEY (account_id) REFERENCES accounts(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3134 (class 0 OID 0)
-- Dependencies: 7
-- Name: public; Type: ACL; Schema: -; Owner: uotmjnaeeeyngs
--

GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- TOC entry 3136 (class 0 OID 0)
-- Dependencies: 618
-- Name: plpgsql; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON LANGUAGE plpgsql TO uotmjnaeeeyngs;


-- Completed on 2018-01-15 15:55:26 MSK

--
-- PostgreSQL database dump complete
--


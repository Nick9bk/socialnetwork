package com.getjavajob.training.web1703.zhadovskiin.controller;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import com.getjavajob.training.web1703.zhadovskiin.json.AccountDTO;
import com.getjavajob.training.web1703.zhadovskiin.log.Log;
import com.getjavajob.training.web1703.zhadovskiin.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nick on 11.06.17.
 */
@RestController
public class SearchController {
    private static final Logger logger = LoggerFactory.getLogger(SearchController.class);

    private AccountService accountService;

    @Autowired
    public SearchController(AccountService accountService) {
        this.accountService = accountService;
    }

    @Log
    @RequestMapping(value = "/searchAjax")
    public List<AccountDTO> searchServletByPage(@RequestParam("text") String regex, @RequestParam("page") int page,
                                                @RequestParam("size") int size) {
        logger.info("searchServletByPage text = " + regex + " page = " + page + " size = " + size);
        List<Account> list = accountService.getByRegex(regex, page, size);
        List<AccountDTO> accountSearches = new ArrayList<>(list.size());
        for (Account account : list) {
            accountSearches.add(new AccountDTO(account));
        }
        return accountSearches;
    }

    @Log
    @RequestMapping(value = "/searchRegex")
    public List<AccountDTO> searchRegex(@RequestParam("text") String regex) {
        logger.info("searchRegex text = " + regex);
        List<Account> accountList = accountService.getByRegex(regex);
        logger.debug("search result = " + accountList);
        List<AccountDTO> accountSearches = new ArrayList<>(accountList.size());
        for (Account account : accountList) {
            accountSearches.add(new AccountDTO(account));
        }
        return accountSearches;
    }
}

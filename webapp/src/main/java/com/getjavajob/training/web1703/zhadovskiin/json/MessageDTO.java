package com.getjavajob.training.web1703.zhadovskiin.json;

import com.getjavajob.training.web1703.zhadovskiin.common.Message;

import java.time.format.DateTimeFormatter;

public class MessageDTO {
    private int idTo;
    private int idFrom;
    private String imageTo;
    private String imageFrom;
    private String text;
    private String date;

    public MessageDTO() {
    }

    public MessageDTO(Message message) {
        setIdFrom(message.getFromAccount().getId());
        setImageFrom(message.getFromAccount().getImage());
        setIdTo(message.getToAccount().getId());
        setImageTo(message.getToAccount().getImage());
        setText(message.getText());
        setDate(message.getSendingDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
    }

    public int getIdTo() {
        return idTo;
    }

    public void setIdTo(int idTo) {
        this.idTo = idTo;
    }

    public int getIdFrom() {
        return idFrom;
    }

    public void setIdFrom(int idFrom) {
        this.idFrom = idFrom;
    }

    public String getImageTo() {
        return imageTo;
    }

    public void setImageTo(String imageTo) {
        this.imageTo = imageTo;
    }

    public String getImageFrom() {
        return imageFrom;
    }

    public void setImageFrom(String imageFrom) {
        this.imageFrom = imageFrom;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

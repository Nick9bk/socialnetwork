package com.getjavajob.training.web1703.zhadovskiin.controller.security;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import com.getjavajob.training.web1703.zhadovskiin.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class MySessionInfo {
    @Autowired
    private AccountService accountService;
    private Account account;

    public Account getCurrentAccount() {
        if (account == null) {
            account = accountService.getAccount(SecurityContextHolder.getContext().getAuthentication().getName());
        }
        return account;
    }
}
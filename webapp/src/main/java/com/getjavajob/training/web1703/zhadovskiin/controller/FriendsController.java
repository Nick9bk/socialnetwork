package com.getjavajob.training.web1703.zhadovskiin.controller;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import com.getjavajob.training.web1703.zhadovskiin.common.Friend;
import com.getjavajob.training.web1703.zhadovskiin.common.StatusFriend;
import com.getjavajob.training.web1703.zhadovskiin.controller.security.MySessionInfo;
import com.getjavajob.training.web1703.zhadovskiin.json.AccountDTO;
import com.getjavajob.training.web1703.zhadovskiin.service.AccountService;
import com.getjavajob.training.web1703.zhadovskiin.service.FriendsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class FriendsController {
    private FriendsService friendsService;
    private AccountService accountService;
    private MySessionInfo mySessionInfo;

    @Autowired
    public FriendsController(FriendsService friendsService, AccountService accountService, MySessionInfo mySessionInfo) {
        this.friendsService = friendsService;
        this.accountService = accountService;
        this.mySessionInfo = mySessionInfo;
    }

    @GetMapping("/getFriends")
    public List<AccountDTO> getFriends(@RequestParam("status") StatusFriend status) {
        Account account = mySessionInfo.getCurrentAccount();
        account.setFriends(friendsService.getFriends(account));
        List<Friend> friends = friendsService.getFriendsByStatus(status, account);
        List<AccountDTO> accountList = new ArrayList<>(friends.size());
        for (Friend accountTemp : friends) {
            accountList.add(new AccountDTO(accountTemp.getAccount()));
        }
        return accountList;
    }

    @GetMapping("/addFriend")
    public boolean actionByFriend(@RequestParam("id") int id, @RequestParam("status") StatusFriend status) {
        Account friend = accountService.getAccount(id);
        friendsService.addFriend(mySessionInfo.getCurrentAccount(), friend, status);
        return true;
    }

    @GetMapping("/removeFriend")
    public boolean removeFriend(@RequestParam("id") int id, @RequestParam("status") StatusFriend status) {
        Account friend = accountService.getAccount(id);
        friendsService.removeFriend(mySessionInfo.getCurrentAccount(), friend);
        return true;
    }
}

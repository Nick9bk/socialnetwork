package com.getjavajob.training.web1703.zhadovskiin.controller.message;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import com.getjavajob.training.web1703.zhadovskiin.common.Message;
import com.getjavajob.training.web1703.zhadovskiin.controller.security.MySessionInfo;
import com.getjavajob.training.web1703.zhadovskiin.json.MessageDTO;
import com.getjavajob.training.web1703.zhadovskiin.json.MessageWithNameDTO;
import com.getjavajob.training.web1703.zhadovskiin.log.Log;
import com.getjavajob.training.web1703.zhadovskiin.service.AccountService;
import com.getjavajob.training.web1703.zhadovskiin.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@RestController
public class MessageController {
    private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

    private MessageService messageService;
    private AccountService accountService;
    private MySessionInfo mySessionInfo;

    @Autowired
    public MessageController(MessageService messageService, AccountService accountService, MySessionInfo mySessionInfo) {
        this.messageService = messageService;
        this.accountService = accountService;
        this.mySessionInfo = mySessionInfo;
    }

    @Log
    @GetMapping(value = "/getAllMessages")
    public List<MessageWithNameDTO> getMessages() {
        ModelAndView modelAndView = new ModelAndView("forward:index?param=message");
        List<Message> messages = messageService.getMessage(mySessionInfo.getCurrentAccount());
        modelAndView.addObject("messages", messages);
        logger.debug("all messages : " + messages);
        List<MessageWithNameDTO> messageDTOS = new ArrayList<>(messages.size());
        for (Message message : messages) {
            messageDTOS.add(new MessageWithNameDTO(message));
        }
        return messageDTOS;
    }

    @Log
    @GetMapping(value = "/chat")
    public List<MessageDTO> getChatMessage(@RequestParam("numberMsg") int numberMsg, @RequestParam("toId") int toId) {
        List<Message> messages = messageService.getMessageOfFriends(mySessionInfo.getCurrentAccount().getId(), toId, numberMsg);
        List<MessageDTO> messageDTOs = new ArrayList<>(messages.size());
        for (Message message : messages) {
            messageDTOs.add(new MessageDTO(message));
        }
        return messageDTOs;
    }

    @Log
    @GetMapping(value = "/newMessage")
    public boolean createChatMessage(@RequestParam("to") int toId, @RequestParam("text") String text) {
        Account accountTo = accountService.getAccount(toId);
        messageService.createMessage(mySessionInfo.getCurrentAccount(), accountTo, text);
        return true;
    }

    @MessageMapping("/chatMsg/{page}/{to}")
    @SendTo("/receiver/{page}/{to}")
    public MessageDTO sendMsg(MessageDTO message) throws Exception {
        Message msg = messageService.createMessage(accountService.getAccount(message.getIdFrom()),
                accountService.getAccount(message.getIdTo()), message.getText());
        return new MessageDTO(msg);
    }
}

package com.getjavajob.training.web1703.zhadovskiin.json;

import com.getjavajob.training.web1703.zhadovskiin.common.Address;

public class AddressDTO {
    private String country;
    private String city;
    private String street;
    private int house;
    private int structure;
    private int apartment;

    public AddressDTO(Address address) {
        setCity(address.getCity());
        setCountry(address.getCountry());
        setApartment(address.getApartment());
        setStreet(address.getStreet());
        setHouse(address.getHouse());
        setStructure(address.getStructure());
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHouse() {
        return house;
    }

    public void setHouse(int house) {
        this.house = house;
    }

    public int getStructure() {
        return structure;
    }

    public void setStructure(int structure) {
        this.structure = structure;
    }

    public int getApartment() {
        return apartment;
    }

    public void setApartment(int apartment) {
        this.apartment = apartment;
    }

    @Override
    public String toString() {
        return "AddressDTO{" +
                "country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", house=" + house +
                ", structure=" + structure +
                ", apartment=" + apartment +
                '}';
    }
}

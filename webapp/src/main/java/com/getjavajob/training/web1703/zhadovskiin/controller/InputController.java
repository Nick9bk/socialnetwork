package com.getjavajob.training.web1703.zhadovskiin.controller;

import com.getjavajob.training.web1703.zhadovskiin.log.Log;
import org.springframework.stereotype.Controller;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class InputController {
    @Log()
    @GetMapping(value = {"/index", "/", "/information", "/messages", "/friends", "/account", "/group", "/groups",
            "/search", "/registration", "/login"})
    public String index() {
        return "index";
    }

    @GetMapping("/jsp/**")
    public String loadJSPPage(HttpServletRequest request) {
        return new AntPathMatcher().extractPathWithinPattern("/jsp/**", request.getRequestURI());
    }
}

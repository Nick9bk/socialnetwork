package com.getjavajob.training.web1703.zhadovskiin.controller;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import com.getjavajob.training.web1703.zhadovskiin.common.Group;
import com.getjavajob.training.web1703.zhadovskiin.controller.security.MySessionInfo;
import com.getjavajob.training.web1703.zhadovskiin.enums.goups.ActionByUser;
import com.getjavajob.training.web1703.zhadovskiin.enums.goups.TypeAccount;
import com.getjavajob.training.web1703.zhadovskiin.json.GroupDTO;
import com.getjavajob.training.web1703.zhadovskiin.json.GroupWithFullDataDTO;
import com.getjavajob.training.web1703.zhadovskiin.log.Log;
import com.getjavajob.training.web1703.zhadovskiin.service.AccountService;
import com.getjavajob.training.web1703.zhadovskiin.service.GroupsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class GroupsController {
    private static final Logger logger = LoggerFactory.getLogger(GroupsController.class);

    private GroupsService groupsService;
    private AccountService accountService;
    private MySessionInfo mySessionInfo;

    @Autowired
    public GroupsController(GroupsService groupsService, AccountService accountService, MySessionInfo mySessionInfo) {
        this.groupsService = groupsService;
        this.accountService = accountService;
        this.mySessionInfo = mySessionInfo;
    }

    @GetMapping("/getGroups")
    public List<GroupDTO> getGroups(@RequestParam String param) {
        List<Group> groups = param.equals("allGroups") ?
                groupsService.getAllGroups() : groupsService.getGroupByAccountId(mySessionInfo.getCurrentAccount().getId());
        logger.trace(groups.toString());
        List<GroupDTO> groupDTOS = new ArrayList<>(groups.size());
        for (Group group : groups) {
            groupDTOS.add(new GroupDTO(group));
        }
        return groupDTOS;
    }

    @PostMapping("/saveGroupImage")
    public boolean saveGroupImage(@RequestParam int id, @RequestParam String image) {
        logger.trace("saveGroupImage id = " + Integer.toString(id));
        return groupsService.saveImage(id, image);
    }

    @GetMapping("/saveGroupDescription")
    public boolean saveGroupDescription(@RequestParam int id, @RequestParam String text) {
        logger.trace("saveGroupImage id = " + Integer.toString(id));
        return groupsService.saveDescription(id, text);
    }

    @GetMapping("/changeGroupUser")
    public boolean changeGroupUser(@RequestParam int id, @RequestParam int idAccount, @RequestParam TypeAccount type,
                                   @RequestParam ActionByUser action) {
        logger.trace("changeGroupUser id = " + Integer.toString(id) + " idAccount = " + Integer.toString(idAccount) +
                " type = " + type + " action = " + action);
        Group group = groupsService.getById(id);
        if (group == null) {
            return false;
        }
        List<Account> users;
        switch (type) {
            case ADMIN:
                users = group.getAdmins();
                break;
            case USER:
                users = group.getUsers();
                break;
            default:
                throw new IllegalArgumentException("type == " + type);
        }
        Account bufAccount = accountService.getAccount(idAccount);
        switch (action) {
            case ADD:
                if (!users.contains(bufAccount)) {
                    users.add(bufAccount);
                } else {
                    return false;
                }
                break;
            case REMOVE:
                users.remove(bufAccount);
                break;
        }
        groupsService.updateGroup(group);
        return true;
    }

    @Log
    @GetMapping("/createGroup")
    public boolean createGroup(@RequestParam String name, @RequestParam String description) {
        Group group = new Group();
        group.setName(name);
        group.setDescription(description);
        group.setCreator(mySessionInfo.getCurrentAccount());
        groupsService.createGroup(group);
        return true;
    }

    @Log
    @GetMapping("/getGroup")
    public GroupWithFullDataDTO getGroupPage(@RequestParam int id) {
        Account account = mySessionInfo.getCurrentAccount();
        Group group = groupsService.getById(id);
        GroupWithFullDataDTO groupWithFullDataDTO = new GroupWithFullDataDTO(group);
        groupWithFullDataDTO.setAdmin(groupsService.isAdmin(group, account) || group.getCreator().equals(account));
        groupWithFullDataDTO.setMember(groupsService.isMembers(group, account));
        logger.debug(groupWithFullDataDTO.toString());
        return groupWithFullDataDTO;
    }
}

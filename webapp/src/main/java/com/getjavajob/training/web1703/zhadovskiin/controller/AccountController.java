package com.getjavajob.training.web1703.zhadovskiin.controller;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import com.getjavajob.training.web1703.zhadovskiin.common.Phone;
import com.getjavajob.training.web1703.zhadovskiin.controller.security.MySessionInfo;
import com.getjavajob.training.web1703.zhadovskiin.json.AccountDTO;
import com.getjavajob.training.web1703.zhadovskiin.json.AccountWithFullDataDTO;
import com.getjavajob.training.web1703.zhadovskiin.log.Log;
import com.getjavajob.training.web1703.zhadovskiin.service.AccountService;
import com.getjavajob.training.web1703.zhadovskiin.service.FriendsService;
import com.google.common.hash.Hashing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by nick on 29.05.17.
 */
@RestController
public class AccountController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    private AccountService accountService;
    private FriendsService friendsService;
    private MySessionInfo mySessionInfo;

    @Autowired
    public AccountController(AccountService accountService, FriendsService friendsService, MySessionInfo mySessionInfo) {
        this.accountService = accountService;
        this.friendsService = friendsService;
        this.mySessionInfo = mySessionInfo;
    }

    @Log
    @GetMapping(value = "/getCurrentAccount")
    public AccountWithFullDataDTO getCurrentAccount() {
        return new AccountWithFullDataDTO(mySessionInfo.getCurrentAccount());
    }


    @Log
    @GetMapping(value = "/getAccount")
    public AccountWithFullDataDTO getAccount(@ModelAttribute("id") int id) {
        logger.info("viewAccount id = " + id);
        Account account = accountService.getAccount(id);
        logger.trace(account.toString());
        AccountWithFullDataDTO accountFull = new AccountWithFullDataDTO(account);
        accountFull.setStatusFriend(friendsService.getStatusFriend(mySessionInfo.getCurrentAccount(), account));
        return accountFull;
    }

    @Log
    @GetMapping(value = "/getAccountSmall")
    public AccountDTO getAccountSmall(@ModelAttribute("id") int id) {
        logger.info("viewAccount id = " + id);
        Account account = accountService.getAccount(id);
        logger.trace(account.toString());
        return new AccountDTO(account);
    }

    @Log
    @PostMapping(value = "/saveInfo")
    public String saveInfo(@RequestBody Account account) {
        logger.debug("Account " + account);
        logger.debug("Phones = " + account.getPhones());
        Account accountFromDB = accountService.getAccount(mySessionInfo.getCurrentAccount().getId());
        commonParam(accountFromDB, account);
        accountFromDB.setIcq(account.getIcq());
        accountFromDB.setSkype(account.getSkype());
        accountFromDB.setImage(account.getImage());
        preparationAndSetPhones(accountFromDB, account.getPhones());
        account.getHomeAddress().setAccount(accountFromDB);
        if (accountFromDB.getHomeAddress() != null) {
            account.getHomeAddress().setId(accountFromDB.getHomeAddress().getId());
        }
        accountFromDB.setHomeAddress(account.getHomeAddress());
        accountService.editAccount(accountFromDB);
        return "OK";
    }

    @Log
    @RequestMapping(value = "/newAccount")
    public String newAccount(@RequestBody Account account) {
        logger.debug(account.toString());
        if (account.getEmail() == null || accountService.getAccount(account.getEmail()) != null) {
            return "ERROR";
        }
        account.setRegistrationDate(LocalDate.now());
        accountService.createAccount(account);
        return "OK";
    }

    @Log
    @PostMapping("/changePassword")
    public boolean changePassword(@RequestParam("newPassword") String newPassword,
                                  @RequestParam("oldPassword") String oldPassword) {
        Account account = mySessionInfo.getCurrentAccount();
        logger.debug("newPassword == " + newPassword);
        logger.debug("oldPassword == " + oldPassword);
        String oldPasswordHash = Hashing.sha256().hashString(oldPassword, StandardCharsets.UTF_8).toString();
        logger.debug("oldPasswordHash = " + oldPasswordHash);
        logger.debug("account.getPassword() = " + account.getPassword());
        if (!oldPasswordHash.equals(account.getPassword())) {
            return false;
        }
        String newPasswordHash = Hashing.sha256().hashString(newPassword, StandardCharsets.UTF_8).toString();
        logger.debug("newPasswordHash = " + newPasswordHash);
        account.setPassword(newPasswordHash);
        accountService.editAccount(account);
        return true;
    }

    private void commonParam(Account importAccount, Account exportAccount) {
        importAccount.setName(exportAccount.getName());
        importAccount.setSurname(exportAccount.getSurname());
        importAccount.setPatronymic(exportAccount.getPatronymic());
        importAccount.setBirthday(exportAccount.getBirthday());
    }

    private void preparationAndSetPhones(Account account, List<Phone> phones) {
        account.getPhones().clear();
        for (Phone phone : phones) {
            phone.setAccount(account);
            account.getPhones().add(phone);
        }
    }

}

package com.getjavajob.training.web1703.zhadovskiin.json;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import com.getjavajob.training.web1703.zhadovskiin.common.Phone;
import com.getjavajob.training.web1703.zhadovskiin.common.StatusFriend;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class AccountWithFullDataDTO extends AccountDTO {
    private StatusFriend statusFriend;
    private String email;
    private String birthday;
    private String registrationDate;
    private String icq;
    private String skype;
    private AddressDTO address;
    private List<Phone> phones;

    public AccountWithFullDataDTO(Account account) {
        super(account);
        setEmail(account.getEmail());
        LocalDate localDate = account.getBirthday();
        setBirthday(localDate != null ? localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) : "");
        localDate = account.getRegistrationDate();
        setRegistrationDate(localDate != null ? localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) : "");
        setIcq(account.getIcq() != null ? account.getIcq() : "");
        setSkype(account.getSkype() != null ? account.getSkype() : "");
        setPhones(account.getPhones());
        if (account.getHomeAddress() != null) {
            setAddress(new AddressDTO(account.getHomeAddress()));
        }
    }

    public StatusFriend getStatusFriend() {
        return statusFriend;
    }

    public void setStatusFriend(StatusFriend statusFriend) {
        this.statusFriend = statusFriend;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }
}

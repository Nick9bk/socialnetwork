package com.getjavajob.training.web1703.zhadovskiin.log;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by nick on 07.07.17.
 */
@Aspect
@Component
public class AspectLog {

    @Around("execution(* *(..)) && @annotation(logAnnotation)")
    public Object logDuration(ProceedingJoinPoint joinPoint, Log logAnnotation) throws Throwable {
        Logger logger = LoggerFactory.getLogger(joinPoint.getSignature().getDeclaringType());
        String methodName = joinPoint.getSignature().getName();
        logger.info(methodName + " : begin");
        Object result = joinPoint.proceed();
        logger.info(methodName + " : end");
        return result;
    }

}

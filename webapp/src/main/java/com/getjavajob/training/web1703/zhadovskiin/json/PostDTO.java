package com.getjavajob.training.web1703.zhadovskiin.json;

import com.getjavajob.training.web1703.zhadovskiin.common.Post;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class PostDTO {
    private int id;
    private String date;
    private String text;

    public PostDTO(Post post) {
        setId(post.getId());
        setDate(post.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        setText(post.getText());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "PostDTO{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}

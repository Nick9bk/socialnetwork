package com.getjavajob.training.web1703.zhadovskiin.configuration;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;


@EnableWebSecurity
public class SpringSecurityConfigurer extends WebSecurityConfigurerAdapter {
    @Autowired
    DataSource dataSource;

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery("SELECT email, password, TRUE FROM accounts where email=?")
                .authoritiesByUsernameQuery("SELECT email, 'ROLE_USER' as role FROM accounts where email=?");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/resources/**", "/webjars/**", "/registration", "/jsp/Registration",
                        "/jsp/Login", "/newAccount").permitAll()
                .anyRequest().authenticated();

        http.formLogin().loginPage("/login").failureUrl("/authenticationFailure").permitAll()
                .usernameParameter("email")
                .passwordParameter("password")
                .loginProcessingUrl("/loginProcessing")
                .defaultSuccessUrl("/successUrl", true);
        http.rememberMe().rememberMeParameter("rememberMe").key("myAppKey").tokenRepository(persistentTokenRepository())
                .tokenValiditySeconds(2592000);
        http.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login")
                .deleteCookies("JSESSIONID");
        http.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())/*.ignoringAntMatchers("/newAccount")*/;
    }

    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
        db.setDataSource(dataSource);
        return db;
    }
}
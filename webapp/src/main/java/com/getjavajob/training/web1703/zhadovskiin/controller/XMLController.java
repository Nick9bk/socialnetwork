package com.getjavajob.training.web1703.zhadovskiin.controller;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import com.getjavajob.training.web1703.zhadovskiin.controller.security.MySessionInfo;
import com.getjavajob.training.web1703.zhadovskiin.exception.AjaxErrorException;
import com.getjavajob.training.web1703.zhadovskiin.json.AccountWithFullDataDTO;
import com.getjavajob.training.web1703.zhadovskiin.log.Log;
import com.getjavajob.training.web1703.zhadovskiin.service.AccountService;
import com.thoughtworks.xstream.XStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;

/**
 * Created by nick on 13.07.17.
 */
@RestController
public class XMLController {
    private static final Logger logger = LoggerFactory.getLogger(SearchController.class);

    private AccountService accountService;
    private MySessionInfo mySessionInfo;

    @Autowired
    public XMLController(AccountService accountService, MySessionInfo mySessionInfo) {
        this.accountService = accountService;
        this.mySessionInfo = mySessionInfo;
    }

    @Log
    private void validateXML(String xml) throws IOException, SAXException {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Source xsd = new StreamSource(XMLController.class.getClassLoader().getResourceAsStream("account.xsd"));
        Schema schema = factory.newSchema(xsd);
        Validator validator = schema.newValidator();
        validator.validate(new StreamSource(new StringReader(xml)));
        logger.info("file is valid");
    }

    @Log
    private XStream getXStream() {
        XStream xStream = new XStream();
        xStream.processAnnotations(Account.class);
        xStream.setClassLoader(Account.class.getClassLoader());
        return xStream;
    }

    @RequestMapping(value = "/writeToXML", method = RequestMethod.GET)
    public HttpEntity<byte[]> writeToXML() {
        Account account = accountService.getAccount(mySessionInfo.getCurrentAccount().getId());
        XStream xStream = getXStream();
        File file = new File("/Downloads", "Account" + account.getId() + ".xml");
        byte[] xml = xStream.toXML(account).getBytes();

        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "xml"));
        header.set("Content-Disposition", "attachment; filename=" + file.getName());
        header.setContentLength(xml.length);
        return new HttpEntity<>(xml, header);
    }

    @PostMapping(value = "/readXML")
    public AccountWithFullDataDTO readXML(@RequestParam("xmlString") String xmlUpload) throws AjaxErrorException, IOException {
        logger.trace(xmlUpload);
        if (xmlUpload == null || xmlUpload.isEmpty()) {
            logger.info("xml file is empty");
            throw new AjaxErrorException("EMPTY_FILE");
        }
        try {
            validateXML(xmlUpload);
            XStream xStream = getXStream();
            Account account = (Account) xStream.fromXML(xmlUpload);
            logger.trace(account.toString());
            logger.trace(account.getPhones().toString());
            return new AccountWithFullDataDTO(account);
        } catch (SAXException e) {
            logger.error("Bad xml file " + e.getMessage());
            throw new AjaxErrorException("BAD_XML");
        }
    }
}

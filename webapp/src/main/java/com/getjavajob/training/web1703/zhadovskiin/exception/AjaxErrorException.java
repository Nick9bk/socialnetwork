package com.getjavajob.training.web1703.zhadovskiin.exception;

public class AjaxErrorException extends RuntimeException {
    public AjaxErrorException() {
    }

    public AjaxErrorException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public AjaxErrorException(String s) {
        super(s);
    }
}

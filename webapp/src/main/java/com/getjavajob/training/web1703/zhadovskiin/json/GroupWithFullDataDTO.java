package com.getjavajob.training.web1703.zhadovskiin.json;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import com.getjavajob.training.web1703.zhadovskiin.common.Group;

import java.util.ArrayList;
import java.util.List;

public class GroupWithFullDataDTO extends GroupDTO {
    private List<AccountDTO> admins;
    private List<AccountDTO> users;
    private AccountDTO creator;
    private boolean admin;
    private boolean isMember;

    public GroupWithFullDataDTO(Group group) {
        super(group);
        creator = new AccountDTO(group.getCreator());
        List<Account> tmp = group.getAdmins();
        admins = new ArrayList<>(tmp.size());
        for (Account admin : tmp) {
            admins.add(new AccountDTO(admin));
        }
        tmp = group.getUsers();
        users = new ArrayList<>(tmp.size());
        for (Account user : tmp) {
            users.add(new AccountDTO(user));
        }
    }

    public List<AccountDTO> getAdmins() {
        return admins;
    }

    public List<AccountDTO> getUsers() {
        return users;
    }

    public AccountDTO getCreator() {
        return creator;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isMember() {
        return isMember;
    }

    public void setMember(boolean member) {
        isMember = member;
    }


    @Override
    public String toString() {
        return "GroupWithFullDataDTO{" +
                super.toString() +
                "admins=" + admins +
                ", users=" + users +
                ", creator=" + creator +
                ", admin=" + admin +
                ", isMember=" + isMember +
                '}';
    }
}

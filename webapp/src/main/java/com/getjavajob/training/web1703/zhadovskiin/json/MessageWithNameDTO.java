package com.getjavajob.training.web1703.zhadovskiin.json;

import com.getjavajob.training.web1703.zhadovskiin.common.Message;

public class MessageWithNameDTO extends MessageDTO {
    private String nameTo;
    private String nameFrom;
    private String surnameTo;
    private String surnameFrom;

    public MessageWithNameDTO(Message message) {
        super(message);
        setNameFrom(message.getFromAccount().getName());
        setSurnameFrom(message.getFromAccount().getSurname());
        setSurnameTo(message.getToAccount().getSurname());
        setNameTo(message.getToAccount().getName());
    }

    public String getNameTo() {
        return nameTo;
    }

    public void setNameTo(String nameTo) {
        this.nameTo = nameTo;
    }

    public String getNameFrom() {
        return nameFrom;
    }

    public void setNameFrom(String nameFrom) {
        this.nameFrom = nameFrom;
    }

    public String getSurnameTo() {
        return surnameTo;
    }

    public void setSurnameTo(String surnameTo) {
        this.surnameTo = surnameTo;
    }

    public String getSurnameFrom() {
        return surnameFrom;
    }

    public void setSurnameFrom(String surnameFrom) {
        this.surnameFrom = surnameFrom;
    }
}

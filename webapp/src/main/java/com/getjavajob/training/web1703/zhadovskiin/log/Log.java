package com.getjavajob.training.web1703.zhadovskiin.log;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by nick on 07.07.17.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Log {
    String value() default "";
}

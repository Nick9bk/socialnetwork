package com.getjavajob.training.web1703.zhadovskiin.json;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;

/**
 * Created by nick on 12.07.17.
 */
public class AccountDTO {
    private int id;
    private String name;
    private String surname;
    private String patronymic;
    private String image;

    public AccountDTO() {
    }

    public AccountDTO(Account account) {
        setId(account.getId());
        setName(account.getName());
        setSurname(account.getSurname());
        setPatronymic(account.getPatronymic());
        setImage(account.getImage());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    @Override
    public String toString() {
        return "AccountDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                '}';
    }

}

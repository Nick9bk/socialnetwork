package com.getjavajob.training.web1703.zhadovskiin.controller.security;

import com.getjavajob.training.web1703.zhadovskiin.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class SecurityController {
    private static final Logger logger = LoggerFactory.getLogger(SecurityController.class);

    @Log
    @GetMapping("/authenticationFailure")
    public String authenticationFailure() {
        return "ERROR";
    }

    @Log
    @GetMapping(value = "/successUrl")
    public String successUrl() {
        return "OK";
    }
}

package com.getjavajob.training.web1703.zhadovskiin.controller;

import com.getjavajob.training.web1703.zhadovskiin.common.Post;
import com.getjavajob.training.web1703.zhadovskiin.controller.security.MySessionInfo;
import com.getjavajob.training.web1703.zhadovskiin.json.PostDTO;
import com.getjavajob.training.web1703.zhadovskiin.log.Log;
import com.getjavajob.training.web1703.zhadovskiin.service.PostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class PostController {
    private static final Logger logger = LoggerFactory.getLogger(PostController.class);

    private PostService postService;
    private MySessionInfo mySessionInfo;

    @Autowired
    public PostController(PostService postService, MySessionInfo mySessionInfo) {
        this.postService = postService;
        this.mySessionInfo = mySessionInfo;
    }

    @Log
    @GetMapping("/getPosts")
    public List<PostDTO> getPosts(@RequestParam("id") int account_id) {
        List<Post> posts = postService.getPosts(account_id);
        List<PostDTO> postDTOS = new ArrayList<>(posts.size());
        for (Post post : posts) {
            postDTOS.add(new PostDTO(post));
        }
        return postDTOS;
    }

    @Log
    @GetMapping("/removePost")
    public boolean removePost(@RequestParam("id") int idPost) {
        postService.removePost(idPost);
        return true;
    }

    @Log
    @GetMapping("/addPost")
    public PostDTO addPost(@RequestParam String text) {
        logger.info("Add post text = " + text);
        Post post = postService.addPost(mySessionInfo.getCurrentAccount(), text);
        logger.debug("new Post : " + post.toString());
        return new PostDTO(post);
    }
}

/**
 * Created by nick on 21.06.17.
 */
$(document).ready(function () {
    var id = $('#myId').val();
    (function () {
        $.get(context + "/getAccount", {id: id}, function (account) {
            console.log(account);
            var divInf = $('#allInfo');
            divInf.find('#avatar').attr('src', img(account.image, defaultAccountImg));
            divInf.find('#registrationDate').val(account.registrationDate);
            setMainData(account);
        });
    })();

    $('body')
        .unbind()
        .on('click', '#saveAll', function () {
        var divInf = $('#allInfo');
        var img = divInf.find('#avatar').attr('src');
        if (img === defaultAccountImg) {
            img = '';
        }
        $.ajax({
            url: context + "/saveInfo",
            data: JSON.stringify({
                id: $('#myId').val(),
                name: divInf.find('#name').val(),
                patronymic: divInf.find('#patronymic').val(),
                surname: divInf.find('#surname').val(),
                email: divInf.find('#email').val(),
                skype: divInf.find('#skype').val(),
                icq: divInf.find('#icq').val(),
                birthday: divInf.find('#birthday').val(),
                homeAddress: {
                    id: divInf.find('#homeAddress.id').val(),
                    country: divInf.find('#country').val(),
                    city: divInf.find('#city').val(),
                    house: divInf.find('#house').val(),
                    structure: divInf.find('#structure').val(),
                    apartment: divInf.find('#apartment').val(),
                    street: divInf.find('#street').val()
                },
                phones: $('#fields').find('.remove-me').map(function (t) {
                    console.log(this.id);
                    var number = this.id.replace(/^\D+/g, '');
                    return {
                        id: $('#idPhone' + number).val(),
                        type: $('#select' + number).val(),
                        phone: $('#field-input' + number).val()
                    };
                }).get(),
                image: img
            }),
            method: "POST",
            contentType: "application/json"
        }).done(function (e) {
            if (e === "OK") {
                $('#successfulSave').modal('show');
                loadMainData();
            }
        });
    })
        .on('click', '.add-more', function () {
        addItem();
    })
        .on('click', '.remove-me', function (e) {
        e.preventDefault();
        var fieldNum = this.id.charAt(this.id.length - 1);
        var fieldID = "#field" + fieldNum;
        $(this).remove();
        $(fieldID).remove();
    })
        .on('click', '#savePassword', function () {
        function clear() {
            $('#passwordOld').val('');
            $('#passwordNew').val('');
            $('#passwordNew2').val('');
        }

        var pass1 = $('#passwordNew').val();
        var pass2 = $('#passwordNew2').val();
        if (pass1 !== pass2) {
            clear();
            $("#wrongPass").fadeTo(2000, 500).slideUp(300, function () {
                $("#wrongPass").slideUp(300);
            });
            return;
        }
        $.post(context + '/changePassword', {
            newPassword: pass1,
            oldPassword: $('#passwordOld').val()
        }, function (data) {
            if (data === true) {
                $('#passwordEditModal').modal('toggle');
            } else {
                $("#errorEdit").fadeTo(2000, 500).slideUp(300, function () {
                    $("#errorEdit").slideUp(300);
                });
            }
        });
        clear();
    });

    $('#xmlUpload').change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function () {
                var xml = reader.result;
                $.post(context + "/readXML", {xmlString: xml}, function (account) {
                    setMainData(account);
                });
                $('#xmlUpload').val('');
            };
            reader.readAsText(this.files[0]);
        }
    });

    function setMainData(account) {
        var divInf = $('#allInfo');
        divInf.find('#email').val(account.email);
        divInf.find('#name').val(account.name);
        divInf.find('#patronymic').val(account.patronymic);
        divInf.find('#surname').val(account.surname);
        divInf.find('#birthday').val(account.birthday);
        divInf.find('#skype').val(account.skype);
        divInf.find('#icq').val(account.icq);
        divInf.find('#homeAddress.id').val(account.address.id);
        divInf.find('#country').val(account.address.country);
        divInf.find('#street').val(account.address.street);
        divInf.find('#city').val(account.address.city);
        divInf.find('#house').val(account.address.house);
        divInf.find('#structure').val(account.address.structure);
        divInf.find('#apartment').val(account.address.apartment);
        removePhones();
        account.phones.forEach(function (phone) {
            addPhoneItem(phone);
        });
        addItem();
    }

    function removePhones() {
        $('#fields').empty().append('<div class="input-group" id="field0"></div>');
        next = 0;
    }
});

var next = 0;

function addItem() {
    if (next !== 0) {
        var phone = $("#field-input" + next).val();
        //regex page https://habrahabr.ru/post/110731/
        var regex = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/im;
        if (phone.match(regex) === null) {
            $("#wrongPhone").fadeTo(2000, 500).slideUp(300, function () {
                $("#wrongPhone").slideUp(300);
            });
            return;
        }
        $("#b" + next).text("-");
        $("#b" + next).removeClass("add-more").addClass("remove-me").addClass("btn-danger");
        $("#field-input" + next).prop("readonly", true);
    }
    var addto = "#field" + next;
    next = next + 1;
    var newGr = '<div class="input-group" id="field' + next + '" style="margin-bottom: 3px">\
            <input type="hidden" id="idPhone' + next + '" name="phones[' + (next - 1) + '].id" value="0"/>\
            <input class="form-control bfh-phone" id="field-input' + next + '" name="phones[' + (next - 1) + '].phone" type="text"\
                 placeholder="Phone" style="width: 60%; border-right:none;"/>\
            <select id="select' + next + '" name="phones[' + (next - 1) + '].type" class="form-control" style="  width: 40%; border-right:none;">\
            <option value="HOME">Домашний</option>\
            <option value="WORK">Рабочий</option>\
            </select>\
            <div class="input-group-btn">\
            <button id="b' + next + '" class="btn add-more" type="button">+</button>\
            </div>\
            </div>';
    var newInput = $(newGr);
    $(addto).after(newInput);
}

function addPhoneItem(phone) {
    addItem();
    $("#field-input" + next).val(phone.phone);
    $("#idPhone" + next).val(phone.id);
    $("#select" + next).prop('value', phone.name);
}

function showEdit() {
    $('#passwordEditModal').modal('show');
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
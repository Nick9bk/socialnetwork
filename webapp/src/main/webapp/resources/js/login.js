$(document).ready(function () {
    $('body')
        .unbind()
        .keypress(function (e) {
            if(e.which === 13) {
                $('#login').click();
                return false;
            }
        })
        .on('click', '#login', function () {
        passwordHash('password');
        var data = {};
        $.each($('#loginForm input'), function () {
            data[this.name] = $(this).val();
        });
        $.ajax({
            method: 'POST',
            url: context + '/loginProcessing',
            data: data,
            success: function (data) {
                if (data === "OK") {
                    console.log('login success!');
                    loadMainData(true);
                    onChangeURL('/');
                } else {
                    $('#password').val('');
                    console.error('bad login or password');
                    errorModal.find('#text').text('Введен неправильный логин или пароль');
                    errorModal.modal('show');
                }
            }
        });
    });
});
var clickedElement;
var idGroup;
var isMember;
$(document).ready(function () {
    idGroup = getUrlVars('id');
    console.log("Group id = " + idGroup);
    $('body')
        .unbind()
        .on('click', '.account', function () {
        onChangeURL("/account?id=" + $(this).find("#id").val());
    })
        .on('click', '#editMode', function () {
        $(".account:not(.creator)").addClass('editMode');
        $("#description").addClass('editMode');
        $("#groupAvatar").addClass('editMode');
    })
        .on('click', '#changeAvatar', function () {
        $('#fileUploadImage').trigger('click');
    })
        .on('click', '#removeAvatar', function () {
        $("#groupAvatar").attr("src", defaultGroupImg);
    })
        .on('click', '#saveChange', function () {
        var src = $("#groupAvatar").attr("src");
        if (src === defaultGroupImg) {
            src = null;
        }
        $.post(context + "/saveGroupImage", {
            id: idGroup,
            image: src
        }, function (data) {
            console.log(data);
        })
    })
        .on('click', '#saveChangeDescription', function () {
        $("#groupDescriptionTextArea").val($("#iDescription").text());
        $('#descriptionEditor').modal('show');
    })
        .on('click', '#saveDescription', function () {
        $.get(context + "/saveGroupDescription", {
            id: idGroup,
            text: $("#groupDescriptionTextArea").val()
        }, function (data) {
            console.log(data);
            if (data === true) {
                $('#descriptionEditor').modal('toggle');
                $("#iDescription").text($("#groupDescriptionTextArea").val());
            }
        });
    })
        .on('click', '#removeAdmin', function () {
        actionByUser("ADMIN", "REMOVE", function () {
            $(clickedElement).remove()
        });
    })
        .on('click', '#appointAdmin', function () {
        actionByUser("ADMIN", "ADD", function () {
            $("#adminsList").append($(clickedElement).clone());
        });
    })
        .on('click', '#removeUser', function () {
        actionByUser("USER", "REMOVE", function () {
            $(clickedElement).remove();
        });
    })
        .on('click', '#joinBtn', function () {
        var acId = $("#myId").val();
        console.log("isMember = " + isMember + " , account id = " + acId);
        if (isMember === true) {
            actionByUserId(acId, "USER", "REMOVE", function () {
                isMember = false;
                $("#joinBtn").text("Вступить в группу");
                $("#user_" + acId).remove();
            });
        } else {
            actionByUserId(acId, "USER", "ADD", function () {
                isMember = true;
                $("#joinBtn").text("Выйти из группы");
                $.get(context + "/getAccountSmall", {
                        id: acId
                    },
                    function (user) {
                        addAccount($('#userList'), 'user', user);
                    });
            });
        }
    })
        .on("contextmenu", ".admin.editMode", function (e) {
        openMenu("#adminsMenu", e, this);
        return false;
    })
        .on('contextmenu', '.user.editMode', function (e) {
        openMenu("#usersMenu", e, this);
        return false;
    })
        .on('contextmenu', '#description.editMode', function (e) {
        openMenu("#descriptionMenu", e, this);
        return false;
    })
        .on('contextmenu', '#groupAvatar.editMode', function (e) {
        openMenu("#avatarMenu", e, this);
        return false;
    });

    $('html').click(function () {
        hideMenus();
    });

    $("#middleColumn").removeClass("m7");
    $("#middleColumn").addClass("m6");

    $("#creatorContainer").appendTo("#rightColumn");
    $("#admins").appendTo("#rightColumn");
    $("#users").appendTo("#rightColumn");

    (function () {
        $.get(context + "/getGroup",
            {id: idGroup},
            function (group) {
                console.log(group);
                //member button
                isMember = group.member;
                if (isMember === true) {
                    $('#joinBtn').text('Выйти из группы');
                } else {
                    $('#joinBtn').text('Вступить в группу');
                }
                //avatar
                $('#groupAvatar').attr('src', img(group.image, defaultGroupImg));
                //name
                $('#groupName').text("Группа " + group.name);
                //admin menu
                if (group.admin === true) {
                    $('#adminMenu').removeAttr('hidden');
                }
                //
                $("#iDescription").text(group.description);

                //creator
                console.log('creator');
                addAccount($('#creatorContainer'), 'creator', group.creator);
                group.admins.forEach(function (admin) {
                    addAccount($('#adminsList'), 'admin', admin);
                });
                //users
                group.users.forEach(function (user) {
                    addAccount($('#userList'), 'user', user);
                });
            });
    })();

    function addAccount(div, type, user) {
        div.append(
            "<form id=\"" + type + "_" + user.id + "\" method=\"post\"\n" +
            " class=\"form-group account " + type + "\">\n" +
            "<img src=\"" + img(user.image, defaultAccountImg) + "\" class=\"img-thumbnail pull-left\">\n" +
            "<i id=\"name\" class=\"text-center\">" + user.name + ' ' + user.surname + "</i>\n" +
            "<input id='id' hidden value='" + user.id + "'>" +
            "</form>");
    }
});

function actionByUser(type, action, actionFunction) {
    var idAccount = $(clickedElement).attr("id").split("_")[1];
    actionByUserId(idAccount, type, action, actionFunction);
}

function actionByUserId(idAccount, type, action, actionFunction) {
    $.get(context + "/changeGroupUser", {
        id: idGroup,
        type: type,
        idAccount: idAccount,
        action: action
    }, function (data) {
        if (data === true) {
            actionFunction();
        }
    });
}


function openMenu(menu, e, element) {
    hideMenus();
    $(menu).css({
        display: "block",
        left: e.pageX,
        top: e.pageY
    });
    clickedElement = element;
}

function hideMenus() {
    $(".menuMouse").hide();
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function () {
            $("#groupAvatar").attr("src", reader.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}



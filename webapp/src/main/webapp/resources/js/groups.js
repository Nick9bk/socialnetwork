function changeList(tab) {
    $("#groupsContainer").html("");
    $(".nav li").removeClass("active");
    tab.addClass('active');
    tabName = tab.attr('id');
    $.get(context + "/getGroups", {param: tabName}, function (data) {
        $.each(data, function (index, group) {
            var html = "<form id='group' class='w3-button w3-block w3-theme-l1' style='height: 82px;" +
                "margin-bottom: 5px;' method='post'\n>" +
                "<input id='id' type='hidden' value='" + group.id + "'>\n" +
                "<div class='pull-left' style='width: 70px;'>" +
                "<img id='avatarGroup' src='" + img(group.image, defaultGroupImg) + "' class='img-thumbnail' " +
                "style='max-height: 62px; max-width: 62px; background-color: #A9A9A9;'/> \n" +
                "</div>" +
                "<div class='w3-col m2 w3-center' style='padding-left: 30px'>\n" +
                "<b>Название:</b> <i class='name'> " + group.name + "</i><br>\n";
            if (group.description) {
                html += "<b>Описание:</b> <a  style='width: 60px;'>" + group.description + "</a>";
            }
            html += "</div>\n" +
                "</form>";
            $('#groupsContainer').append(html);
        })
    })
}

$(document).ready(function () {
    console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    $('body')
        .unbind()
        .on('click', '#addGroup', function () {
        $("#descriptionEditor").modal("show");
    })
        .on('click', '#createGroup', function () {
        $.get(context + "/createGroup",
            {
                name: $("#nameGroup").val(),
                description: $("#groupDescriptionTextArea").val()
            }, function (data) {
                if (data === true) {
                    changeList($('.active'));
                }
            });
        $("#descriptionEditor").modal("toggle");
    })
        .on('click', '#group', function () {
        onChangeURL("/group?id=" + $(this).find("#id").val());
    });

    changeList($('#myGroups'));
});
$(document).ready(function () {
    $('.body').unbind();
    $('#regForm').submit(function () {
        var pwdObj = $('#password');
        var pwdObj2 = $('#password2');
        if (pwdObj2.val() !== pwdObj.val()) {
            alert('Пароли не совпадают!');
            return false;
        }
        passwordHash('password');
        pwdObj2.val('');

        var img = $('#avatar').attr('src');
        if (img === defaultAccountImg) {
            img = '';
        }

        $.ajax({
            url: context + "/newAccount",
            data: JSON.stringify({
                email: $('#email').val(),
                name: $('#name').val(),
                surname: $('#surname').val(),
                patronymic: $('#patronymic').val(),
                birthday: $('#birthday').val(),
                image: img,
                password: pwdObj.val()
            }),
            method: "POST",
            contentType: "application/json"
        }).done(function (ok) {
            $('#statusOperation').modal('show');
            if (ok === 'OK') {
                $('#statusMessage').text('Пользователь создан');
                onChangeURL('/login');
            } else {
                $('#statusMessage').text('Ошибка создания акаунта! Пользователь с такой почной уже существует');
            }
        });
        return false;
    });

});
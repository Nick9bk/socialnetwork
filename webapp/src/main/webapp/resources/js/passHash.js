function passwordHash(value) {
    var pwdObj = document.getElementById(value);
    var hashObj = new jsSHA("SHA-256", "TEXT", {numRounds: 1});
    hashObj.update(pwdObj.value);
    pwdObj.value = hashObj.getHash("HEX");
}
var numberMsg = 0;
var stompClient = null;
var fromId;
var toId;
$(document).ready(function () {
    loadData();
    fromId = $("#myId").val();
    $('body')
        .unbind()
        .on('click', '#addToFriend', function () {
            $.get(context + "/addFriend", {id: toId, status: 'NONE'}, function (data) {
                if (data === true) {
                    $('#addOk').modal('show');
                }
            })
        })
        .on('click', '#closeChat', function () {
            $("#middleColumn").removeClass("m5");
            $("#middleColumn").addClass("m7");
            $("#chatDiv").remove();
            $('#closeChat').attr('id', 'openChat');
            $("#openChat").text("Открыть чат");
        })
        .on('click', '#openChat', function () {
        numberMsg = 0;
        $('#openChat').attr('id', 'closeChat');
        $("#closeChat").text("Закрыть чат");
        $("#middleColumn").removeClass("m7");
        $("#middleColumn").addClass("m5");
        var html = "<div class=\"col-sm-3 frame\" style='width: 100%;' id=\"chatDiv\">" +
            "<ul id='chatList'></ul>" +
            "<div>\n" +
            "<button class=\"btn btn-info\" onclick='sendMessage()'>\n" +
            "<i class=\"glyphicon glyphicon-envelope\"></i>\n" +
            "</button>" +
            "<div class=\"msj-rta macro\" style=\"margin:auto\">\n" +
            "<div class=\"text text-r\" style=\"background:whitesmoke !important\">\n" +
            "<input class=\"mytext\" placeholder=\"Type a message\"/>\n" +
            "</div> \n" +
            "</div>\n" +
            "</div>";
        $('#rightColumn').append(html);
        updateChat();
    });

    $("#rightColumn").on("keyup", ".mytext", function (e) {
        if (e.which === 13) {
            sendMessage()
        }
    });

    connect();

    function loadData() {
        function addItem(table, item, name) {
            if (item !== '') {
                table.append(
                    "<tr>\n" +
                    "<td>" + name + ":</td>\n" +
                    "<td>" + item + "</td>\n" +
                    "</tr>"
                );
            }
        }

        toId = getUrlVars('id');
        console.log("id ===== " + toId);
        $.get(context + "/getAccount", {id: toId}, function (account) {
            console.log(account);
            $('#id').val(account.id);
            toId = account.id;
            $('#avatarAccount').attr('src', img(account.image, defaultAccountImg));

            var openChatBut = $("#openChat");
            if (fromId == toId) {
                openChatBut.remove();
            }
            switch (account.statusFriend) {
                case 'CONFIRMED':
                    openChatBut.after(
                        "<span class=\"label label-info pull-right\">\n" +
                        "Ваш друг\n" +
                        "</span>");
                    break;
                case 'OUT':
                    openChatBut.after("<label class=\"label label-info pull-right\">\n" +
                        "Отправленна заявка в друзья\n" +
                        "</label>"
                    );
                    break;
                case 'IN':
                    openChatBut.after("<label class=\"label label-info pull-right\">\n" +
                        "Прислал заявку в друзья\n" +
                        "</label>");
                    break;
                default:
                    openChatBut.after("<button class=\"btn btn-primary pull-right\" type=\"button\" id=\"addToFriend\">\n" +
                        "Добавить в друзья\n" +
                        "</button>"
                    );

            }

            $('#accountName').text(account.name + ' ' + account.surname + ' ' + account.patronymic + ' ('
                + account.email + ')');
            $('#accountRegistrationDate').text(account.registrationDate);
            var table = $('#tableInfo');
            addItem(table, account.birthday, 'Дата рождения');
            addItem(table, account.skype, 'Skype');
            addItem(table, account.icq, 'ICQ');
            if (Array.isArray(account.phones) && account.phones.length) {
                table.append(
                    "<tr>\n" +
                    "<td>Телефоны :</td>\n" +
                    "<td>\n" +
                    "<table class=\"table table-condensed\">\n" +
                    "<thead>\n" +
                    "<tr>\n" +
                    "<th>Тип</th>\n" +
                    "<th>Номер</th>\n" +
                    "</tr>\n" +
                    "</thead>\n" +
                    "<tbody  id=\"phonesTable\">" +
                    "</tbody>\n" +
                    "</table>\n" +
                    "</td>\n" +
                    "</tr>");
                account.phones.forEach(function (phone) {
                    $('#phonesTable').append(
                        "<tr>\n" +
                        "<td>" + phone.type + "</td>\n" +
                        "<td>" + phone.phone + "</td>\n" +
                        "</tr>"
                    );
                });
            }
        })
    }

});

function sendMessage() {
    var text = $('.mytext').val();
    if (text !== "") {
        stompClient.send('/app/chatMsg/' + getAddress(), {},
            JSON.stringify({idTo: toId, idFrom: fromId, text: text}));
        $('.mytext').val('');
    }
}

function getAddress() {
    return fromId < toId ? fromId + '/' + toId : toId + '/' + fromId;
}

function updateChat() {
    $.get(context + "/chat", {numberMsg: numberMsg, toId: toId}, function (data) {
        numberMsg += data.length;
        $.each(data, function (index, message) {
            insertChat(message);
        });
        scrollDouwn();
    });
}

function scrollDouwn() {
    $('#chatList').animate({scrollTop: $('#chatList').prop("scrollHeight")});
}

function insertChat(message) {
    var control = "";
    if (message.idFrom === parseInt(fromId)) {
        control = '<li style="width:100%">' +
            '<div class="msj macro">' +
            '<div class="avatar">' +
            '<img class="img-circle" style="width:100%;" src="' + message.imageFrom + '" />' +
            '</div>' +
            '<div class="text text-l">' +
            '<p>' + message.text + '</p>' +
            '<p><small>' + message.date + '</small></p>' +
            '</div>' +
            '</div>' +
            '</li>';
    } else {
        control = '<li style="width:100%;">' +
            '<div class="msj-rta macro">' +
            '<div class="text text-r">' +
            '<p>' + message.text + '</p>' +
            '<p><small>' + message.date + '</small></p>' +
            '</div>' +
            '<div class="avatar" style="padding:0 0 0 10px !important">' +
            '<img class="img-circle" style="width:100%;" src="' + message.imageFrom + '" />' +
            '</div>' +
            '</li>';
    }
    $("#chatList").append(control);
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    console.log("Disconnected");
}

function connect() {
    disconnect();
    var socket = new SockJS('/chatMsg');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/receiver/' + getAddress(), function (message) {
            insertChat(JSON.parse(message.body));
            scrollDouwn();
        });
    });
}
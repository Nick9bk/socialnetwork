$(document).ready(function () {
    $('.body').unbind();
    $.get(context + "/getAllMessages", function (data) {
        $.each(data, function (index, mes) {
            var html = "<div class=\"row msg_container base_sent\">\n" +
                "                <div class=\"col-md-2 col-xs-2 avatar img_sent\">\n" +
                "                    <c:set var=\"img\" value=\"${mes.getFromAccount().getImage()}\"/>\n" +
                "                    <c:if test=\"${img == null}\">\n" +
                "                        <c:set var=\"img\" value=\"${pageContext.request.contextPath}/resources/image/avatar.png\"/>\n" +
                "                    </c:if>\n" +
                "                    <img src=\"";
            if (mes.imageFrom === null) {
                html += defaultAccountImg;
            } else {
                html += mes.imageFrom;
            }
            html += "\" class=\" img-responsive img_avatar\">\n" +
                "                </div>\n" +
                "                <div class=\"col-md-10 col-xs-10\">\n" +
                "                    <div class=\"row\">\n" +
                "                        <div class=\"pull-left\">\n" + mes.nameFrom + " " + mes.surnameFrom +
                "                        </div>\n" +
                "                        <div class=\"pull-right\">\n" + mes.nameTo + " " + mes.surnameTo +
                "                        </div>\n" +
                "                    </div>\n" +
                "                    <div class=\"messages msg_sent msg_receive\">\n" +
                "                        <p>" + mes.text + "</p>\n" +
                "                        <time>" + mes.date + "</time>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "                <div class=\"col-md-2 col-xs-2 avatar img_receive\">\n" +
                "                    <img src=\"";
            if (mes.imageTo === null) {
                html += defaultAccountImg;
            } else {
                html += mes.imageTo;
            }
            html += "\" class=\" img-responsive img_avatar\">\n" +
                "                </div>\n" +
                "            </div>";
            $('#allMessages').append(html);
        })
    })
});
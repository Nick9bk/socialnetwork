var tabName;

function changeList(tab) {
    $("#accounts").html("");
    $(".nav li").removeClass("active");
    tab.addClass('active');
    tabName = tab.attr('id');
    $.get(context + "/getFriends", {status: tabName}, function (data) {
        $.each(data, function (index, account) {
            var html = "<form id='friend' class=\"w3-button w3-block w3-theme-l1\" style=\"height: 82px;" +
                " margin-bottom: 5px;\" method=\"post\">\n" +
                "<input id=\"id\" type=\"hidden\" value=\"" + account.id + "\">\n" +
                "<div class=\"pull-left\" style='width: 70px;'>" +
                "<img id=\"avatarAccount\" src=\"";
            if (account.image === null) {
                html += defaultAccountImg;
            } else {
                html += account.image;
            }
            html += "\"\n" +
                "class=\"img-thumbnail \" style=\"max-height: 62px; max-width: 62px;\">\n" +
                "</div>" +
                "<div class=\"w3-col m2 w3-center\" style=\"padding-left: 30px\">\n" +
                "Имя: <i class=\"name\"> " + account.name + " </i><br>\n" +
                "Отчество: <i class=\"patronymic\"> " + account.patronymic + "</i><br>\n" +
                "Фамилия: <i class=\"surname\"> " + account.surname + "</i><br>\n" +
                "</div>\n" +
                "<input id=\"remove\" type=\"button\" class=\"btn btn-default btn-sm pull-right\" " +
                "style=\"width: 80px\" onclick='actionByFriend(this)' value='Удалить'> </input>\n";
            if (tabName === 'IN') {
                html += "<input id=\"add\" class=\"btn btn-default btn-sm pull-right\" style=\"width: 180px\" " +
                    "onclick='actionByFriend(this)' value='Добавить в друзья'\>\n";
            }
            html += "</form>";
            $('#accounts').append(html);
        })
    })
}

$(document).ready(function () {

    $('body')
        .unbind()
        .on('click', '#friend', function (e) {
        if ($(e.target).is(".btn")) {
            return false;
        }
        onChangeURL("/account?id=" + $(this).find("#id").val());
    });

    changeList($('#CONFIRMED'));
});

function actionByFriend(but) {
    var button = $(but.closest('form')).find('#id');
    console.log('CLICKED BUTTON');
    $.get(context + "/" + $(but).attr('id') + "Friend", {id: button.val(), status: tabName}, function (data) {
        if (data === true) {
            changeList($('#' + tabName));
        }
    })
}

$(document).ready(function () {
    (function () {
        $.get(context + '/getPosts', {id: $('#myId').val()}, function (posts) {
            $.each(posts, function (index, post) {
                newPost(post);
            });
        })
    })();

    $('#posts').on('click', '.close', function () {
        var id = $(this).parent().find('#id').val();
        console.log('remove post id = ' + id);
        $.get(context + '/removePost', {id : id}, function (data) {
            if(data === true) {
                $('#post' + id).remove();
            }
        })
    });

    $('body').on('click', '#newPost', function () {
        var postDiv = $('#post0');
            $.get(context + '/addPost', {text : postDiv.text()}, function (post) {
                newPost(post);
                postDiv.text('');
            })
    });

    function newPost(post) {
        console.log(post);
        $('#newPostDiv').after('<div class="well" id="post' + post.id + '">\n' +
            '<input type="hidden" id="id" value="' + post.id + '"/>\n' +
            '<button type="button" class="close" data-dissmiss="alert" style="margin-top: -15px">&times;</button>\n' +
            '<p class="text" style="margin: 0px; word-wrap:break-word">' + post.text + '</p>\n' +
            '<small class="text-muted" id="date" style="position: relative; top: 15px;">' + post.date + '</small>\n' +
            '</div>')
    }
});
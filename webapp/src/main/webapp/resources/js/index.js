var defaultGroupImg;
var defaultAccountImg;
var currentPage;
var loginDiv;
var mainDiv;
var errorModal;

$(window).on('load', function () {
    mainDiv = $('#mainDiv');
    loginDiv = $('#loginDiv');
    errorModal = $('#errorModal');
    $.ajaxSetup({
        beforeSend: function (xhr) {
            var token = $("meta[name='_csrf']").attr("content");
            var header = $("meta[name='_csrf_header']").attr("content");
            xhr.setRequestHeader(header, token);
        }
    });

    loadMainData();
    onChangeURL(location.pathname + location.search);
});

function searchFunction() {
    var search = $("#search").val();
    if (search !== '') {
        onChangeURL('/search?text=' + search);
    } else {
        errorModal.find('#text').text('Пустой запрос поиска!!!');
        errorModal.modal('show');
    }
}

function onChangeURL(url) {
    currentPage = url;
    window.history.pushState(null, null, url);
    console.log('onChangeURL : ' + currentPage);
    $('#middleContainer').empty();
    $('#rightColumn').empty();
    loginDiv.empty();
    show();
}

function show() {
    var page;
    switch (currentPage) {
        case '/index':
        case '/':
            $.get(context + '/csrf', function (data) {
                console.log(data);
                $.ajaxSetup({
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader(data.headerName, data.token);
                    }
                })
            });
            page = 'post';
            break;
        default:
            page = currentPage.slice(1);
    }
    page = page.charAt(0).toUpperCase() + page.slice(1);
    if (currentPage === '/registration' || currentPage === '/login') {
        loginDiv.show();
        mainDiv.hide();
        $('#loginDiv').load(context + '/jsp/' + page);
    } else {
        loginDiv.hide();
        mainDiv.show();
        $('#middleContainer').load(context + '/jsp/' + page);
    }
}


function getUrlVars(parameter) {
    return (location.search.split(parameter)[1] || '').split('&')[0].substr(1);
}

function img(image, defaultImg) {
    if (image === '' || image === null) {
        return defaultImg;
    }
    return image;
}

function loadMainData(load) {
    var page = location.pathname;
    if ((load !== true) && (page.match(/\/login/) !== null || page.match(/\/registration/) !== null)) {
        return;
    }
    console.log('loadMainData');
    $.get(context + "/getCurrentAccount", function (account) {
        $('#myId').val(account.id);
        var mainDiv = $("#mainInfo");

        function nameSetters(value, name) {
            if (value === '') {
                mainDiv.find('#p' + name).hide();
            } else {
                mainDiv.find('#p' + name).show();
                mainDiv.find('#i' + name).text(value);
            }
        }

        mainDiv.find('#avatar').attr('src', img(account.image, defaultAccountImg));
        nameSetters(account.name, 'Name');
        nameSetters(account.surname, 'Surname');
        nameSetters(account.patronymic, 'Patronymic');
        nameSetters((account.address.city !== '' ? account.address.city + ', ' : '') + account.address.country, 'Address');
        nameSetters(account.birthday, 'Birthday');
    });
}

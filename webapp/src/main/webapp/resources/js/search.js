var page = 3;
const pageSize = 2;
var all = 1;
var text;

$(document).ready(function () {
    $('.body').unbind();
    $("#search").autocomplete({
        source: function (request, response) {
            $.get(context + '/searchRegex', {text: request.term}, function (data) {
                response($.map(data, function (account) {
                    return {
                        value: account.name + ' ' + account.surname,
                        label: account.name + ' ' + account.surname + ' ' + account.patronymic,
                        id: account.id
                    }
                }));
            });
        },
        minLength: 1,
        select: function (event, ui) {
            event.preventDefault();
            $('#search').val("");
            onChangeURL("/account?id=" + ui.item.id);
        },
        open: function () {
            var auto = $('.ui-autocomplete');
            auto.css('width', '500px');
            auto.css('overflow-x', 'hidden');
        }
    }).keypress(function (e) {
        if (e.which === 13) {
            searchFunction();
            $(this).val("");
            $(this).autocomplete('close');
        }
    });

    $('#container').scroll(function () {
        var container = $('#container');
        if (all === 1 && container.scrollTop() + container.height() === container.get(0).scrollHeight) {
            getSearchResult(text, page, pageSize);
            ++page;
        }
    });

    $('body').on('click', '.searchForm', function () {
        onChangeURL("/account?id=" + $(this).find("#id").val());
    });
});


function getSearchResult(text, from, pageSize) {
    $.get(context + '/searchAjax', {text: text, page: from, size: pageSize}, function (data) {
        console.log('SIZE == ' + data.length);
        $.each(data, function (index, account) {
            var img = account.image;
            if (img === null) {
                img = defaultAccountImg;
            }
            var html = "<form class='w3-button w3-block w3-theme-l1' style='height: 82px; margin-bottom: 5px;' method='post' " +
                "class = searchForm" +
                "<input id='id' type='hidden' value='" + account.id + "'> " +
                "<img id='avatarAccount' src='" + img + "' " +
                "class='w3-card-2 w3-round w3-white w3-left' height='62px' width='62px'>" +
                "<div class='w3-col m2 w3-center' style='padding-left: 30px'>" +
                "Имя: <i class='name'>" + account.name + "</i><br>" +
                "Отчество: <i class='patronymic'>" + account.patronymic + "</i><br>" +
                "Фамилия: <i class='surname'>" + account.surname + "</i><br>" +
                "</div>" +
                "<button id='remove' class='w3-white w3-col m2 w3-right' style='width: 80px'>" +
                "Удалить" +
                "</button>" +
                "<button id='add' class=' w3-white w3-col m2 w3-right' style='width: 180px'>" +
                "Добавить в друзья" +
                "</button>" +
                "</form>";
            $('#container').append(html);
        });
    }).fail(function () {
        all = 0;
    });
}


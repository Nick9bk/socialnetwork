<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/avatar.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/w3.css">
    <script src="${pageContext.request.contextPath}/resources/js/avatar.js"></script>
</head>
<body>
<div class="containerAvatar inline w3-display-container" id="moduleAvatar">
    <label for="fileUploadImage">
        <img class="avatar w3-centered" id="avatar"/>
    </label>
    <input id="fileUploadImage" name="fileUpload" type="file" accept="image/jpeg" onchange="readURL(this)"/>
</div>
</body>
</html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=utf-8" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/login.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/w3.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/w3-theme-blue-grey.css">
    <script src="${pageContext.request.contextPath}/resources/js/sha.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/passHash.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/login.js"></script>
<body class="w3-theme-l5">
<div class="w3-top">
    <form id="loginForm" method="post" class="w3-card-2 w3-round w3-white">
        <div class="imgcontainer">
            <img src="${pageContext.request.contextPath}/resources/image/loginAvatar.jpg" alt="Avatar" class="Logo">
        </div>
        <div class="container-fluid ">
            <div class="form-group">
                <label><b>Логин</b></label>
                <input type="email" placeholder="Введите email" name="email" required>
            </div>
            <div class="form-group">
                <label><b>Пароль</b></label>
                <input type="password" placeholder="Введите пароль" name="password" id="password" required/>
            </div>
            <button type="button" id="login" class="btn btn-primary login">Войти</button>
            <div class="form-group">
                <label>
                    <input type="checkbox" checked="checked" name="rememberMe" value="true"/>
                    Запомнить меня
                </label>
            </div>
            <button type="button" class="btn btn-danger registrationButton"
                    onclick="onChangeURL('/registration');">
                Зарегестрироваться
            </button>
        </div>
    </form>
    <!-- Footer -->
    <footer id="footer" class="footer w3-container w3-theme-d3 ">
        <h5>Social network</h5>
    </footer>
</div>
</body>
</html>
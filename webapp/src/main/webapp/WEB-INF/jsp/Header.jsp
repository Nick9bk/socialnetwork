<head>
    <script>
        var context = "${pageContext.request.contextPath}";
        var defaultGroupImg = context + "/resources/image/group.png";
        var defaultAccountImg = context + "/resources/image/avatar.png";
    </script>
    <link rel="shortcut icon" type="image/x-icon"
          href="${pageContext.request.contextPath}/resources/image/favicon.ico"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/jquery-ui-themes/base/jquery-ui.css">
    <script src="${pageContext.request.contextPath}/webjars/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/webjars/jquery-ui/jquery-ui.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/js/bootstrap.js"></script>



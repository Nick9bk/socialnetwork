<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="modal fade" id="passwordEditModal" role="dialog">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Изменение пароля</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Старый пароль</label>
                    <input type="password" class="form-control" placeholder="Введите пароль" id="passwordOld"
                           name="passwordOld" required>
                </div>
                <div class="form-group">
                    <label>Новый пароль</label>
                    <input type="password" class="form-control" placeholder="Введите пароль" id="passwordNew"
                           name="passwordNew" required>
                </div>
                <div class="form-group">
                    <label>Повторите новый пароль</label>
                    <input type="password" class="form-control" placeholder="Повторите пароль" id="passwordNew2"
                           name="passwordNew2" required>
                </div>
                <div class="alert alert-warning" id="wrongPass" hidden>
                    <strong>Ошибка!</strong>
                    Пароли не совпадают!
                </div>
                <div class="alert alert-warning" id="errorEdit" hidden>
                    <strong>Ошибка!</strong>
                    Не удалось сменить пароль! Повторите ввод!
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary active" data-dismiss="modal">Нет</button>
                <button type="button" class="btn btn-primary active" id="savePassword">Да</button>
            </div>
        </div>
    </div>
</div>

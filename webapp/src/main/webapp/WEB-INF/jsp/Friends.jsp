<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <script src="${pageContext.request.contextPath}/resources/js/friends.js" type="text/javascript"></script>
</head>
<body>
<h6 class="w3-opacity w3-center w3-large">Мои друзья</h6>
<div class="w3-display-container" style=" height: 404px;">
    <ul class="nav nav-pills">
        <li id="CONFIRMED" class="active"><a onclick="changeList($('#CONFIRMED'))">Друзья</a></li>
        <li id="OUT"><a onclick="changeList($('#OUT'))">Исходящие заявки</a></li>
        <li id="IN"><a onclick="changeList($('#IN'))">Входящие заявки</a></li>
    </ul>
    <div id="accounts">
    </div>
</div>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/w3.css">
<head>
    <title>Title</title>
</head>
<body>
<!-- The Modal -->
<div id="id01" class="w3-modal">
    <div class="w3-modal-content">
        <form class="w3-container" action="index?param=information">
            <div class="w3-section">
                <label><b>Страна</b></label>
                <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Введите страну"
                       name="country">
                <label><b>Город</b></label>
                <input class="w3-input w3-border" type="text" placeholder="Введите город" name="City">
                <label><b>Улица</b></label>
                <input class="w3-input w3-border" type="text" placeholder="Введите улицу" name="street">
                <label><b>Дом</b></label>
                <input class="w3-input w3-border" type="text" placeholder="Введите дом" name="house">
                <label><b>Строение</b></label>
                <input class="w3-input w3-border" type="text" placeholder="Введите строение" name="structure">
                <label><b>Квартира</b></label>
                <input class="w3-input w3-border" type="text" placeholder="Введите квартиру" name="apartment">

                <button class="w3-btn w3-right w3-white w3-border"
                        onclick="document.getElementById('id01').style.display='none'">Close
                </button>
            </div>
        </form>
    </div>
</div>
</body>
</html>

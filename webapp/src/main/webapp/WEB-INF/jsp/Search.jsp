<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="${pageContext.request.contextPath}/resources/js/searchPage.js"></script>
</head>
<body>
<h3 class="text-center" style="padding-top: 5px; margin-top: 0px;">Результаты поиска</h3>
<div class="pre-scrollable" id="container" style="max-height: 453px;">
    <input type="hidden" value="${param['text']}" id="text"/>
</div>
</body>
</html>

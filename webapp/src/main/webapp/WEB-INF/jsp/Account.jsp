<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/w3.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/chat.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css"/>
    <script src="${pageContext.request.contextPath}/resources/js/account.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/sockjs-client/sockjs.js"></script>
    <script src="${pageContext.request.contextPath}/webjars/stomp-websocket/stomp.js"></script>
</head>
<body>
<h6 class="w3-opacity w3-center w3-large" style="margin-top: 0px">Просмотр аккаунта</h6>
<form class="w3-display-container" style="margin:20px" method="post">
    <input type="hidden" id="id"/>
    <p class="w3-center"><img id="avatarAccount"
                              class="w3-card-4 w3-round w3-white w3-center" height="120px" width="120px">
    </p>
    <h3 style="margin-top: 0px">
        <button class="btn btn-primary pull-left" type="button" id="openChat">
            Открыть чат
        </button>
    </h3>
    <div class="m2" style="margin-top: 50px">
        <div class="panel panel-info" style="margin-top: 40px">
            <div class="panel-heading form-group">
                <label id="accountName">
                </label>
            </div>
            <table class="table table-condensed">
                <tbody id="tableInfo">
                <tr>
                    <td>Дата регистрации :</td>
                    <td id="accountRegistrationDate"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</form>
<div class="modal fade" id="addOk" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Запрос!</h4>
            </div>
            <div class="modal-body">
                <p>Запрос на добавление в друзья отправлен!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary active" data-dismiss="modal"
                        onclick="location.reload();">Закрыть
                </button>
            </div>
        </div>
    </div>
</div>
</body>
</html>


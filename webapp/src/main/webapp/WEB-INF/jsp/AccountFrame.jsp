<%--
  Created by IntelliJ IDEA.
  User: nick
  Date: 06.06.17
  Time: 21:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<form class="w3-button w3-block w3-theme-l1" style="height: 82px; margin-bottom: 5px;" method="post"
      action="${pageContext.request.contextPath}/account?id=${account1.getId()}" onclick="this.submit()">

    <input id="id" type="hidden" value="${account1.getId()}">
    <img id="avatarAccount" src="${a:encoder(account1.getImage())}"
         class="w3-card-2 w3-round w3-white w3-left" height="62px" width="62px">
    <div class="w3-col m2 w3-center" style="padding-left: 30px">
        Имя: <i class="name"> ${account1.getName()} </i><br>
        Отчество: <i class="patronymic"> ${account1.getPatronymic()}</i><br>
        Фамилия: <i class="surname"> ${account1.getSurname()}</i><br>
    </div>
    <button id="remove" class=" w3-white w3-col m2 w3-right" style="width: 80px">
        Удалить
    </button>
    <button id="add" class=" w3-white w3-col m2 w3-right" style="width: 180px">
        Добавить в друзья
    </button>
</form>

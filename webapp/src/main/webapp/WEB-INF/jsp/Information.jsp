<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <script src="${pageContext.request.contextPath}/resources/js/sha.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/information.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/w3.css">
</head>
<body>
<h2 class="text-center" style="margin-top: 0px">Мой акаунт</h2>
<input id="xmlUpload" name="xmlUpload" type="file" accept="text/xml" style="display: none"/>
<div class="dropdown pull-right" style="z-index: 1;">
    <button class="glyphicon glyphicon-pencil btn btn-default dropdown-toggle"
            type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
        <li><label for="xmlUpload"> <a>Загрузить из файла</a></label></li>
        <li><label><a href="${pageContext.request.contextPath}/writeToXML">Записать в файл</a></label>
        </li>
        <li><label onclick="showEdit()"><a>Сменить пароль</a></label></li>
    </ul>
</div>
<form id="informationForm" method="post" class="container-fluid" enctype="multipart/form-data">
    <div class="container-fluid" id="allInfo" style="margin: 7px">
        <jsp:include page="Avatar.jsp"/>
        <div class="form-group">
            <label><b>Логин</b></label>
            <input class="form-control" type="email" name="email" id="email" readonly>
        </div>
        <div class="form-group">
            <label><b>Дата регистрации</b></label>
            <input class="form-control" type="date" id="registrationDate" readonly/>
        </div>
        <div class="form-group">
            <label>Имя</label>
            <input class="form-control" type="text" placeholder="Введите имя" name="name" id="name"/>
        </div>
        <div class="form-group">
            <label>Отчество</label>
            <input class="form-control" type="text" placeholder="Введите отчество" name="patronymic" id="patronymic"/>
        </div>
        <div class="form-group">
            <label>Фамилия</label>
            <input class="form-control" type="text" placeholder="Введите фамилию" name="surname" id="surname">
        </div>
        <div class="form-group">
            <label>Дата рождения</label>
            <input class="form-control" type="date" name="birthday" id="birthday">
        </div>

        <div class="form-group">
            <label>Skype</label>
            <input class="form-control" type="text" name="skype" id="skype">
        </div>

        <div class="form-group">
            <label>Icq</label>
            <input class="form-control" type="text" name="icq" id="icq">
        </div>

        <div class="form-group">
            <label>Адрес</label>
            <input type="hidden" id="homeAddress.id" name="homeAddress.id"/>
            <div class="form-group well">
                <div class="form-group row">
                    <div class="col-md-6">
                        <label class="col-md-4  control-label" for="country">Страна</label>
                        <div class="col-md-8">
                            <input class="form-control input-md" type="text" id="country" name="homeAddress.country"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="col-md-4  control-label" for="city">Город </label>
                        <div class="col-md-8" style="padding-right: 0px">
                            <input class="form-control input-md" type="text" id="city" name="homeAddress.city"/>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <label class="col-md-2 control-label" for="street">Улица</label>
                        <div class="col-md-10" style="padding-right: 0px">
                            <input class="form-control input-md" type="text" id="street" name="homeAddress.street"/>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <label class="col-md-4  control-label" for="house">Дом </label>
                        <div class="col-md-7">
                            <input class="form-control input-md" type="text" onkeypress="return isNumberKey(event)"
                                   id="house" name="homeAddress.house"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="col-md-5  control-label" for="structure">Корпус</label>
                        <div class="col-md-7">
                            <input class="form-control input-md" type="text" onkeypress="return isNumberKey(event)"
                                   id="structure" name="homeAddress.structure"/>
                        </div>
                    </div>
                    <div class="col-md-4" style="padding-right: 0px">
                        <label class="col-md-6  control-label" for="apartment">Квартира</label>
                        <div class="col-md-6">
                            <input class="form-control input-md" type="text" onkeypress="return isNumberKey(event)"
                                   id="apartment" name="homeAddress.apartment"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">

            <div class="alert alert-warning" id="wrongPhone" hidden>
                <strong>Ошибка!</strong>
                Введен не корректный формат телефона!
            </div>
            <label>Телефоны</label>
            <div class="row">
                <div class="control-group">
                    <div class="container-fluid" id="fields">
                    </div>
                </div>
            </div>
        </div>
        <button type="button" id="savebtn" class="btn btn-primary active form-control" data-toggle="modal"
                data-target="#myModal">
            Сохранить изменения
        </button>
    </div>
</form>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Подтверждение</h4>
            </div>
            <div class="modal-body">
                <p>Вы хотите применить изменения?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary active" data-dismiss="modal">Нет</button>
                <button type="button" class="btn btn-primary active" data-dismiss="modal" id="saveAll">Да</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="successfulSave" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p>Данные успешно сохраненны!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary active" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<jsp:include page="/WEB-INF/jsp/PasswordEdit.jsp"/>
</body>
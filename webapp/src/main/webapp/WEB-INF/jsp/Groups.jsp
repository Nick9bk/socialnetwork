<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="${pageContext.request.contextPath}/resources/js/groups.js" type="text/javascript"></script>
</head>
<body>
<button class="btn btn-primary" id="addGroup" style="right: 200px; position: absolute;">Добавить группу</button>
<h6 class="w3-opacity w3-center w3-large" style="margin-top: 0px">Мои группы</h6>

<div class="w3-display-container" style=" height: 404px;">
    <div class="w3-display-container" style=" height: 404px;">
        <ul class="nav nav-pills">
            <li id="myGroups" class="active"><a onclick="changeList($('#myGroups'))">Мои группы</a></li>
            <li id="allGroups"><a onclick="changeList($('#allGroups'))">Все группы</a></li>
        </ul>
        <div id="groupsContainer">
        </div>
    </div>
</div>

<div class="modal fade" id="descriptionEditor" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center">Создание новой группы</h4>
            </div>
            <div class="modal-body form-horizontal" style="height: 350px;">

                <div class="form-group">
                    <b class="col-sm-2">Название группы</b>
                    <div class="col-sm-10">
                        <input class="form-control" id="nameGroup">
                    </div>
                </div>
                <div class="form-group">
                    <ul>Описание группы</ul>
                    <textarea id="groupDescriptionTextArea" style="height: 230px; width: 90%; margin-left: 5%">
                </textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary active" data-dismiss="modal">Нет</button>
                <button type="button" class="btn btn-primary active" id="createGroup">Да</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/messages.css">
    <script src="${pageContext.request.contextPath}/resources/js/messages.js"></script>
</head>
<body>
<h6 class="w3-opacity w3-center w3-large">Мои сообщения</h6>
<div class="w3-display-container" style=" height: 423px;">
    <div class="panel-body msg_container_base" id="allMessages">
    </div>
</div>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
</head>
<body>
<div class="modal fade" id="modalError" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Ошибка</h4>
            </div>
            <div class="modal-body">
                <p id="errorString">Вы хотите применить изменения?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary active" data-dismiss="modal">Ок</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<jsp:include page="Header.jsp"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/w3.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/w3-theme-blue-grey.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/index.css">
<script src="${pageContext.request.contextPath}/resources/js/index.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/search.js"></script>
<meta name="_csrf" content="${_csrf.token}"/>
<meta name="_csrf_header" content="${_csrf.headerName}"/>
</head>
<input type="hidden" id="myId"/>
<title>Social Network</title>
<body class="w3-theme-l5" id="body">
<div id="loginDiv">
</div>
<div id="mainDiv">
    <div class="w3-top">
        <div class="w3-bar w3-theme-d2 w3-left-align w3-large">
            <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2"
               href="javascript:void(0);" onclick=""><i class="fa fa-bars"></i></a>
            <a onclick="onChangeURL('/index')" class="w3-bar-item w3-button w3-padding-large w3-theme-d4">
                <i class="glyphicon glyphicon-home w3-margin-right"></i>Home</a>
            <a onclick="onChangeURL('/information')" class="w3-bar-item w3-button w3-hide-small
        w3-padding-large w3-hover-white"
               title="Account Settings">
                <i class="glyphicon glyphicon-user"></i></a>
            <a onclick="onChangeURL('/messages')" class="w3-bar-item w3-button w3-hide-small w3-padding-large
        w3-hover-white" title="Messages">
                <i class="glyphicon glyphicon-envelope"></i></a>

            <a class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" title="Поиск..."
               onclick="searchFunction()"> <i class="glyphicon glyphicon-search"></i></a>

            <input class="w3-bar-item " id="search" name="search" placeholder="Поиск..."
                   style="height: 42px; color: black;">

            <a href="${pageContext.request.contextPath}/logout"
               class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white"
               title="Выйти">
                <img src="${pageContext.request.contextPath}/resources/image/logout.jpg" class="w3-circle"
                     style="height:25px;width:25px" alt="LogOut"></a>
        </div>
    </div>
    <!-- Page Container -->
    <div class="w3-container w3-content" id="mainContainer"
         style="max-width:1100px; margin-top:20px;min-height: 500px;">
        <!-- The Grid -->
        <div class="w3-row">
            <!-- Left Column -->
            <div class="w3-col m3">
                <!-- Profile -->
                <div class="w3-card-2 w3-round w3-white">
                    <div class="w3-container" id="mainInfo">
                        <p class="w3-center"><img class="w3-card-4" style="height:106px;width:106px" alt="Avatar"
                                                  id="avatar"></p>
                        <hr>
                        <p id="pName"> Имя : <i class="name" id="iName"></i></p>
                        <p id="pPatronymic"> Отчество : <i class="patronymic" id="iPatronymic"></i></p>
                        <p id="pSurname"> Фамилия : <i class="surname" id="iSurname"></i></p>
                        <p id="pAddress">
                            <i class="fa fa-home fa-fw w3-margin-right w3-text-theme" id="iAddress"></i>
                        </p>
                        <p id="pBirthday">
                            <span class="glyphicon glyphicon-gift" style="color:#3f51b5">    </span>
                            <i class="" id="iBirthday"></i>
                        </p>
                    </div>
                </div>
                <br>
                <!-- Accordion -->
                <div class="w3-card-2 w3-round">
                    <div class="w3-white">
                        <button id="account" onclick="onChangeURL('/information')"
                                class="w3-button w3-block w3-theme-l1 w3-left-align">
                            <i class="fa fa-cog fa-fw w3-margin-right"></i>Мой аккаунт
                        </button>
                    </div>
                    <div class="w3-white">
                        <button id="groups" onclick="onChangeURL('/groups')"
                                class="w3-button w3-block w3-theme-l1 w3-left-align"
                                style="margin-top: 6px;"><i class="fa fa-circle-o-notch fa-fw w3-margin-right">
                        </i>Мои группы
                        </button>
                    </div>
                    <div class="w3-white">
                        <button id="friends" onclick="onChangeURL('/friends')"
                                class="w3-button w3-block w3-theme-l1 w3-left-align"
                                style="margin-top: 6px;"><i class="fa fa-user fa-fw w3-margin-right"></i>Мои друзья
                        </button>
                    </div>
                    <div class="w3-white">
                        <button id="messages" onclick="onChangeURL('/messages')"
                                class="w3-button w3-block w3-theme-l1 w3-left-align"
                                style="margin-top: 6px;"><i class="fa fa-envelope fa-fw w3-margin-right"></i>Сообщения
                        </button>
                    </div>
                </div>
                <!-- End Left Column -->
            </div>
            <!-- Middle Column -->
            <div class="w3-col m7" id="middleColumn">
                <div class="w3-row-padding">
                    <div class="w3-col m12">
                        <div class="w3-card-2 w3-round w3-white" id="middleContainer" style=" min-height: 485px;">
                        </div>
                    </div>
                </div>
            </div>
            <%--Column--%>
            <div class="w3-col m4">
                <div class="w3-card-2 w3-round w3-white w3-center">
                    <div class="w3-container" id="rightColumn">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
</div>

<div class="modal fade" id="errorModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Ошибка!</h4>
            </div>
            <div class="modal-body">
                <p id="text"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary active" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
</body>
</html> 

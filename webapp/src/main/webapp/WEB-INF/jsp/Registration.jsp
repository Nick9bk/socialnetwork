<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html style="position:absolute;">
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/registration.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/w3.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/w3-theme-blue-grey.css">
    <script src="${pageContext.request.contextPath}/resources/js/passHash.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/sha.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/registration.js"></script>
</head>
<body class="w3-theme-l5" id="registrationBody">
<div class="w3-bar w3-theme-d2 w3-left-align w3-large">
    <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2"
       href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars"></i></a>
</div>
<div class="w3-top">
    <form id="regForm" class="w3-card-2 w3-round w3-white" enctype="multipart/form-data" method="post"
          style="width: 50%;">
        <h1>Регистрация</h1>
        <div>
            <jsp:include page="/WEB-INF/jsp/Avatar.jsp"/>
        </div>
        <div class="container-fluid">
            <div class="form-group">
                <label><b>Логин*</b></label>
                <input type="email" placeholder="Введите email" maxlength="25" name="email" id="email" required>
            </div>
            <div class="form-group">
                <label>Имя</label>
                <input type="text" placeholder="Иван" maxlength="15" name="name" id="name">
            </div>
            <div class="form-group">
                <label>Отчество</label>
                <input type="text" placeholder="Иванович" maxlength="15" name="patronymic" id="patronymic">
            </div>
            <div class="form-group">
                <label>Фамилия</label>
                <input type="text" placeholder="Иванов" maxlength="15" name="surname" id="surname">
            </div>
            <div class="form-group">
                <label>Дата рождения</label>
                <input type="date" name="birthday" id="birthday">
            </div>
            <div class="form-group">
                <label><b>Пароль*</b></label>
                <input type="password" placeholder="Введите пароль" id="password" name="password" required>
            </div>
            <div class="form-group">
                <label><b>Подтверждение пароля*</b></label>
                <input type="password" placeholder="Повторите пароль" id="password2" name="password2" required>
            </div>
            <button type="submit" class="registrationButton btn btn-primary" id="regOk">
                Зарегистрировать
            </button>
        </div>
        <div class="container1">
            <button type="button" class="btn btn-danger backButton"
                    onclick="onChangeURL('/login');">Назад
            </button>
        </div>
    </form>
</div>
<div class="modal fade" id="statusOperation" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p id="statusMessage"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary active" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>

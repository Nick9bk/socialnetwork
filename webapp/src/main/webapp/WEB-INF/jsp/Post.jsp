<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="${pageContext.request.contextPath}/resources/js/post.js" type="text/javascript"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/post.css">
</head>
<body id="postsBody">
<h2 class="text-center" style="margin-top: 0px">Моя стена</h2>
<button type="button" id="newPost" class="btn btn-primary pull-right"><i class="fa fa-pencil"></i>Post</button>
<hr>

<div class="container-fluid" id="posts">
    <div class="panel panel-default " id="newPostDiv">
        <div class="panel-heading">Новый пост:</div>
        <p id="post0" data-placeholder="Введите новый пост ..." contenteditable="true"></p>
    </div>
</div>
</body>
</html>
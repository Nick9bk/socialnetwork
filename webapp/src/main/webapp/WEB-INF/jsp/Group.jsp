<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="${pageContext.request.contextPath}/resources/js/group.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/group.css">
</head>
<body>

<div class="dropdown pull-right" style="" id="adminMenu" hidden>
    <button class="glyphicon glyphicon-pencil btn btn-default dropdown-toggle"
            type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="width: 340px;">
        <li id="editMode"><label>Режим редактирования группы</label></li>
    </ul>
</div>

<h3 id="groupName" class="text-center text-muted" style="margin-top: 0px "></h3>
<div class="row">
    <div class="col-md-3" style="margin-left: 30px">
        <img id="groupAvatar" class="img-thumbnail" style="" src=""/>
        <input id="fileUploadImage" name="fileUpload" type="file" accept="image/jpeg" onchange="readURL(this)"/>
    </div>
    <div class="col-md-4 col-md-offset-4">
        <button class="btn btn-primary btn-sm pull-right" id="joinBtn"></button>
    </div>
</div>

<div style="margin: 10px; height: 300px">
    <b>Описание:</b>
    <div id="description" class="form-control">
        <p class="text-primary" style="font-size: 15px;">
            <i id="iDescription" class="text-muted">${group.description}</i>
        </p>
    </div>
</div>

<div class="well well-sm" id="admins">
    <p class="text-muted">Администраторы группы</p>
    <div class="form-control" id="adminsList">
    </div>
</div>

<div class="well well-sm" id="creatorContainer">
    <p class="text-muted">Создатель группы</p>
    <%--<form id="creator" action="${pageContext.request.contextPath}/account?id=${group.getCreator().id}" method="post"
          class="form-group account" style="">
        <img id="avatarCreator" class="img-thumbnail  pull-left" src=""/>
        <i id="nameCreator" class="text-center"></i>
    </form>--%>
</div>

<div class="well well-sm" id="users">
    <p class="text-muted">Члены группы:</p>
    <div class="form-control" id="userList">
    </div>
</div>

<div class="modal fade" id="descriptionEditor" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center">Изменение описания группы</h4>
            </div>
            <div class="modal-body">
                <textarea id="groupDescriptionTextArea">
                </textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary active" data-dismiss="modal">Нет</button>
                <button type="button" class="btn btn-primary active" id="saveDescription">Да</button>
            </div>
        </div>
    </div>
</div>

<c:import url="${pageContext.request.contextPath}/WEB-INF/jsp/GroupMenus.jsp"/>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="usersMenu" class="dropdown clearfix menuMouse">
    <ul class="dropdown-menu rightButtonMenu" role="menu" aria-labelledby="dropdownMenu">
        <li id="removeUser"><a>Удалить из группы</a></li>
        <li id="appointAdmin"><a>Сделать администратором</a></li>
    </ul>
</div>

<div id="adminsMenu" class="dropdown clearfix menuMouse">
    <ul class="dropdown-menu rightButtonMenu" role="menu" aria-labelledby="dropdownMenu">
        <li id="removeAdmin"><a>Удалить администратора</a></li>
    </ul>
</div>

<div id="descriptionMenu" class="dropdown clearfix menuMouse">
    <ul class="dropdown-menu rightButtonMenu" role="menu" aria-labelledby="dropdownMenu">
        <li id="saveChangeDescription"><a>Изменить описание</a></li>
    </ul>
</div>
<div id="avatarMenu" class="dropdown clearfix menuMouse">
    <ul class="dropdown-menu rightButtonMenu" role="menu" aria-labelledby="dropdownMenu">
        <li id="changeAvatar"><a>Сменить аватар</a></li>
        <li id="removeAvatar"><a>Удалить аватар</a></li>
        <li id="saveChange"><a>Сохранить изменения</a></li>
    </ul>
</div>
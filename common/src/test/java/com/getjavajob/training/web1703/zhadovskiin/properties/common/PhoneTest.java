package com.getjavajob.training.web1703.zhadovskiin.properties.common;

import com.getjavajob.training.web1703.zhadovskiin.common.Phone;
import com.getjavajob.training.web1703.zhadovskiin.common.PhoneType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by nick on 07.05.17.
 */
public class PhoneTest {
    @Test
    public void equals() {
        Phone phone = new Phone("1234566", PhoneType.HOME);
        Phone phone1 = new Phone("1234566", PhoneType.HOME);
        assertEquals(phone, phone1);
    }

    @Test
    public void equalsFalse() {
        Phone account = new Phone("1234566", PhoneType.HOME);
        Phone account1 = new Phone("123", PhoneType.HOME);
        assertNotEquals(account, account1);
    }

}

package com.getjavajob.training.web1703.zhadovskiin.properties.common;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import com.getjavajob.training.web1703.zhadovskiin.common.Phone;
import com.getjavajob.training.web1703.zhadovskiin.common.PhoneType;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by nick on 29.04.17.
 */
public class AccountTest {
    @Test
    public void equals() {
        Account account = new Account();
        Account account1 = new Account();
        initAccount(account);
        initAccount(account1);
        assertEquals(account, account1);
    }

    @Test
    public void equalsFalse() {
        Account account = new Account();
        Account account1 = new Account();
        initAccount(account);
        initAccount(account1);
        account.setEmail("gmail@gmail.com");
        assertNotEquals(account, account1);
    }

    public void initAccount(Account account) {
        account.setId(123);
        account.setName("Николай");
        account.setSurname("Иванов");
        account.setPatronymic("Иванович");
        account.setBirthday(LocalDate.of(1988, Month.NOVEMBER, 3));
        account.setEmail("nick@bk.ru");
        account.setPhones(Arrays.asList(new Phone("84950179674", PhoneType.HOME),
                new Phone("89160719674", PhoneType.WORK)));
        account.setIcq("1123455");
        account.setSkype("nickkk");
        account.setOther("java");
    }
}

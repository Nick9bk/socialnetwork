package com.getjavajob.training.web1703.zhadovskiin.common;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class Friend implements Serializable {
    private static final long serialVersionUID = 1L;

    @OneToOne
    @JoinColumn(name = "friend_id")
    private Account account;

    @Column(name = "status")
    @JoinColumn(name = "friend_id")
    @Enumerated(EnumType.STRING)
    private StatusFriend status;

    public Friend() {
    }

    public Friend(Account account) {
        this.account = account;
    }

    public Friend(Account account, StatusFriend status) {
        this(account);
        this.status = status;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account friend) {
        this.account = friend;
    }


    public StatusFriend getStatus() {
        return status;
    }

    public void setStatus(StatusFriend status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Friend{" +
                "friend id=" + account.getId() +
                ", status='" + status + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Friend friend1 = (Friend) o;
        return account != null ? account.equals(friend1.account) : friend1.account == null;
    }

    @Override
    public int hashCode() {
        int result = account != null ? account.hashCode() : 0;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }
}

package com.getjavajob.training.web1703.zhadovskiin.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import org.springframework.data.annotation.Transient;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by nick on 27.04.17.
 */
@Entity
@XStreamAlias("Account")
@Table(name = "accounts")
public class Account extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private String name;
    private String surname;
    private String patronymic;

    @XStreamOmitField
    @ElementCollection
    @CollectionTable(name = "friends", joinColumns = @JoinColumn(name = "id"))
    private List<Friend> friends;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;

    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "account", fetch = FetchType.LAZY, orphanRemoval = true)
    @XStreamImplicit(itemFieldName = "Phone")
    private List<Phone> phones;

    @XStreamOmitField
    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "account", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Post> posts;

    @XStreamOmitField
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinTable(name = "groups_link",
            joinColumns = @JoinColumn(name = "account_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "group_id", referencedColumnName = "id"))
    private List<Group> groups;

    private String email;
    private String icq;
    private String skype;
    private String other;
    @OneToOne(cascade = {CascadeType.ALL}, mappedBy = "account", fetch = FetchType.LAZY)
    private Address homeAddress;
    @XStreamOmitField
    private String password;
    @XStreamOmitField
    private LocalDate registrationDate;
    @XStreamOmitField
    private String image;

    @XStreamOmitField
    @OrderBy("date")
    @OneToMany(mappedBy = "fromAccount", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Message> messagesFrom;

    @XStreamOmitField
    @OneToMany(mappedBy = "toAccount", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @OrderBy("date")
    private List<Message> messagesTo;


    public Account() {
        friends = new ArrayList<>();
        phones = new ArrayList<>();
        posts = new ArrayList<>();
    }

    public List<Friend> getFriends() {
        return friends;
    }

    public void setFriends(List<Friend> friends) {
        this.friends = friends;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || !(o instanceof Account)) {
            return false;
        }
        Account account = (Account) o;
        return email.equals(account.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, surname, patronymic, birthday, phones, email, icq,
                skype, other);
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    @Override
    public String toString() {
        return "Account{" +
                super.toString() +
                ", name='" + name + '\'' +
                "surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", birthday=" + birthday +
//                ", phones='" + phones + '\'' +
                ", email='" + email + '\'' +
                ", icq='" + icq + '\'' +
                ", skype='" + skype + '\'' +
                ", other='" + other + '\'' +
                ", registrationDate='" + registrationDate + '\'' +
                ", homeAddress = '" + homeAddress + '\'' +
                '}';
    }


    public Friend getFriend(int id) {
        for (Friend friend : friends) {
            if (friend.getAccount().getId() == id) {
                return friend;
            }
        }
        return null;
    }

    public void addFriend(Friend friend) {
        friends.add(friend);
    }

    public void removeFriend(Friend friend) {
        friends.remove(friend);
    }

    public Address getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(Address homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<Message> getMessagesFrom() {
        return messagesFrom;
    }

    public void setMessagesFrom(List<Message> messagesFrom) {
        this.messagesFrom = messagesFrom;
    }

    public List<Message> getMessagesTo() {
        return messagesTo;
    }

    public void setMessagesTo(List<Message> messagesTo) {
        this.messagesTo = messagesTo;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
}

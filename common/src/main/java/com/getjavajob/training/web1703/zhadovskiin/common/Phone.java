package com.getjavajob.training.web1703.zhadovskiin.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import javax.persistence.*;

@XStreamAlias("Phone")
@Entity
@Table(name = "phones")
public class Phone extends BaseEntity {
    private static final long serialVersionUID = 1L;

    @ManyToOne(fetch = FetchType.LAZY)
    @XStreamOmitField
    @JsonIgnore
    private Account account;

    private String phone;

    @Enumerated(EnumType.STRING)
    private PhoneType type;

    public Phone() {
    }

    public Phone(String phone, PhoneType type) {
        this.type = type;
        this.phone = phone;
    }

    public Phone(Account account, String phone, PhoneType type) {
        this(phone, type);
        this.account = account;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public PhoneType getType() {
        return type;
    }

    public void setType(PhoneType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phone phone1 = (Phone) o;
        return phone != null ? (phone.equals(phone1.phone) && phone1.type == type) : phone1.phone == null;
    }

    @Override
    public int hashCode() {
        return phone != null ? phone.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "type= " + type + ", " +
                " phone= '" + phone + '\'' +
                '}';
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}

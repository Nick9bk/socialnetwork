package com.getjavajob.training.web1703.zhadovskiin.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import javax.persistence.*;
import java.util.List;

/**
 * Created by nick on 27.04.17.
 */
@Entity
@Table(name = "groups")
@XStreamAlias("Group")
public class Group extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private String name;
    @XStreamOmitField
    private String image;
    private String description;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "group_admins", joinColumns = @JoinColumn(name = "group_id"),
            inverseJoinColumns = @JoinColumn(name = "account_id"))
    @MapKey(name = "id")
    private List<Account> admins;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "groups_link", joinColumns = @JoinColumn(name = "group_id"),
            inverseJoinColumns = @JoinColumn(name = "account_id"))
    @MapKey(name = "id")
    private List<Account> users;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "creator", referencedColumnName = "id", nullable = false)
    private Account creator;

    public Group() {
    }

    public Group(int id) {
        setId(id);
    }

    @Override
    public String toString() {
        return "Group{" +
                super.toString() +
                ", name='" + name + '\'' +
                ", description = " + description +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Account> getAdmins() {
        return admins;
    }

    public void setAdmins(List<Account> admins) {
        this.admins = admins;
    }

    public Account getCreator() {
        return creator;
    }

    public void setCreator(Account creator) {
        this.creator = creator;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Account> getUsers() {
        return users;
    }

    public void setUsers(List<Account> users) {
        this.users = users;
    }
}

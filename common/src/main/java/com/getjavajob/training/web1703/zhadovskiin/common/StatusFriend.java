package com.getjavajob.training.web1703.zhadovskiin.common;

public enum StatusFriend {
    NONE,
    OUT,
    IN,
    CONFIRMED
}

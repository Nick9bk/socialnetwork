package com.getjavajob.training.web1703.zhadovskiin.common;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "message")
@NamedQueries({
        @NamedQuery(name = "selectMessage",
                query = "FROM Message WHERE fromAccount.id = :from OR toAccount.id = :to ORDER BY sendingDate")
})
public class Message extends BaseEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "from_id")
    private Account fromAccount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "to_id")
    private Account toAccount;

    private String text;

    @Column(name = "date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime sendingDate;

    public Account getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(Account fromAccount) {
        this.fromAccount = fromAccount;
    }

    public Account getToAccount() {
        return toAccount;
    }

    public void setToAccount(Account toAccount) {
        this.toAccount = toAccount;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getSendingDate() {
        return sendingDate;
    }

    public void setSendingDate(LocalDateTime sendingDate) {
        this.sendingDate = sendingDate;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + getId() +
                ", fromAccount=" + fromAccount.getId() +
                ", toAccount=" + toAccount.getId() +
                ", text='" + text + '\'' +
                ", sendingDate=" + sendingDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Message message = (Message) o;

        if (fromAccount != null ? !fromAccount.equals(message.fromAccount) : message.fromAccount != null) return false;
        if (toAccount != null ? !toAccount.equals(message.toAccount) : message.toAccount != null) return false;
        if (text != null ? !text.equals(message.text) : message.text != null) return false;
        return sendingDate != null ? sendingDate.equals(message.sendingDate) : message.sendingDate == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (fromAccount != null ? fromAccount.hashCode() : 0);
        result = 31 * result + (toAccount != null ? toAccount.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (sendingDate != null ? sendingDate.hashCode() : 0);
        return result;
    }
}

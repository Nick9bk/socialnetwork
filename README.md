####Functionality:
* registration
* authentication(Spring Security)
* ajax search with scroll loading 
* display profile
* edit profile
* upload and download avatar
* users export/import to xml
* adding/deleting friends
* chat with user 
* single page application
 
####Tools:
JDK 8, Spring 4, JPA 2 / Hibernate 4, jQuery 2, Twitter Bootstrap 3, JUnit 4, Mockito, Maven 3, Git / Bitbucket,
 Tomcat, MySQL, PostgreSQL, IntelliJIDEA 16, WebSocket.


Java Web, 09 2017

_  
**Zhadovskii Nikolai**  
Training getJavaJob   
[http://www.getjavajob.com](http://www.getjavajob.com)

package com.getjavajob.training.web1703.zhadovskiin.service;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import com.getjavajob.training.web1703.zhadovskiin.common.Post;
import com.getjavajob.training.web1703.zhadovskiin.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class PostService {
    private PostRepository postRepository;

    @Autowired
    public PostService(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Transactional
    public Post addPost(Account account, String text) {
        return postRepository.save(new Post(account, LocalDateTime.now(), text));
    }

    @Transactional
    public void removePost(int id) {
        postRepository.delete(id);
    }

    @Transactional
    public List<Post> getPosts(int accountId) {
        return postRepository.findByAccountId(accountId);
    }
}

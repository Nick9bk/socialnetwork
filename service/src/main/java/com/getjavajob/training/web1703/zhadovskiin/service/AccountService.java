package com.getjavajob.training.web1703.zhadovskiin.service;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import com.getjavajob.training.web1703.zhadovskiin.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by nick on 02.05.17.
 */
@Service
public class AccountService {
    private AccountRepository accountDao;

    @Autowired
    public AccountService(AccountRepository accountDao) {
        this.accountDao = accountDao;
    }


    @Transactional
    public int createAccount(Account account) {
        return accountDao.save(account).getId();
    }

    @Transactional
    public void editAccount(Account account) {
        accountDao.save(account);
    }

    @Transactional
    public void removeAccount(Account account) {
        accountDao.delete(account.getId());
    }

    @Transactional
    public List<Account> getAllAccounts() {
        return accountDao.findAll();
    }

    @Transactional
    public Account getAccount(String email) {
        return accountDao.findByEmail(email);
    }

    @Transactional
    public Account getAccount(int id) {
        return accountDao.findById(id);
    }

    @Transactional
    public List<Account> getByRegex(String regex) {
        return accountDao.findByRegex(regex, null);
    }

    @Transactional
    public List<Account> getByRegex(String regex, int page, int size) {
        return accountDao.findByRegex(regex, new PageRequest(page, size));
    }
}

package com.getjavajob.training.web1703.zhadovskiin.service;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import com.getjavajob.training.web1703.zhadovskiin.common.Message;
import com.getjavajob.training.web1703.zhadovskiin.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class MessageService {
    private MessageRepository messageDao;

    @Autowired
    public MessageService(MessageRepository messageDao) {
        this.messageDao = messageDao;
    }

    @Transactional
    public List<Message> getMessage(Account account) {
        return messageDao.findByAccountId(account.getId());
    }

    @Transactional
    public Message createMessage(Account from, Account to, String text) {
        Message message = new Message();
        message.setToAccount(to);
        message.setFromAccount(from);
        message.setText(text);
        message.setSendingDate(LocalDateTime.now());
        return messageDao.save(message);
    }

    @Transactional
    public List<Message> getMessageOfFriends(int from, int to, int start) {
        return messageDao.getMessageOfFriends(from, to);
    }
}

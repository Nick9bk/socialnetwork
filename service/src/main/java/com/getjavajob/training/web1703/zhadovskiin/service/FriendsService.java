package com.getjavajob.training.web1703.zhadovskiin.service;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import com.getjavajob.training.web1703.zhadovskiin.common.Friend;
import com.getjavajob.training.web1703.zhadovskiin.common.StatusFriend;
import com.getjavajob.training.web1703.zhadovskiin.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Service
public class FriendsService {
    private AccountRepository accountDao;

    @Autowired
    public FriendsService(AccountRepository accountDao) {
        this.accountDao = accountDao;
    }


    @Transactional
    public List<Friend> getFriends(Account account) {
        return accountDao.findById(account.getId()).getFriends();
    }

    @Transactional
    public void removeFriend(Account account, Account friend) {
        account.getFriends().remove(new Friend(friend));
        friend.getFriends().remove(new Friend(account));
        accountDao.save(friend);
        accountDao.save(account);
    }

    @Transactional
    public void addFriend(Account account, Account friend, StatusFriend statusFriend) {
        switch (statusFriend) {
            case IN:
                removeFriend(account, friend);
                account.addFriend(new Friend(friend, StatusFriend.CONFIRMED));
                friend.addFriend(new Friend(account, StatusFriend.CONFIRMED));
                accountDao.save(friend);
                accountDao.save(account);
                break;
            case NONE:
                account.addFriend(new Friend(friend, StatusFriend.OUT));
                friend.addFriend(new Friend(account, StatusFriend.IN));
                accountDao.save(friend);
                accountDao.save(account);
                break;
            case CONFIRMED:
                break;
            case OUT:
                break;
        }
    }

    public List<Friend> getFriendsByStatus(StatusFriend status, Account account) {
        List<Friend> friends = new LinkedList<>();
        for (Friend friend : account.getFriends()) {
            if (friend.getStatus() == status) {
                friends.add(friend);
            }
        }
        return friends;
    }


    public StatusFriend getStatusFriend(Account account, Account friend) {
        int ind = account.getFriends().indexOf(new Friend(friend));
        return ind == -1 ? StatusFriend.NONE : account.getFriends().get(ind).getStatus();
    }
}

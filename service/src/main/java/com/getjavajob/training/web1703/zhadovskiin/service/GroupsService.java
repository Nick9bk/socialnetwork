package com.getjavajob.training.web1703.zhadovskiin.service;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import com.getjavajob.training.web1703.zhadovskiin.common.Group;
import com.getjavajob.training.web1703.zhadovskiin.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class GroupsService {

    private GroupRepository groupDao;

    @Autowired
    public GroupsService(GroupRepository groupDao) {
        this.groupDao = groupDao;
    }

    @Transactional
    public List<Group> getAllGroups() {
        return groupDao.findAll();
    }

    @Transactional
    public Group getById(int id) {
        return groupDao.findById(id);
    }

    @Transactional
    public void updateGroup(Group group) {
        groupDao.save(group);
    }

    @Transactional
    public boolean saveImage(int id, String image) {
        Group group = groupDao.findById(id);
        if (group == null) {
            return false;
        }
        group.setImage(image.isEmpty() ? null : image);
        groupDao.save(group);
        return true;
    }

    @Transactional
    public boolean saveDescription(int id, String description) {
        Group group = groupDao.findById(id);
        if (group == null) {
            return false;
        }
        group.setDescription(description);
        groupDao.save(group);
        return true;
    }

    @Transactional
    public void createGroup(Group group) {
        groupDao.save(group);
    }

    public boolean isAdmin(Group group, Account account) {
        return group.getAdmins().contains(account);
    }

    public boolean isMembers(Group group, Account account) {
        return group.getUsers().contains(account);
    }

    @Transactional
    public List<Group> getGroupByAccountId(int id) {
        return groupDao.getGroupByAccountId(id);
    }
}

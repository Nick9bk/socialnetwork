package com.getjavajob.training.web1703.zhadovskiin.service;

import com.getjavajob.training.web1703.zhadovskiin.common.Group;
import com.getjavajob.training.web1703.zhadovskiin.repository.GroupRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GroupServiceTest extends BasicServiceTest {
    @Mock
    private GroupRepository groupDao;
    private Group group;

    @InjectMocks
    private GroupsService groupsService;

    @Before
    public void init() {
        super.init();
        initGroup();
    }

    @Test
    public void saveImageTest() {
        when(groupDao.findById(123)).thenReturn(group);
        groupsService.saveImage(123, "1234");
        verify(groupDao).findById(123);
        verify(groupDao).save(group);
    }

    @Test
    public void saveDescriptionTest() {
        when(groupDao.findById(123)).thenReturn(group);
        groupsService.saveDescription(123, "1234");
        verify(groupDao).findById(123);
        verify(groupDao).save(group);
    }

    @Test
    public void updateGroupTest() {
        groupsService.updateGroup(group);
        verify(groupDao).save(group);
    }

    @Test
    public void getByIdTest() {
        when(groupDao.findById(any(Integer.class))).thenReturn(group);
        groupsService.getById(123);
        verify(groupDao).findById(any(Integer.class));
    }

    @Test
    public void getAllGroupsTest() {
        when(groupDao.findAll()).thenReturn(Arrays.asList(group));
        groupsService.getAllGroups();
        verify(groupDao).findAll();
    }

    private void initGroup() {
        group = new Group();
        group.setId(1);
        group.setCreator(account);
    }
}

package com.getjavajob.training.web1703.zhadovskiin.service;

import com.getjavajob.training.web1703.zhadovskiin.common.Message;
import com.getjavajob.training.web1703.zhadovskiin.repository.MessageRepository;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceTest extends BasicServiceTest {

    @Mock
    private MessageRepository messageDao;

    @InjectMocks
    private MessageService messageService;

    @BeforeClass
    public static void initClass() {
    }

    @Before
    public void init() {
        super.init();
    }

    @Test
    public void getMessageTest() {
        List<Message> messages = Arrays.asList(createMessage());
        when(messageDao.findByAccountId(account.getId())).thenReturn(messages);
        List<Message> messagesTest = messageService.getMessage(account);
        verify(messageDao).findByAccountId(account.getId());
        assertEquals(messages, messagesTest);
    }

    @Test
    public void createMessageTest() {
        Message message = createMessage();
        messageService.createMessage(message.getFromAccount(), message.getToAccount(), message.getText());
        verify(messageDao).save(message);
    }

    @Test
    public void getMessageOfFriendsTest() {
        List<Message> messages = Arrays.asList(createMessage());
        when(messageDao.getMessageOfFriends(account.getId(), account1.getId())).thenReturn(messages);
        List<Message> messagesTest = messageService.getMessageOfFriends(account.getId(), account1.getId(), 0);
        verify(messageDao).getMessageOfFriends(account.getId(), account1.getId());
        assertEquals(messages, messagesTest);
    }

    private Message createMessage() {
        Message message = new Message();
        message.setToAccount(account);
        message.setFromAccount(account1);
        message.setText("test");
        message.setSendingDate(LocalDateTime.now());
        return message;
    }
}

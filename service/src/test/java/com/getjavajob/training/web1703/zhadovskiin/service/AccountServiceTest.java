package com.getjavajob.training.web1703.zhadovskiin.service;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import com.getjavajob.training.web1703.zhadovskiin.repository.AccountRepository;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by nick on 03.05.17.
 */
@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest extends BasicServiceTest {

    @Mock
    private AccountRepository accountDao;

    @InjectMocks
    private AccountService accountManager;

    @BeforeClass
    public static void initClass() {
    }

    @Before
    public void init() {
        super.init();
    }

    @Test
    public void deleteAccount() {
        accountManager.removeAccount(account);
        verify(accountDao).delete(account.getId());
    }

    @Test
    public void createAccount() {
        when(accountDao.save(any(Account.class))).thenReturn(account);
        accountManager.createAccount(account);
        verify(accountDao).save(account);
    }

    @Test
    public void getAllAccounts() {
        when(accountDao.findAll()).thenReturn(Arrays.asList(account1));
        List<Account> friends = accountManager.getAllAccounts();
        verify(accountDao).findAll();
        assertEquals(Arrays.asList(account1), friends);
    }
}

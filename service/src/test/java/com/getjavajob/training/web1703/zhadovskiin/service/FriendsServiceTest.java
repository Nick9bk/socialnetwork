package com.getjavajob.training.web1703.zhadovskiin.service;

import com.getjavajob.training.web1703.zhadovskiin.common.Friend;
import com.getjavajob.training.web1703.zhadovskiin.common.StatusFriend;
import com.getjavajob.training.web1703.zhadovskiin.repository.AccountRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FriendsServiceTest extends BasicServiceTest {

    @Mock
    private AccountRepository accountDao;

    @InjectMocks
    private FriendsService friendsService;

    @Before
    public void init() {
        super.init();
    }

    @Test
    public void getFriends() {
        account.addFriend(new Friend(account1, StatusFriend.NONE));
        when(accountDao.findById(123)).thenReturn(account);
        List<Friend> friends = friendsService.getFriends(account);
        verify(accountDao).findById(123);
        List<Friend> friends1 = new ArrayList<>();
        friends1.add(new Friend(account1));
        assertEquals(friends1, friends);
    }

    @Test
    public void addFriend() throws SQLException {
        friendsService.addFriend(account, account1, StatusFriend.IN);
        verify(accountDao, times(2)).save(account);
    }

    @Test
    public void removeFriend() {
        friendsService.removeFriend(account, account1);
        verify(accountDao).save(account);
    }

}

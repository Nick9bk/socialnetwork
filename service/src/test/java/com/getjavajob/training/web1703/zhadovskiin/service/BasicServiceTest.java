package com.getjavajob.training.web1703.zhadovskiin.service;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;

import java.time.LocalDate;
import java.time.Month;

public class BasicServiceTest {
    protected Account account;
    protected Account account1;

    public void init() {
        account = new Account();
        account1 = new Account();
        initAccount(account);
        initAccount(account1);
        account1.setId(122);
        account1.setEmail("niii@gmail.com");
    }


    public void initAccount(Account account) {
        account.setId(123);
        account.setName("Николай");
        account.setSurname("Иванов");
        account.setPatronymic("Иванович");
        account.setBirthday(LocalDate.of(1988, Month.NOVEMBER, 3));
        account.setEmail("nick@bk.ru");
        account.setIcq("1123455");
        account.setSkype("nickkk");
        account.setOther("java");
    }
}

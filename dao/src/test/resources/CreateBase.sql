DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(15) DEFAULT NULL,
  `surname` varchar(15) DEFAULT NULL,
  `patronymic` varchar(15) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `phones` varchar(15) DEFAULT NULL,
  `workPhone` varchar(15) DEFAULT NULL,
  `email` varchar(15) DEFAULT NULL,
  `icq` varchar(15) DEFAULT NULL,
  `skype` varchar(15) DEFAULT NULL,
  `other` text,
  `registrationDate` date,
  `password` char(64),
  `image` blob DEFAULT NULL
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `friends`;
CREATE TABLE `friends` (
  `id` int(11) unsigned NOT NULL,
  `friend_id` int(11) unsigned NOT NULL,
  `status` varchar(9) NOT NULL check (`status` in ('CONFIRMED', 'IN', 'OUT', 'NONE')) ,
  CONSTRAINT `fk_friends_1` FOREIGN KEY (`id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_friends_2` FOREIGN KEY (`friend_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(15) DEFAULT NULL,
  `creator` int(11) unsigned NOT NULL,
  `image` blob,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  CONSTRAINT `fk_groups_1` FOREIGN KEY (`creator`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `group_admins`;
CREATE TABLE `group_admins` (
  `group_id` int(10) unsigned NOT NULL,
  `account_id` int(10) unsigned NOT NULL,
  CONSTRAINT `fk_group_admins_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_group_admins_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `phones`;
CREATE TABLE `phones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(10) unsigned NOT NULL,
  `phone` varchar(15) NOT NULL,
  `type` varchar(4) NOT NULL check (`type` in ('WORK','HOME')), -- enum ('WORK','HOME')
  CONSTRAINT `fk_phones_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(10) unsigned NOT NULL,
  `country` varchar(15) DEFAULT NULL,
  `city` varchar(15) DEFAULT NULL,
  `street` varchar(45) DEFAULT NULL,
  `house` int(11) DEFAULT NULL,
  `structure` int(11) DEFAULT NULL,
  `apartment` int(11) DEFAULT NULL,
  CONSTRAINT `fk_homeAdress_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `groups_link`;
CREATE TABLE `groups_link` (
  `group_id` int(11) unsigned NOT NULL,
  `account_id` int(11) unsigned NOT NULL,
  CONSTRAINT `fk_groups_link_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_groups_link_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `from_id` int(11) unsigned NOT NULL,
  `to_id` int(11) unsigned NOT NULL,
  `text` mediumtext NOT NULL,
  CONSTRAINT `fk_message_from` FOREIGN KEY (`from_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_message_to` FOREIGN KEY (`to_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) unsigned NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `text` text NOT NULL,
  CONSTRAINT `fk_posts_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
) ENGINE=InnoDB;
commit;
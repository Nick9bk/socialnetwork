package com.getjavajob.training.web1703.zhadovskiin.repository;

import com.getjavajob.training.web1703.zhadovskiin.common.Group;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-context-test.xml"})
@Sql(value = {"classpath:DeleteAccounts.sql",
        "classpath:DeleteGroups.sql",
        "classpath:DeleteAdmins.sql",
        "classpath:InsertAccount.sql",
        "classpath:InsertGroups.sql",
        "classpath:InsertGroupsLink.sql",
        "classpath:InsertAdmins.sql"})
@Transactional
public class GroupRepositoryTest {
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private AccountRepository accountDao;

    @Test
    public void findAll() {
        List<Group> list = Arrays.asList(new Group(13), new Group(33));
        assertEquals(list, groupRepository.findAll());
        System.out.println(groupRepository.findAll());
    }

    @Test
    public void findById() {
        assertEquals(new Group(13), groupRepository.findById(13));
    }

    @Test
    public void findByName() {
        assertEquals(Arrays.asList(new Group(13)), groupRepository.findByName("Sport"));
    }

    @Test
    public void deleteById() {
        groupRepository.deleteById(13);
        assertEquals(Arrays.asList(new Group(33)), groupRepository.findAll());
    }

    @Test
    @Transactional
    public void update() {
        Group group = groupRepository.findById(13);
        assertEquals("Sport", group.getName());
        group.setName("Volleyball");
        group = groupRepository.save(group);
        assertEquals(groupRepository.findById(13).getName(), group.getName());
    }

    @Test
    public void insert() {
        Group group = new Group();
        group.setCreator(accountDao.findById(123));
        group.setName("Volleyball");
        group = groupRepository.save(group);
        assertEquals(groupRepository.findById(group.getId()).getName(), group.getName());
        assertEquals(groupRepository.findById(group.getId()), group);
    }

    @Test
    public void getGroupByAccountIdTest() {
        System.out.println(groupRepository.getGroupByAccountId(123));
    }
}
package com.getjavajob.training.web1703.zhadovskiin.repository;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import com.getjavajob.training.web1703.zhadovskiin.common.Post;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.time.LocalDateTime;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-context-test.xml"})
@Sql(value = {"classpath:DeletePosts.sql",
        "classpath:InsertPosts.sql"})
public class PostRepositoryTest {
    private Account account;

    @Autowired
    private PostRepository postRepository;
    @Autowired
    private AccountRepository accountRepository;

    @Before
    public void init() {
        account = accountRepository.findById(123);
    }
    @Test
    public void findByAccountIdTest() {
        assertEquals(postRepository.findByAccountId(123), Arrays.asList(
                new Post(1, account, LocalDateTime.parse("2017-05-30T19:43:06"), "Hello"),
                new Post(2, account, LocalDateTime.parse("2011-05-30T19:43:06"), "Hello - 1")));
    }

    @Test
    public void deleteTest() {
        postRepository.delete(1);
        assertEquals(postRepository.findByAccountId(123), Arrays.asList(
                new Post(2, account, LocalDateTime.parse("2011-05-30T19:43:06"), "Hello - 1")));
    }

    @Test
    public void saveTest() {
        Post post = postRepository.save(new Post(account, LocalDateTime.parse("2010-05-30T19:43:06"), "Hello - 3"));
        Account account = accountRepository.findById(123);
        assertEquals(postRepository.findByAccountId(123), Arrays.asList(
                new Post(1, account, LocalDateTime.parse("2017-05-30T19:43:06"), "Hello"),
                new Post(2, account, LocalDateTime.parse("2011-05-30T19:43:06"), "Hello - 1"), post));
    }
}

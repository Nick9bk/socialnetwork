package com.getjavajob.training.web1703.zhadovskiin.repository;

import com.getjavajob.training.web1703.zhadovskiin.common.Message;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-context-test.xml"})
@Sql(value = {"classpath:DeleteAccounts.sql",
        "classpath:InsertAccount.sql",
        "classpath:InsertMessage.sql",})
@Transactional
public class MessageRepositoryTest {
    @Autowired
    private MessageRepository messageDao;

    @Autowired
    private AccountRepository accountDao;

    private Message createMessage() {
        Message message = new Message();
        message.setText("test test");
        message.setFromAccount(accountDao.findById(122));
        message.setToAccount(accountDao.findById(123));
        message.setSendingDate(LocalDateTime.parse("2017-05-30T19:43:06"));
        message.setId(1);
        return message;
    }

    @Test
    public void getMessageByIdTest() {
        Message message = createMessage();
        assertEquals(messageDao.findByAccountId(122), Arrays.asList(message));
    }

    @Test
    public void createMessageTest() {
        Message message = createMessage();
        message.setId(0);
        message = messageDao.save(message);
        assertEquals(messageDao.findByAccountId(122), Arrays.asList(createMessage(), message));
    }

    @Test
    public void getMessageOfFriendsStartFromTest() {
        Message message = createMessage();
        message.setId(0);
        message.setSendingDate(LocalDateTime.now());
        message = messageDao.save(message);
        Message message2 = createMessage();
        message2.setId(1);
        assertEquals(messageDao.getMessageOfFriends(122, 123), Arrays.asList(message2, message));
    }
}

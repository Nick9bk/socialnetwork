package com.getjavajob.training.web1703.zhadovskiin.repository;

import com.getjavajob.training.web1703.zhadovskiin.common.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by nick on 29.04.17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-context-test.xml"})
@Sql(value = {"classpath:DeleteAccounts.sql",
        "classpath:DeleteGroups.sql",
        "classpath:InsertAccount.sql",
        "classpath:InsertPhone.sql",
        "classpath:InsertAddress.sql",
        "classpath:InsertFriend.sql",
        "classpath:InsertGroups.sql",
        "classpath:InsertGroupsLink.sql",
        "classpath:InsertMessage.sql",})
public class AccountRepositoryTest {

    @Autowired
    private AccountRepository accountDao;

    @Test
    @Transactional
    public void add() {
        Account account = new Account();
        initAccount(account);
        accountDao.save(account);
        Account account1 = accountDao.findById(account.getId());
        assertEquals(account, account1);
    }

    @Test
    @Transactional
    public void update() {
        String actual = "Sergey";
        Account account = accountDao.findById(123);
        assertNotEquals(actual, account.getName());
        account.setName(actual);
        accountDao.save(account);
        account = accountDao.findById(account.getId());
        assertEquals(account.getName(), actual);
    }

    @Test
    @Transactional
    public void delete() {
        int id = 123;
        Account account = accountDao.findById(id);
        assertNotNull(account);
        accountDao.deleteById(id);
        account = accountDao.findById(id);
        assertNull(account);
    }

    @Test
    @Transactional
    public void get() {
        Account account = new Account();
        initAccount(account);
        Account account1 = accountDao.findById(account.getId());
        assertEquals(account, account1);
        System.out.println(account1.getHomeAddress());
        System.out.println(account1.getPhones());
    }

    @Test
    @Transactional
    public void getAll() {
        List<Account> accounts = new ArrayList<>();
        Account account1 = new Account();
        account1.setEmail("nick@bk.ru");
        accounts.add(account1);
        account1 = new Account();
        account1.setEmail("nick99@bk.ru");
        accounts.add(account1);
        account1 = new Account();
        account1.setEmail("nick999@bk.ru");
        accounts.add(account1);
        assertEquals(accounts, accountDao.findAll());
    }

    @Test
    @Transactional
    public void getIdByEmail() {
        assertEquals(accountDao.findByEmail("nick@bk.ru").getId(), 123);
    }

    @Test
    @Transactional
    public void getByRegex() {
        List<Account> accounts = new ArrayList<>();
        Account account1 = new Account();
        account1.setEmail("nick999@bk.ru");
        accounts.add(account1);
        assertEquals(accounts, accountDao.findByRegex("Никол", new PageRequest(1, 2)));
    }

    @Test
    @Transactional
    public void getByRegexAll() {
        List<Account> accounts = new ArrayList<>();
        Account account1 = new Account();
        account1.setEmail("nick@bk.ru");
        accounts.add(account1);
        account1 = new Account();
        account1.setEmail("nick99@bk.ru");
        accounts.add(account1);
        account1 = new Account();
        account1.setEmail("nick999@bk.ru");
        accounts.add(account1);
        assertEquals(accounts, accountDao.findByRegex("Никол", null));
    }


    @Test
    @Transactional
    public void getAddress() {
        Account account = new Account();
        initAddress(account);
        assertEquals(account.getHomeAddress(), accountDao.findById(123).getHomeAddress());
    }

    @Test
    @Transactional
    public void updateAddress() {
        Account account = accountDao.findById(123);
        account.getHomeAddress().setCity("USA");
        accountDao.save(account);
        assertEquals(account.getHomeAddress(), accountDao.findById(123).getHomeAddress());
    }

    @Test
    @Transactional
    public void deleteAddress() {
        Account account = accountDao.findById(123);
        account.setHomeAddress(null);
        accountDao.save(account);
        assertEquals(null, accountDao.findById(123).getHomeAddress());
    }

    @Test
    @Transactional
    public void getPhones() {
        Account account = new Account();
        initAccount(account);
        assertEquals(account.getPhones(), accountDao.findById(123).getPhones());
    }

    @Test
    @Transactional
    public void updatePhones() {
        Account account = accountDao.findById(123);
        Phone phone = new Phone("12344555", PhoneType.WORK);
        phone.setAccount(account);
        account.getPhones().add(phone);
        accountDao.save(account);
        assertEquals(account.getPhones(), accountDao.findById(123).getPhones());
    }

    @Test
    @Transactional
    public void getFriend() {
        Account account = new Account();
        account.setEmail("nick99@bk.ru");
        List<Friend> accountMap = new ArrayList<>();
        accountMap.add(new Friend(account));
        assertEquals(accountMap, accountDao.findById(123).getFriends());
    }

    public void initAccount(Account account) {
        account.setId(123);
        account.setName("Николай");
        account.setSurname("Иванов");
        account.setPatronymic("Иванович");
        account.setBirthday(LocalDate.of(1988, Month.NOVEMBER, 3));
        account.setEmail("nick@bk.ru");
        account.setPhones(Arrays.asList(new Phone(account, "84950179674", PhoneType.HOME),
                new Phone(account, "89160719674", PhoneType.WORK)));
        account.setIcq("1123455");
        account.setSkype("nickkk");
        account.setOther("java");
        account.setRegistrationDate(LocalDate.now());
        initAddress(account);
    }

    private void initAddress(Account account) {
        Address address = new Address(account);
        account.setHomeAddress(address);
        address.setCountry("Россия");
        address.setCity("Москва");
        address.setStreet("Ленина");
        address.setApartment(3);
        address.setHouse(1);
        address.setStructure(2);
    }
}

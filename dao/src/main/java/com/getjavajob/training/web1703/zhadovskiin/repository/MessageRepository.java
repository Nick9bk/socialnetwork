package com.getjavajob.training.web1703.zhadovskiin.repository;

import com.getjavajob.training.web1703.zhadovskiin.common.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Integer> {
    @Query("select m from Message as m where m.fromAccount.id = ?1 or m.toAccount.id = ?1")
    List<Message> findByAccountId(int id);

    List<Message> findAll();

    Message save(Message group);

    @Query("select m from Message as m where (m.fromAccount.id  = ?1 and  m.toAccount.id = ?2) or " +
            "(m.fromAccount.id  = ?2 and  m.toAccount.id = ?1)")
    List<Message> getMessageOfFriends(int from, int to);
}

package com.getjavajob.training.web1703.zhadovskiin.repository;

import com.getjavajob.training.web1703.zhadovskiin.common.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface GroupRepository extends JpaRepository<Group, Integer> {
    Group findById(int id);

    List<Group> findByName(String name);

    List<Group> findAll();

    void deleteById(int id);

    Group save(Group group);

    @Query("select a.groups from Account a where a.id = ?1")
    List<Group> getGroupByAccountId(int accountId);
}
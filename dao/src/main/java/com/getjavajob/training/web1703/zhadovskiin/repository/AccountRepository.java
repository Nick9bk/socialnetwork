package com.getjavajob.training.web1703.zhadovskiin.repository;

import com.getjavajob.training.web1703.zhadovskiin.common.Account;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Integer> {
    Account findById(int id);

    Account findByEmail(String name);

    List<Account> findAll();

    void deleteById(int id);

    Account save(Account group);

    @Query("select a from Account as a where a.name LIKE CONCAT('%', ?1,'%') or a.surname LIKE CONCAT('%', ?1,'%') or " +
            "a.patronymic LIKE CONCAT('%', ?1,'%')")
    List<Account> findByRegex(String regexName, Pageable pageable);
}
